package com.addcel.telecom.pago.servicios.services;



import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.telecom.pago.servicios.utils.UtilsService;
import com.addcel.telecom.pago.servicios.utils.Utils;

import com.addcel.telecom.pago.servicios.model.vo.ReglasResponse;
import com.addcel.telecom.pago.servicios.model.vo.ReqConsultaServicioRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqEstatusServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqPagoServicioRequestVO;
import com.addcel.telecom.pago.servicios.utils.AddCelGenericMail;
import com.addcel.telecom.pago.servicios.utils.Constantes;


import static com.addcel.telecom.pago.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;

import com.addcel.telecom.pago.client.telecom.wsservicios.GetConsultaPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetConsultaPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetEstatusPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetEstatusPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetPeticionPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetPeticionPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.ObjectFactory;
import com.addcel.telecom.pago.client.telecom.wsservicios.ReqConsultaServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ReqEstatusServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ReqPagoServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResConsultaServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResPagoServicio;
import com.addcel.telecom.pago.servicios.model.mapper.ServiceMapper;
import com.addcel.telecom.pago.servicios.model.vo.AbstractVO;
import com.addcel.telecom.pago.servicios.model.vo.AfiliacionVO;
import com.addcel.telecom.pago.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.PagoServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.PagoTelecomDetalle;
import com.addcel.telecom.pago.servicios.model.vo.UsuarioVO;
import com.addcel.telecom.pago.servicios.model.vo.ValidacionVO;
import com.addcel.telecom.pago.servicios.model.vo.RuleVO;
import com.addcel.telecom.pago.servicios.model.vo.Servicio;
import com.addcel.telecom.pago.servicios.model.vo.ServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.telecom.pago.servicios.model.vo.BitacoraVO;
import com.addcel.telecom.pago.servicios.model.vo.ConsultapsVO;
import com.addcel.telecom.pago.servicios.model.vo.ResponsePagoMovilesVO;
import com.addcel.telecom.pago.servicios.model.vo.DatosDevolucion;
import com.addcel.telecom.pago.servicios.model.vo.PagoTelecomInfo;
import com.addcel.telecom.pago.servicios.model.vo.TBitacoraVO;
import com.addcel.telecom.pago.servicios.model.vo.ResponseConsultaLCVO;
import com.addcel.telecom.pago.servicios.model.vo.DatosCorreoVO;
import com.addcel.telecom.pago.servicios.model.vo.AprovisionamientoResponse;
import com.addcel.telecom.pago.servicios.model.vo.Bitacora;
import com.addcel.utils.AddcelCrypto;

@Service

public class Pago3DS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pago3DS.class);
	
	@Autowired
	private UtilsService jsonUtils;	
	
	@Autowired
	private ServiceMapper mapper;	
	
	payService service = new payService();
	
	@Autowired
    @Qualifier("webServicePagoServicioBridge")
    private transient WebServiceTemplate webServiceTemplate;
	
	public ModelAndView procesaPago(String json, ModelMap modelo) {
		ModelAndView mav = null;
		MobilePaymentRequestVO pago = null;
		UsuarioVO usuarioVO = null;
		ReglasResponse response = null;
		AbstractVO resValidacion = null;
		ServicioVO servicioVO = null;
		
		ReqPagoServicioRequestVO reqPagoServicioRequestVO = new ReqPagoServicioRequestVO();
		ReqConsultaServicioRequestVO reqConsultaServicioRequestVO = new ReqConsultaServicioRequestVO();
		ConsultapsVO consultapsVO = new ConsultapsVO();
		
		try {
			/*LOGGER.info("JSON PAGO BANORTE - "+ json);			
			json = Utils.encryptJson(json);*/
			LOGGER.info("JSON ENCRIPTADO PAGO BANORTE - "+ json);
			json = Utils.decryptJson(json);
			pago = (MobilePaymentRequestVO) jsonUtils.jsonToObject(json, MobilePaymentRequestVO.class);
		
			reqConsultaServicioRequestVO = setReqConsultaServicioRequestVO(pago);	
			consultapsVO = this.getConsultaPS(reqConsultaServicioRequestVO);
			//servicioVO =  mapper.ConsultaServicioByIdProducto(pago.getIdProductoTerceros());
			
			
			if (consultapsVO.getIdError() == 0)	{
				LOGGER.info("consultapsVO.getConsultaPS().getTotal(): "+consultapsVO.getConsultaPS().getTotal());
				
				pago.setTotal(consultapsVO.getConsultaPS().getTotal());				
			
				LOGGER.info("PAGO: "+json);
				
	    		mav = new ModelAndView("payworks/pagina-3DS");
	    		if(pago.getIdTarjeta() == 0){
	    			mav = new ModelAndView("payworks/pagina-error");
	    			mav.addObject("mensajeError", "Estimado usuario, hemos detectado que tiene una version antigua de Telecomm, "
	    					+ "por favor, actualicela para poder hacer uso de los servicios que le ofrece Telecomm. Por su atencion, gracias. ");
	    			return mav;
	    		}
	    		usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUsuario()), pago.getIdTarjeta());
	    		LOGGER.info("usuarioVO.getNombreEstado(): "+usuarioVO.getNombreEstado());
	    		if ("1".equals(usuarioVO.getTipoTarjeta()) || "2".equals(usuarioVO.getTipoTarjeta())){
	    			response = validaReglas(usuarioVO.getIdUsuario(), usuarioVO.getNumTarjeta(), 0);
	    			if(response == null){
	    				resValidacion = validaBloqueos(usuarioVO.getNumTarjeta());
	    				if(resValidacion.getIdError() == 0){
	    					pago.setNombreTarjeta(usuarioVO.getNombreTarjeta());
	    					pago.setNombre(usuarioVO.getNombre());
	                		pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
	                		pago.setTarjeta("XXXX XXXX XXXX " + 
	                				pago.getTarjeta().substring(pago.getTarjeta().length() - 4 ,pago.getTarjeta().length()));
	                		
	                		pago.setEstado(usuarioVO.getNombreEstado());
	                		pago.setCiudad(usuarioVO.getCiudad());
	                		pago.setCalle(usuarioVO.getCalle());
	                		pago.setColonia(usuarioVO.getColonia());	     
	                		pago.setCodigoPostal(usuarioVO.getCodigoPostal());
	                		pago.setFechaNacimiento(usuarioVO.getFechaNac());
	                		pago.setTelefono(usuarioVO.getTelefonoCasa());
	                		if("1".equals(usuarioVO.getTipoTarjeta())){
	                			pago.setTarjetaT("VISA");
	                		} else {
	                			pago.setTarjetaT("MASTERCARD");
	                		}
	                		pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
	            			if(usuarioVO.getCvv2() != null && StringUtils.isNotEmpty(usuarioVO.getCvv2())){
	            				pago.setCvv2(AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
	            			}
	            			modelo.put("pagoForm", pago);
	            			mav.addObject("pagoForm", pago);
	    				}
	    				
	    			} else {
	    				mav = new ModelAndView("payworks/pagina-error");
	        			mav.addObject("mensajeError", response.getMensaje());
	    			}
	    		} else if ("3".equals(usuarioVO.getTipoTarjeta())) {
	    			mav = new ModelAndView("payworks/pagina-error");
	    			mav.addObject("mensajeError", "Disculpe las molestias, continuamos trabajando para habilitar pagos con tarjetas AMEX.");
	    		} else {
	    			mav = new ModelAndView("payworks/pagina-error");
	    			mav.addObject("mensajeError", "Tarjeta no valida");
	    		}
		}//End if 
		else{
			
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Resultado </br> </br> "+consultapsVO.getMensajeError());
			return mav;
		}
		}catch (Exception pe) {
			LOGGER.error("Error General en el proceso de pago.: {}", pe.getMessage());
			pe.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Error General en el proceso de pago: " + pe.getMessage());
		}
		
		return mav;
	}
	
	public ModelAndView procesaPago(MobilePaymentRequestVO pago, ModelMap modelo){
		ModelAndView mav = new ModelAndView("exito");
		AfiliacionVO afiliacion = null;
		UsuarioVO usuarioVO = null;
		try{
			LOGGER.info("procesaPago");			
			if("2".equals(pago.getIdPais())){								
				afiliacion = mapper.buscaAfiliacion("5");
			} else {
				afiliacion = mapper.buscaAfiliacion("1");
			}
			if(afiliacion == null){
				LOGGER.debug("NO hay afiliacion configurada" );
				mav = new ModelAndView("pagina-error");
				mav.addObject("mensajeError", "No existe una afiliacion configurada.");
			}else{
				usuarioVO = mapper.getUsuario(Long.valueOf(pago.getIdUsuario()), pago.getIdTarjeta());
				LOGGER.info("DATOS  DE USUARIO: "+usuarioVO.getNombre()+", TARJETA: "+usuarioVO.getNumTarjeta()
        				+", VIGENCIA: "+usuarioVO.getVigenciaTarjeta()+", CVV: "+usuarioVO.getCvv2() +", CVV FORM: "+pago.getCvv2());
				pago.setAfiliacion(afiliacion);				
				pago.setNombre(usuarioVO.getNombre());
				LOGGER.info("usuarioVO.getApellidoP(): "+usuarioVO.getApellidoP());
				pago.setApPat(usuarioVO.getApellidoP());
				pago.setNombreTarjeta(usuarioVO.getNombreTarjeta());
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(usuarioVO.getNumTarjeta()));
    			pago.setVigencia(AddcelCrypto.decryptTarjeta(usuarioVO.getVigenciaTarjeta()));
	    		if(usuarioVO.getCvv2() != null && StringUtils.isNotEmpty(usuarioVO.getCvv2())){
	    			pago.setCvv2(AddcelCrypto.decryptTarjeta(usuarioVO.getCvv2()));
	    		} else {
	    			pago.setCvv2(pago.getCvv2());
	    		}
	    		pago.setTipoTarjeta(Integer.valueOf(usuarioVO.getTipoTarjeta()));
	    		LOGGER.info("DATOS  DE USUARIO: "+pago.getNombre()+", TARJETA: "+pago.getTarjeta()
        				+", VIGENCIA: "+pago.getVigencia()+", CVV: "+pago.getCvv2() );
				insertaBitacoras(pago);
				mav = procesaPago3DSecurePayWorks(pago, modelo);
				HttpServletRequest request = null;
//				return payworks2Respuesta(pago.getIdTransaccion()+"", "100", "", 
//						"7627488", "A", "A", "000067538", "766220", "A", "", "", "", "", modelo,request);
			}
		}catch(Exception e){
			LOGGER.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Error General en el proceso de pago: " + e.getMessage());
		}
		return mav;
	}
	
	
	
	public ModelAndView procesaPago3DSecurePayWorks(MobilePaymentRequestVO pago, ModelMap modelo) {
		ModelAndView mav = null;
		HashMap<String, String> resp = new HashMap<String, String>();
		int respBD = 0;
		double total = 0;
		try{
			LOGGER.info("DATOS ENVIADOS [URL: https://eps.banorte.com/secure3d/Solucion3DSecure.htm, ID BITACORA: "+pago.getIdTransaccion()
					+", NOMBRE: "+pago.getNombre()+", AFILIACION: "+pago.getAfiliacion().getIdAfiliacion()
					+", USUARIO AFILIACION: "+pago.getAfiliacion().getUsuario()
					+", ID TERMINAL: "+pago.getAfiliacion().getIdTerminal()
					+", MERCHANT ID: "+pago.getAfiliacion().getMerchantId()
					+", URL_BACK: "+pago.getAfiliacion().getForwardPath()+"]");
			resp.put("ID_AFILIACION", pago.getAfiliacion().getIdAfiliacion());
			resp.put("USUARIO", pago.getAfiliacion().getUsuario());
			resp.put("CLAVE_USR", pago.getAfiliacion().getClaveUsuario());
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", pago.getAfiliacion().getIdTerminal());
			resp.put("Reference3D", pago.getIdTransaccion() + "");
			resp.put("MerchantId", pago.getAfiliacion().getMerchantId());
			resp.put("ForwardPath", pago.getAfiliacion().getForwardPath());
//			resp.put("Expires", pago.getMes() + "/" + pago.getAnio().substring(2, 4));
			resp.put("Expires", pago.getVigencia());
			total = pago.getMonto() + pago.getComision();
			resp.put("Total", Utils.formatoMontoPayworks(total + ""));
			resp.put("Card", pago.getTarjeta());
			resp.put("CardType", pago.getTipoTarjeta() == 1? "VISA": "MC");
////			resp.put("Card", "5482341205309648");
//			resp.put("Card", "4000160000004147");
//			resp.put("CardType","MC");
//			resp.put("Expires", "01/17");
			respBD = mapper.guardaTBitacoraPIN(pago.getIdTransaccion(), Utils.encryptHard(pago.getCvv2()));
			if(respBD == 1){
				mav = new ModelAndView("payworks/comercio-send");
				mav.addObject("prosa", resp);
				mav.addObject("pagoRequest", pago);
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	
	public ModelAndView procesaRespuesta3DPayworks(String cadena, ModelMap modelo) {
		ResponsePagoMovilesVO datosResp = null;
		ModelAndView mav = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		String pin = null;
		String msg = null;
		AfiliacionVO afiliacion = null;
		DatosDevolucion devolucion = null;
		try{
			LOGGER.info("procesaRespuesta3DPayworks1");
			cadena = cadena.replace(" ", "+");
			LOGGER.info("Cadena procesaRespuesta3DPayworks: " + cadena);
			String[] spl = cadena.split("&");
			String[] data = null;
			for(String cad: spl){
				LOGGER.info("data:  " + cad);
				data = cad.split("=");
				resp.put(data[0], data.length >= 2? data[1]: "");
			}		
			
			LOGGER.info("(String) resp.get(Reference3D): "+(String) resp.get("Reference3D"));
			PagoTelecomInfo pagoInfo = mapper.buscarDetallePago((String) resp.get("Reference3D"));
			if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){
				afiliacion = mapper.buscaAfiliacion("1");
				pin = mapper.buscarTBitacoraPIN((String) resp.get("Reference3D"));
				pin = AddcelCrypto.decryptHard(pin);
				mav = new ModelAndView("payworks/comercioPAYW2");
				mav.addObject("prosa", resp);
				resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
				resp.put("USUARIO", afiliacion.getUsuario());
				resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
				resp.put("CMD_TRANS", "VENTA");
				resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
				resp.put("Total", Utils.formatoMontoPayworks((String)resp.get("Total")));				
				resp.put("MODO", "PRD");				
				resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
				resp.put("NUMERO_TARJETA", resp.get("Number"));
				resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
				resp.put("CODIGO_SEGURIDAD", pin);
				resp.put("MODO_ENTRADA", "MANUAL");
				resp.put("URL_RESPUESTA", Constantes.VAR_PAYW_URL_BACK_W2);
				resp.put("IDIOMA_RESPUESTA", "ES");
				devolucion = new DatosDevolucion();
				if(resp.get("XID") != null){
					resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
					resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
					devolucion.setXid((String)resp.get("XID"));
					devolucion.setCavv((String)resp.get("CAVV"));
				}
				resp.put("CODIGO_SEGURIDAD", pin);
//				devolucion = new DatosDevolucion();
//				devolucion.setIdTransaccion(pagoInfo.getIdTransaccion());
//				devolucion.setXid("");
//				devolucion.setCavv("");
//				devolucion.setEci((String)resp.get("ECI"));
//				mapper.insertaDatosDevolucion(devolucion);
				LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());
			} else {				
				mav=new ModelAndView("payworks/pagina-error");
				datosResp = new ResponsePagoMovilesVO();				
				msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D RECHAZADA: " + resp.get("Status") ;				
				datosResp.setIdError(Integer.parseInt((String)resp.get("Status")));				
				datosResp.setMensajeError("El pago fue rechazado. " + 
						(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + Constantes.errorPayW.get(resp.get("Status")) );				
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));				
				datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());				
				updateBitacoras(pagoInfo, datosResp, null, null, null, null, null, null, msg);
				mav.addObject("mensajeError", datosResp.getMensajeError());				
			}
		}catch(Exception e){
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			mav=new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	
	public ModelAndView payworks2Respuesta(String NUMERO_CONTROL,
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW, String RESULTADO_AUT, String BANCO_EMISOR,
			String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("exito");
		ResponsePagoMovilesVO datosResp = null;
		ResponseConsultaLCVO respServ = new ResponseConsultaLCVO();
		DatosCorreoVO datosCorreoVO=null;
		String msg = null;
		AprovisionamientoResponse respuesta = null;
		payService service = new payService();
		ReqPagoServicioRequestVO reqPagoServicioRequestVO = new ReqPagoServicioRequestVO();
		PagoServicioVO pagoServicioVO = null;
		
		try{
			LOGGER.info("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
					+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
					+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
					+" CODIGO_PAYW: " + CODIGO_AUT+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
					+" MARCA_TARJETA: " + MARCA_TARJETA);			
			if(MARCA_TARJETA != null){
				MARCA_TARJETA = MARCA_TARJETA.replaceAll("-", "");
				TIPO_TARJETA = MARCA_TARJETA + "/" + TIPO_TARJETA;
			}
			//Comentar datos cuando en PROD, cuando paywork regrese informacion
			if (StringUtils.equals(Constantes.MODO, Constantes.AUT)) {
				BANCO_EMISOR = "BANORTE";
				TIPO_TARJETA = "VISA/CREDITO";
			}
			TEXTO = Utils.cambioAcento(TEXTO);
			
			PagoTelecomInfo pagoInfo = mapper.buscarDetallePago(NUMERO_CONTROL);
			//pagoInfo.setApPat("Segovia");
			
			LOGGER.info("Consulta detalle pago: "+pagoInfo.toTrace());
			LOGGER.info("Consulta detalle pago1: "+pagoInfo.toTrace());
			datosResp = new ResponsePagoMovilesVO();
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			datosResp.setReferencia(pagoInfo.getReferencia());
			datosResp.setMonto(pagoInfo.getMonto());
			pagoInfo.setTarjeta(AddcelCrypto.decryptTarjeta(pagoInfo.getTarjeta()));
			datosResp.setTarjeta(pagoInfo.getTarjeta().substring(0, 6) + " XXXXXX " + pagoInfo.getTarjeta().substring(pagoInfo.getTarjeta().length() - 4));
			datosResp.setBancoTarjeta(BANCO_EMISOR);
			
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				msg = "EXITO PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": pagoInfo.getTipoTarjeta() == 2? "MASTER": "VISA") 
						+ " "+BANCO_EMISOR+", "+TIPO_TARJETA;
				datosResp.setAutorizacion(CODIGO_AUT);
	//			mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setUsuario(String.valueOf(pagoInfo.getIdUsuario()));
				datosResp.setFecha(respServ.getFecha());
				datosResp.setHora(respServ.getHora());
				datosResp.setReferen(pagoInfo.getReferencia());
				datosResp.setMedioPresentacion("Internet");
				datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
				datosResp.setTipo(TIPO_TARJETA);
				mav = new ModelAndView("payworks/exito");
				datosCorreoVO = new DatosCorreoVO();
				datosCorreoVO.setEmail(pagoInfo.geteMail());
				/*********************************************
				 * 
				 * AQUI ES DONDE SE LE DICE AL COMERCIO
				 * QUE EL COBRO ES EXITOSO
				 * 
				 * ***************************************/
				
				
				//respuesta = notificacionComercio(pagoInfo);
				
				reqPagoServicioRequestVO.setIdProductoTerceros(pagoInfo.getIdProductoTerceros());
				reqPagoServicioRequestVO.setFolioCliente(pagoInfo.getFolioCliente());
				reqPagoServicioRequestVO.setReferencia1(pagoInfo.getReferencia1());
				reqPagoServicioRequestVO.setReferencia2(pagoInfo.getReferencia2());
				reqPagoServicioRequestVO.setReferencia3(pagoInfo.getReferencia3());
				reqPagoServicioRequestVO.setFechaOperacion(pagoInfo.getFechaOperacion());
				reqPagoServicioRequestVO.setNombre(pagoInfo.getNombre());
				reqPagoServicioRequestVO.setApPat(pagoInfo.getApPat());
				reqPagoServicioRequestVO.setApMat(pagoInfo.getApMat());
				reqPagoServicioRequestVO.setFechaNacimiento(pagoInfo.getFechaNacimiento());
				reqPagoServicioRequestVO.setRfc(pagoInfo.getRfc());
				reqPagoServicioRequestVO.setCalle(pagoInfo.getCalle());
				reqPagoServicioRequestVO.setColonia(pagoInfo.getColonia());;
				reqPagoServicioRequestVO.setCiudad(pagoInfo.getCiudad());
				reqPagoServicioRequestVO.setCodigoPostal(pagoInfo.getCodigoPostal());
				reqPagoServicioRequestVO.setEstado(pagoInfo.getEstado());
				reqPagoServicioRequestVO.setTelefono(pagoInfo.getTelefono());
				reqPagoServicioRequestVO.setPrincipalPesos(pagoInfo.getPrincipalPesos());
				reqPagoServicioRequestVO.setPrincipalLocal(pagoInfo.getPrincipalLocal());
				reqPagoServicioRequestVO.setTipoCambio(pagoInfo.getTipoCambio());
				reqPagoServicioRequestVO.setTipoMoneda(pagoInfo.getTipoMoneda());
				reqPagoServicioRequestVO.setPrecio(pagoInfo.getPrecio());
				reqPagoServicioRequestVO.setPrecio2(pagoInfo.getPrecio2());
				reqPagoServicioRequestVO.setTienda(pagoInfo.getTienda());
				reqPagoServicioRequestVO.setTerminal(pagoInfo.getTerminal());
				reqPagoServicioRequestVO.setOperador(pagoInfo.getOperador());
				reqPagoServicioRequestVO.setDisponible1(pagoInfo.getDisponible1());
				reqPagoServicioRequestVO.setDisponible2(pagoInfo.getDisponible2());
				reqPagoServicioRequestVO.setDisponible3(pagoInfo.getDisponible3());
				
				
				LOGGER.info("ANTES DEL getPeticionPS");
				pagoServicioVO = getPeticionPS(reqPagoServicioRequestVO);
				LOGGER.info("DESPUES DEL getPeticionPS");
				
				respuesta = new AprovisionamientoResponse();				
				//if(respuesta.getIdError() == 0){
				if (pagoServicioVO.getIdError() == 0){
					LOGGER.info("pagoServicioVO.getPagoServicio().getTotal() "+pagoServicioVO.getPagoServicio().getTotal());
					double total = Double.parseDouble(pagoServicioVO.getPagoServicio().getTotal());
					
					//double total = respuesta.getMonto() + respuesta.getComision();					
					try {
						enviaCorreo(respuesta, total, CODIGO_AUT, String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), pagoInfo.getConcepto(),pagoServicioVO.getPagoServicio().getPrincipalPesos(),pagoServicioVO.getPagoServicio().getIva(),pagoServicioVO.getPagoServicio().getRedondeo());
					} catch (Exception e) {
						LOGGER.error("No se pudo enviar el correo..");
					}
					datosResp.setIdError(0);
					updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, 
								msg+" "+pagoInfo.getConcepto());
					mav.addObject("mensajeError", "<BR>Estimado usuario, su pago ha sido exitoso." 
							+"<br><BR><BR> Pago: "+pagoInfo.getConcepto()
							+"<br>Fecha: "+FECHA_REQ_CTE
							+"<br>Referencia: "+REFERENCIA
							+"<br>Folio: "+RESULTADO_PAYW
							+"<br>Sucursal: 1"
							+"<br>Caja: 1"
							+"<br>Forma de pago: Tarjeta de credito"
							+"<br>Numero de Autorizacion: "+CODIGO_AUT
							+"<br>Folio MC: "+respuesta.getNumAuth()
							+"<br>Importe: "+pagoServicioVO.getPagoServicio().getPrincipalPesos()
							+"<br>Iva: "+pagoServicioVO.getPagoServicio().getIva()
							+"<br>Comision: "+pagoServicioVO.getPagoServicio().getPremio()
							+"<br>Redondeo: "+pagoServicioVO.getPagoServicio().getRedondeo()
							+"<br>Total (incluida comision): "+total
							+"<BR><BR>Servicio operado por Plataforma Telecomm. "
							+ "El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas. "
							+ "Para aclaraciones marque 01-800-925-5001. Favor de guardar este comprobante de pago para "
							+ "posibles aclaraciones.");
				} else {
					datosResp.setConcepto("Solicitud será aplicada en las próximas 24 hrs.");
					datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
					datosResp.setIdError(respuesta.getIdError());
					datosResp.setMensajeError(respuesta.getMensajeAntad());
					updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, datosResp.getMensajeError());
					mav = new ModelAndView("payworks/exito");
					mav.addObject("mensajeError", "El cobro fue realizado con éxito con la autorización "+CODIGO_AUT+
							" y tu solicitud será aplicada en las próximas 24 hrs.");
					try {
						double total = respuesta.getMonto() + respuesta.getComision();
						enviaCorreo(respuesta, total, CODIGO_AUT, String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), pagoInfo.getConcepto());
						LOGGER.info("ENVIO CORRECTAMENTE EL CORREO CUANDO TELECOMM FALLO.");
					} catch (Exception e) {
						LOGGER.error("No se pudo enviar el correo..");
						e.printStackTrace();
					}
				}
			} else {
//				mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setIdError(-9);
				datosResp.setMensajeError("El pago fue " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +BANCO_EMISOR+", "+TIPO_TARJETA+
					" . " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW:RESULTADO_AUT != null? RESULTADO_AUT: "") + ", Descripcion: " + TEXTO  );
				
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", datosResp.getMensajeError());
				updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, msg);
			}
		}catch(Exception e){
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
					+ "Por favor vuelva a intentarlo en unos minutos.");
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			datosResp = new ResponsePagoMovilesVO(100, 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
							+ "Por favor vuelva a intentarlo en unos minutos.");
		}finally{
			modelo.put("datosResp", datosResp);
			mav.addObject("datosResp", datosResp);
		}
		return mav;	
	}
	
	

	
	private void insertaBitacoras(MobilePaymentRequestVO pago) throws Exception{
		BitacoraVO tb = null;
		UtilsService utilsService = new UtilsService();
		try {
			pago.setTarjeta(pago.getTarjeta()!= null  && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "");
			tb = new BitacoraVO(Long.valueOf(pago.getIdUsuario()), ""+pago.getConcepto() + " "+( pago.getTipoTarjeta() == 3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					"Referencia: " + pago.getReferencia() + ", Importe: " + pago.getMonto(), 
					pago.getTarjeta(), pago.getTipo(), 
					pago.getSoftware(), pago.getModelo(), pago.getImei(), 
					pago.getMonto() );
			tb.setIdProveedor(45);
			mapper.insertaBitacoraTransaccion(tb);
			pago.setIdTransaccion(tb.getIdBitacora());

			TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					pago.getIdUsuario(),null, null, null, 
					"" +pago.getConcepto() + " "+ (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					String.valueOf(pago.getMonto()), pago.getComision()+"", "0", "0");	
			tbProsa.setTarjeta("");
			tbProsa.setTransaccion("");
			mapper.addBitacoraProsa(tbProsa);			
			PagoTelecomDetalle pagoTelecomDetalle = new PagoTelecomDetalle();
			pagoTelecomDetalle.setIdTransaccion(pago.getIdTransaccion());
			pagoTelecomDetalle.setDescripcion(pago.getConcepto());
			pagoTelecomDetalle.setTipoTarjeta(pago.getTipoTarjeta());
			pagoTelecomDetalle.setTotal(pago.getMonto());
			pagoTelecomDetalle.setComision(pago.getComision());
			pagoTelecomDetalle.setReferencia(pago.getReferencia());
			pagoTelecomDetalle.setEmisor(String.valueOf(pago.getEmisor()));
			pagoTelecomDetalle.setCodigoRespuesta("");
			/*Datos propios de telecom*/
			pagoTelecomDetalle.setApMat(quitarAcento(pago.getApMat()));			
			pagoTelecomDetalle.setApPat(quitarAcento(pago.getApPat()));
			pagoTelecomDetalle.setCalle(pago.getCalle());
			pagoTelecomDetalle.setCiudad(pago.getCiudad());
			pagoTelecomDetalle.setCodigoPostal(pago.getCodigoPostal());
			pagoTelecomDetalle.setColonia(pago.getColonia());
			pagoTelecomDetalle.setComision(pago.getComision());
			pagoTelecomDetalle.setDisponible1(pago.getDisponible1());
			pagoTelecomDetalle.setDisponible2(pago.getDisponible2());
			pagoTelecomDetalle.setDisponible3(pago.getDisponible3());
			//pagoTelecomDetalle.setEmisor(pago.getemisor);
			pagoTelecomDetalle.setEstado(pago.getEstado());
			pagoTelecomDetalle.setFechaNacimiento(pago.getFechaNacimiento());
			pagoTelecomDetalle.setFechaOperacion(utilsService.getFechaActual());
			pagoTelecomDetalle.setFolioCliente(pago.getFolioCliente());
			pagoTelecomDetalle.setH2h(pago.getH2h());
			pagoTelecomDetalle.setIdProductoTerceros(pago.getIdProductoTerceros());
			pagoTelecomDetalle.setNombre(pago.getNombre());			
			pagoTelecomDetalle.setOperador(pago.getOperador());
			pagoTelecomDetalle.setPrecio(pago.getPrecio());
			pagoTelecomDetalle.setPrecio2(pago.getPrecio2());
			pagoTelecomDetalle.setPrincipalLocal(pago.getPrincipalLocal());
			pagoTelecomDetalle.setPrincipalPesos(pago.getPrincipalPesos());
			pagoTelecomDetalle.setReferencia1(pago.getReferencia1());
			pagoTelecomDetalle.setReferencia2(pago.getReferencia2());
			pagoTelecomDetalle.setReferencia3(pago.getReferencia3());
			pagoTelecomDetalle.setRfc(pago.getRfc());
			pagoTelecomDetalle.setTelefono(pago.getTelefono());
			pagoTelecomDetalle.setTerminal(pago.getTerminal());
			pagoTelecomDetalle.setTienda(pago.getTienda());
			pagoTelecomDetalle.setTipoCambio(pago.getTipoCambio());
			pagoTelecomDetalle.setTipoMoneda(pago.getTipoMoneda());
			pagoTelecomDetalle.setTelefono(pago.getTelefono());
			LOGGER.info("msj1");
			pagoTelecomDetalle.setTotal(Double.parseDouble(pago.getTotal()));			
			LOGGER.info("msj2");
			mapper.insertaPagoTelecomDetalle(pagoTelecomDetalle);		
			LOGGER.info("DETALLE ANTAD GUARDADO EXITOSAMENTE - ID TRANSACCION: "+pago.getIdTransaccion()+", LOGIN: "+pago.getUsuario());
			if(pago.getTarjeta() != null && !"".equals(pago.getTarjeta())){
				pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			}
		} catch (Exception e) {
			LOGGER.error("Error al guardar en bitacoras,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
	
	private void updateBitacoras(PagoTelecomInfo pago, ResponsePagoMovilesVO datosResp,  String REFERENCIA, String CODIGO_AUT, 
			String REFERENCIA_CAN, String CODIGO_AUT_CAN, 
			String BANCO_EMISOR, String TIPO_TARJETA, String msg){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
        	LOGGER.info("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraVO.setBitConcepto(msg);
        	tbitacoraVO.setBitTicket(msg);        	
        	tbitacoraVO.setBitNoAutorizacion(datosResp.getAutorizacion());
        	if(datosResp.getIdError() == 0){
        		tbitacoraVO.setBitStatus(1);
        	} else {
        		tbitacoraVO.setBitStatus(datosResp.getIdError());
        	}
        	tbitacoraVO.setBitCodigoError(datosResp.getIdError() != 0? datosResp.getIdError(): 0);
        	tbitacoraVO.setDestino("Referencia: " + pago.getReferencia() + "-" + TIPO_TARJETA);
        	mapper.updateBitacora(tbitacoraVO);
        	LOGGER.debug("Fin Update TBitacora.");
        	LOGGER.debug("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraProsaVO.setConcepto(msg);
        	tbitacoraProsaVO.setAutorizacion(datosResp.getAutorizacion());
        	tbitacoraProsaVO.setReferenciaPayw(REFERENCIA);
        	mapper.updateBitacoraProsa(tbitacoraProsaVO);
        	LOGGER.debug("Fin Update TBitacoraProsa.");
        } catch(Exception e){
        	LOGGER.error("Error al actualizar las bitacoras: ", e);
        }
	}
	
	
	
	private void enviaCorreo(AprovisionamientoResponse response, double total,
			String eM_Auth, String eM_OrderID, String nombre, String concepto,String importe,String iva,String redodeo) {
		DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
		String ticket = null;
		String bodyMail = null;
		String host = null;
	    int port = 0;
	    String username = null;
	    String password = null;
	    String from = null; 
	    Properties props = null;
		try {			
			datosCorreoVO.setDescServicio(response.getConcepto());
			datosCorreoVO.setFecha(UtilsService.getFechaActual());
			datosCorreoVO.setReferenciaServicio(response.getReferencia());			
			datosCorreoVO.setIdBitacora(Long.valueOf(eM_OrderID));
			datosCorreoVO.setNoAutorizacion(eM_Auth);
			if(response.getNumAuth() != null){
				datosCorreoVO.setFolioXcd(response.getNumAuth());
			} else {
				datosCorreoVO.setFolioXcd("Pendiente por Aprobar.");
			}
			datosCorreoVO.setImporte(response.getMonto());
			datosCorreoVO.setComision(response.getComision());
			datosCorreoVO.setMonto(total);
			datosCorreoVO.setNombre(nombre);
			datosCorreoVO.setConcepto(concepto);
//			ticket = response.getMensajeTicket().replace("|", "<br>");
//			ticket = ticket.replace("01(81)-8988-0200", "01–800–925–5001");
			ticket = "Plataforma Telecomm. El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas.";
			datosCorreoVO.setTicket(ticket);
			datosCorreoVO.setEmail(response.geteMail());
			bodyMail = mapper.getParametro("@MENSAJE_COMPRATELECOMM");
			host =  mapper.getParametro("@SMTP"); 
            port = Integer.parseInt( mapper.getParametro("@SMTP_PORT") ); 
            username =  mapper.getParametro("@SMTP_USER"); 
            password =  mapper.getParametro("@SMTP_PWD"); 
            from = mapper.getParametro("@SMTP_MAIL_SEND");
            props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            LOGGER.debug("HOST: " + host);
            LOGGER.debug("PORT: " + port);
            LOGGER.debug("USERNAME: " + username);
//            AddCelGenericMail.generatedMailCommons(datosCorreoVO, bodyMail, props, from, host, port, username, password);
            /*Descomento lo del email si se necesita lo del correo*/
			AddCelGenericMail.generatedMail(datosCorreoVO, bodyMail);
		} catch (Exception e) {
			LOGGER.error("Error al mandar el correo: "+e.getCause());
		}	
	}

	
	private void enviaCorreo(AprovisionamientoResponse response, double total,
			String eM_Auth, String eM_OrderID, String nombre, String concepto) {
		DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
		String ticket = null;
		String bodyMail = null;
		String host = null;
	    int port = 0;
	    String username = null;
	    String password = null;
	    String from = null; 
	    Properties props = null;
		try {			
			datosCorreoVO.setDescServicio(response.getConcepto());
			datosCorreoVO.setFecha(UtilsService.getFechaActual());
			datosCorreoVO.setReferenciaServicio(response.getReferencia());			
			datosCorreoVO.setIdBitacora(Long.valueOf(eM_OrderID));
			datosCorreoVO.setNoAutorizacion(eM_Auth);
			if(response.getNumAuth() != null){
				datosCorreoVO.setFolioXcd(response.getNumAuth());
			} else {
				datosCorreoVO.setFolioXcd("Pendiente por Aprobar.");
			}
			datosCorreoVO.setImporte(response.getMonto());
			datosCorreoVO.setComision(response.getComision());
			datosCorreoVO.setMonto(total);
			datosCorreoVO.setNombre(nombre);
			datosCorreoVO.setConcepto(concepto);
//			ticket = response.getMensajeTicket().replace("|", "<br>");
//			ticket = ticket.replace("01(81)-8988-0200", "01–800–925–5001");
			ticket = "Plataforma Telecomm. El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas.";
			datosCorreoVO.setTicket(ticket);
			datosCorreoVO.setEmail(response.geteMail());
			bodyMail = mapper.getParametro("@MENSAJE_COMPRATELECOMM");
			host =  mapper.getParametro("@SMTP"); 
            port = Integer.parseInt( mapper.getParametro("@SMTP_PORT") ); 
            username =  mapper.getParametro("@SMTP_USER"); 
            password =  mapper.getParametro("@SMTP_PWD"); 
            from = mapper.getParametro("@SMTP_MAIL_SEND");
            props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            LOGGER.debug("HOST: " + host);
            LOGGER.debug("PORT: " + port);
            LOGGER.debug("USERNAME: " + username);
//            AddCelGenericMail.generatedMailCommons(datosCorreoVO, bodyMail, props, from, host, port, username, password);
            /*Descomento lo del email si se necesita lo del correo*/
			AddCelGenericMail.generatedMail(datosCorreoVO, bodyMail);
		} catch (Exception e) {
			LOGGER.error("Error al mandar el correo: "+e.getCause());
		}	
	}


	
	private ReglasResponse validaReglas(long idUsuario, String tarjeta, int idProducto) {
		ReglasResponse response = null;
		String numUsuario = null;
		String numTarjeta = null;
		RuleVO valoresU = null;
		RuleVO valoresT = null;
		try {
			numUsuario = mapper.getParametro("@ANTAD_NUM_TRAN_USUARIO");
			numTarjeta = mapper.getParametro("@ANTAD_NUM_TRAN_TARJETA");

			valoresU = mapper.selectRule(idUsuario, tarjeta, "45", "@ANTAD_NUM_TRAN_USUARIO");
			valoresT = mapper.selectRule(idUsuario, tarjeta, "45", "@ANTAD_NUM_TRAN_TARJETA");
			LOGGER.info("RESPUESTA DE REGLAS DE VALIDACION: TRANSACCIONES USUARIO: "+numUsuario
					+", TRANSACCION TARJETA: "+numTarjeta+", BITACORA USUARIO: "+valoresU.getNumero()
					+", BITACORA TARJETA: "+valoresT.getNumero());
			
			if (((Integer) valoresU.getNumero()).doubleValue() >= Integer.valueOf(numUsuario)) {		
				response = new ReglasResponse(0, "920",
						"En el periodo " + valoresU.getFechaInicio() + " a " + valoresU.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas: " + numUsuario);
			} else if (((Integer) valoresT.getNumero()).doubleValue() >= Integer.valueOf(numTarjeta)) {

				response = new ReglasResponse(0, "922",
						"En el periodo " + valoresT.getFechaInicio() + " a " + valoresT.getFechaFin()
								+ " a superado el número maximo de transacciones permitidas para una Tarjeta: "
								+ numTarjeta);
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);
			response = new ReglasResponse(0, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return response;
	}
	
	private AbstractVO validaBloqueos(String tarjeta) {
		AbstractVO response = new AbstractVO();
		int bloqueoTarjeta = 0;
		bloqueoTarjeta = mapper.getBloqueoTarjeta(tarjeta);
		if(bloqueoTarjeta != 0){
			response.setIdError(-1);
			response.setMensajeError("Estimado usuario, hemos detectado actividad sospechosa por lo cual su tarjeta se encuentra bloqueada.");
		} 
		return response;
	}
	
	
	public ReqConsultaServicioRequestVO setReqConsultaServicioRequestVO(MobilePaymentRequestVO reqPagoServicioRequestVO) {
		
		UtilsService utilsService = new UtilsService();
		String fecha = utilsService.getCurrentTimeStamp();
		ReqConsultaServicioRequestVO reqConsultaServicioRequestVO = new ReqConsultaServicioRequestVO();
		
		reqConsultaServicioRequestVO.setReferencia1(reqPagoServicioRequestVO.getReferencia1());
		reqConsultaServicioRequestVO.setIdProductoTerceros(reqPagoServicioRequestVO.getIdProductoTerceros());
		reqConsultaServicioRequestVO.setReferencia2(reqPagoServicioRequestVO.getReferencia2());
		reqConsultaServicioRequestVO.setReferencia3(reqPagoServicioRequestVO.getReferencia3());
		reqConsultaServicioRequestVO.setFechaOperacion(fecha);
		reqConsultaServicioRequestVO.setPrincipalPesos(reqPagoServicioRequestVO.getPrincipalPesos());
		reqConsultaServicioRequestVO.setDisponible1(reqPagoServicioRequestVO.getDisponible1());
		reqConsultaServicioRequestVO.setDisponible2(reqPagoServicioRequestVO.getDisponible2());
		reqConsultaServicioRequestVO.setDisponible3(reqPagoServicioRequestVO.getDisponible3());	
		
		return reqConsultaServicioRequestVO;
	}
	
	public ConsultapsVO getConsultaPS(ReqConsultaServicioRequestVO request) {
		
		LOGGER.info("getConsultaPS");
		GetConsultaPSResponse response = null;
		GetConsultaPS getConsultaPS = new GetConsultaPS(); 
		ReqConsultaServicio reqConsultaServicio = new ReqConsultaServicio();
		ConsultapsVO consultapsVO = new ConsultapsVO();
		ResConsultaServicio resConsultaServicio = new  ResConsultaServicio();
		ValidacionVO validacionVO = new ValidacionVO();
		Servicio servicio = null;
		String mensaje ="";
		int idProducto =0;
		
		
		LOGGER.info("request.getReferencia1()"+request.getReferencia1());	
		LOGGER.info("request.getIdProductoTerceros()"+request.getIdProductoTerceros());
		LOGGER.info("request.getPrincipalPesos()"+request.getPrincipalPesos());
		
		try {
			LOGGER.info("1");
			ObjectFactory factory = new ObjectFactory();		
			getConsultaPS.setAutentificacion(service.getAutentifica());
			
//			if ((request.getIdProductoTerceros() != "") && UtilsService.validarNumero(request.getIdProductoTerceros()))  
//				idProducto = utilsService.convertToInt(request.getIdProductoTerceros());			
				
			
			validacionVO = service.validacionIdProductoTerceros(request.getIdProductoTerceros());
			
			if (validacionVO.isValida()) {
				servicio = mapper.ConsultaServicioByIdProducto(request.getIdProductoTerceros());
				LOGGER.info("servicioVO.getIdProducto(): "+""+servicio.getIdProducto());
				LOGGER.info("servicioVO.getProducto(): "+""+servicio.getProducto());
				LOGGER.info("servicioVO.getConsulta(): "+""+servicio.getConsulta());
				LOGGER.info("servicio.getEstatus: "+""+servicio.getEstatus());				
				idProducto = jsonUtils.convertToInt(request.getIdProductoTerceros());
				/*El proveedor no permite realizar consultas*/
				if (servicio.getConsulta().equals("0"))	{
					
					consultapsVO.setIdError(-1);
					consultapsVO.setMensajeError("El proveedor no permite realizar consultas");
					
					return consultapsVO;
				}

			}
			else	{
				LOGGER.info("no es valido");
				consultapsVO = service.setConsultaVO(validacionVO);
				return consultapsVO;
			}			
			

			ConsultapsVO consultapsVOConsulta = service.ConsultaServicio(idProducto,request);			
			
			
			//if (consultapsVOConsulta.getClave().equals("-1"))
			if (consultapsVOConsulta.getIdError() == -1)				
				return consultapsVOConsulta;
			
			
				
			reqConsultaServicio = service.setReqConsultaServicio(request);			
			getConsultaPS.setReqConsultaServicio(reqConsultaServicio);
			
			LOGGER.info("ANTES DE ENVIAR AL WS");
			
			response = (GetConsultaPSResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetConsultaPS(getConsultaPS));
			
			LOGGER.info("DESPUES DE ENVIAR AL WS");
			LOGGER.info("response: "+response);
			resConsultaServicio = response.getReturn();
			
			if (resConsultaServicio != null) {
				
				consultapsVO = 	service.getConsultapsVO(resConsultaServicio);
			
			}
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR getConsultaPS - ERROR: "+desc);
		} 		

		return consultapsVO;
		
	}
	
	
	/***************************************************************************************/
	
	public PagoServicioVO getPeticionPS(ReqPagoServicioRequestVO reqPagoServicioRequestVO) {
		LOGGER.info("getPeticionPS");
		GetPeticionPS getPeticionPS = new GetPeticionPS();
		PagoServicioVO pagoServicioVO = new PagoServicioVO();
		ObjectFactory factory = new ObjectFactory();
		ReqPagoServicio reqPagoServicio = new ReqPagoServicio(); 
		GetPeticionPSResponse response = null;
		ResPagoServicio resPagoServicio = new ResPagoServicio();
		PagoServicioVO pagoServicioEstatusVO = new PagoServicioVO();
		boolean operacionPendiente = false;
		ReqConsultaServicioRequestVO reqConsultaServicioRequestVO = new ReqConsultaServicioRequestVO();
		ConsultapsVO consultapsVO = new ConsultapsVO(); 
		Bitacora bitacoraVO = new Bitacora();
		ValidacionVO validacionVO = new ValidacionVO();
		Servicio servicio = null;
		int intento = 0;
		int idProducto =0;
		
		try{
			LOGGER.info(" idUsuario: "+Long.toString(reqPagoServicioRequestVO.getIdUsuario()));
			LOGGER.info("reqPagoServicioRequestVO.getImei(): "+reqPagoServicioRequestVO.getImei());
			String fecha = service.getCurrentTimeStamp();
			LOGGER.info("fecha: "+fecha);
			getPeticionPS.setAutentificacion(service.getAutentifica());
			
			//if ((reqPagoServicioRequestVO.getIdProductoTerceros() != "") && UtilsService.validarNumero(reqPagoServicioRequestVO.getIdProductoTerceros()))  
				//idProducto = utilsService.convertToInt(reqPagoServicioRequestVO.getIdProductoTerceros());
			
			validacionVO = service.validacionIdProductoTerceros(reqPagoServicioRequestVO.getIdProductoTerceros());
			
			if (validacionVO.isValida()) {
				idProducto = jsonUtils.convertToInt(reqPagoServicioRequestVO.getIdProductoTerceros());
			}
			else	{
				LOGGER.info("no es valido");
				pagoServicioVO = service.setPeticionVO(validacionVO);				
				return pagoServicioVO;
			}			

			pagoServicioVO = service.pagoServicio(idProducto,reqPagoServicioRequestVO);
			
			//pagoServicioVO = pagoServicio(idProducto, reqPagoServicioRequestVO.getReferencia1(),reqPagoServicioRequestVO.getReferencia2(),reqPagoServicioRequestVO.getReferencia3());
			
			if (pagoServicioVO.getIdError() ==-1) 
				return pagoServicioVO;
			
			if (pagoServicioVO.getPagoServicio().isConsultaWS()) {
				
				LOGGER.info("TRUE");			
				
//				reqConsultaServicioRequestVO = setReqConsultaServicio(request)
				
				reqPagoServicioRequestVO.setFechaOperacion(fecha);
				reqConsultaServicioRequestVO = service.setReqConsultaServicioRequestVO(reqPagoServicioRequestVO);
				consultapsVO = this.getConsultaPS(reqConsultaServicioRequestVO);
				
//				if (!consultapsVO.getClave().equals("0")) {
					
				if (consultapsVO.getIdError() != 0)	{
					pagoServicioVO.setIdError(consultapsVO.getIdError());
					pagoServicioVO.setMensajeError(consultapsVO.getMensajeError());
					
					return pagoServicioVO;
				}
				else{
					LOGGER.info("consultapsVO.getConsultaPS().getPrincipalPesos(): "+consultapsVO.getConsultaPS().getPrincipalPesos());
					reqPagoServicioRequestVO.setPrincipalPesos(consultapsVO.getConsultaPS().getPrincipalPesos());
					
					if (consultapsVO.getConsultaPS().getIdProductoTerceros().equals("210")) {
						reqPagoServicioRequestVO.setReferencia3(consultapsVO.getConsultaPS().getReferencia3());
					}
				}
			}			

			LOGGER.info("pagoServicioVO.isConsultaWS(): "+pagoServicioVO.getPagoServicio().isConsultaWS());
			
			LOGGER.info("reqPagoServicioRequestVO.getPrincipalLocal(): "+reqPagoServicioRequestVO.getPrincipalLocal());
			reqPagoServicio = service.setReqPagoServicio(reqPagoServicioRequestVO);
			LOGGER.info("reqPagoServicio.getNombre(): "+reqPagoServicio.getNombre());
			getPeticionPS.setReqPagoServicio(reqPagoServicio);
			
			LOGGER.info("ANTES DE ENVIAR AL WS");
			
			response = (GetPeticionPSResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createGetPeticionPS(getPeticionPS));
			
			LOGGER.info("DESPUES DE ENVIAR AL WS");
			LOGGER.info("response: "+response);
			
			
			resPagoServicio = response.getReturn();
			
			if (resPagoServicio != null) { 
				
			
				LOGGER.info("resPagoServicio.getClave():"+resPagoServicio.getClave());
				LOGGER.info("reqPagoServicioRequestVO.getH2h(): "+reqPagoServicioRequestVO.getH2h());
				LOGGER.info("Response detallado "+resPagoServicio.getDisponible1()+" "+ resPagoServicio.getDisponible2()+" "+ resPagoServicio.getDisponible3()+" "+ resPagoServicio.getFechaOperacion()+" "+ resPagoServicio.getFolioCliente()+" "+ resPagoServicio.getFolioProveedor()+" "+ resPagoServicio.getFolioTelecomm()+" "+ resPagoServicio.getIdProductoTerceros()+" "+reqPagoServicioRequestVO.getNombre()+" "+reqPagoServicioRequestVO.getFechaNacimiento()+" "+reqPagoServicioRequestVO.getApPat()+" "+reqPagoServicioRequestVO.getApMat()+" "+ resPagoServicio.getIva()+" "+ resPagoServicio.getMensaje()+" "+ resPagoServicio.getOperador()+" "+ resPagoServicio.getPrecio()+" "+ resPagoServicio.getPremio()+" "+ resPagoServicio.getPrincipalLocal()+" "+ resPagoServicio.getPrincipalPesos()+" "+ resPagoServicio.getRedondeo()+" "+ resPagoServicio.getReferencia1()+" "+ resPagoServicio.getReferencia2()+" "+ resPagoServicio.getReferencia3()+" "+ resPagoServicio.getSubTotal()+" "+ resPagoServicio.getTerminal()+" "+ resPagoServicio.getTienda()+" "+ resPagoServicio.getTipoCambio()+" "+ resPagoServicio.getTipoMoneda()+" "+ resPagoServicio.getTotal()+" "+reqPagoServicioRequestVO.getRfc()+" "+reqPagoServicioRequestVO.getCalle()+" "+reqPagoServicioRequestVO.getColonia()+" "+reqPagoServicioRequestVO.getCiudad()+" "+reqPagoServicioRequestVO.getEstado()+" "+reqPagoServicioRequestVO.getCodigoPostal()+" "+reqPagoServicioRequestVO.getTelefono());
				
				servicio = mapper.ConsultaServicioByIdProducto(resPagoServicio.getIdProductoTerceros());
				
				LOGGER.info("servicio.getH2h(): "+servicio.getH2h());
				
				reqPagoServicioRequestVO.setH2h(servicio.getH2h());
				
				
				//H2h = false
				if (reqPagoServicioRequestVO.getH2h().equals("0")) {
					
					

					if (resPagoServicio.getClave().equals("0")) {
						
						pagoServicioVO = new PagoServicioVO(Integer.parseInt(resPagoServicio.getClave()), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal(),reqPagoServicioRequestVO.getRfc(),reqPagoServicioRequestVO.getCalle(),reqPagoServicioRequestVO.getColonia(),reqPagoServicioRequestVO.getCiudad(),reqPagoServicioRequestVO.getEstado(),reqPagoServicioRequestVO.getCodigoPostal(),reqPagoServicioRequestVO.getTelefono());
						pagoServicioVO.getPagoServicio().setStatus(0);
						LOGGER.info("resPagoServicio.getPrincipalLocal(): "+resPagoServicio.getPrincipalLocal());
						
					}
					
					else if  (resPagoServicio.getClave().equals("23") || resPagoServicio.getClave().equals("24")) {
						LOGGER.info("El usuario o el passwd son incorrectos, favor de verificar el dato enviado.");
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
						pagoServicioVO.getPagoServicio().setStatus(-2);
					
					}
					else {				
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());						
						pagoServicioVO.getPagoServicio().setStatus(-1);
						pagoServicioVO.getPagoServicio().setPrincipalPesos("");

					}					
					
				}
				
				
				//H2h = true
				else if (reqPagoServicioRequestVO.getH2h().equals("1")) {
					
					//prueba controlada 
					/*Inicio de prueba*/
					//resPagoServicio.setClave("1");
					/*Fin prueba */
					if (resPagoServicio.getClave().equals("0")) {
						
						//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
						//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
						pagoServicioVO = new PagoServicioVO(Integer.parseInt(resPagoServicio.getClave()), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal(),reqPagoServicioRequestVO.getRfc(),reqPagoServicioRequestVO.getCalle(),reqPagoServicioRequestVO.getColonia(),reqPagoServicioRequestVO.getCiudad(),reqPagoServicioRequestVO.getEstado(),reqPagoServicioRequestVO.getCodigoPostal(),reqPagoServicioRequestVO.getTelefono());
						pagoServicioVO.getPagoServicio().setStatus(0);
					}					
					else if  (resPagoServicio.getClave().equals("1")) {
						
						LOGGER.info("ES IGUAL A 1");
						ReqEstatusServicioVO reqEstatusServicioVO = new ReqEstatusServicioVO();
						reqEstatusServicioVO.setFolioCliente(resPagoServicio.getFolioCliente());
						reqEstatusServicioVO.setFolioTelecomm(resPagoServicio.getFolioTelecomm());	
						++intento; 
						reqEstatusServicioVO.setIntento((String.valueOf(intento)));
						pagoServicioEstatusVO = this.getEstatusPS(reqEstatusServicioVO);
						
						LOGGER.info("/*******************************************************/");
						LOGGER.info("pagoServicioEstatusVO.getIdError() "+pagoServicioEstatusVO.getIdError());
						/*prueba controlada*/
						//pagoServicioEstatusVO.setClave("2");
						
						/*fin prueba controlada*/
						if (pagoServicioEstatusVO.getIdError()== 0) {
							
							LOGGER.info("ESTATUS 0");
							//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
							//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
							/*Coloco la clave de pagoServicioEstatusVO ya que el metodo getEstatusPS la clave = 0 deberia responder 0 en vez de 1 que es la clave pendiente*/
							pagoServicioVO = new PagoServicioVO(pagoServicioEstatusVO.getIdError(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal(),reqPagoServicioRequestVO.getRfc(),reqPagoServicioRequestVO.getCalle(),reqPagoServicioRequestVO.getColonia(),reqPagoServicioRequestVO.getCiudad(),reqPagoServicioRequestVO.getEstado(),reqPagoServicioRequestVO.getCodigoPostal(),reqPagoServicioRequestVO.getTelefono());
							/*Le coloco Status == 0 ya que cuando fui contra el metodo getEstatusPS dio clave == 0*/
							pagoServicioVO.getPagoServicio().setStatus(0);
							
						}
						// este else es mientras resulevo lo del ciclo que esta abajo
						else{
							pagoServicioVO.setIdError(-1);
							pagoServicioVO.setMensajeError("Error, operacion denegada");						
							pagoServicioVO.getPagoServicio().setStatus(-1);
							pagoServicioVO.getPagoServicio().setPrincipalPesos("");
						}
						
						
						long tiempoInicio = System.currentTimeMillis();
						operacionPendiente = ((pagoServicioEstatusVO.getIdError() ==2) == true);
						/*Comente mientras resuelvo lo de intentar por 80 segundos*/
						while (operacionPendiente) {							
							operacionPendiente = false;	
							pagoServicioEstatusVO = new PagoServicioVO();
//							//Clave == 2 Operacion Pendiente
							LOGGER.info("ENTRE AL WHILE");
							intento = Integer.parseInt(reqEstatusServicioVO.getIntento());
							++intento;
							reqEstatusServicioVO.setIntento((String.valueOf(intento)));
							pagoServicioEstatusVO = this.getEstatusPS(reqEstatusServicioVO);
//							/*prueba controlada*/
							//pagoServicioEstatusVO.setClave("2");	
//							/*fin prueba controlada*/
							if (pagoServicioEstatusVO.getIdError() == 2) {
//								
								long totalTiempo = (System.currentTimeMillis() - tiempoInicio)/1000;
								LOGGER.info("totalTiempo: "+totalTiempo);
//								// Solo se puede volver a ejecutar siempre y cuando la respuesta sea
//								// igual a 2 el tiempo de ejecucion del metodo getEstatus menor o igual
//								// a 80 segundos
								if (totalTiempo <= 80)	{
									operacionPendiente = true;
								}
								else {
									LOGGER.info("EL TIEMPO ES MAYOR A 80 SEGUNDOS");
									pagoServicioVO = new PagoServicioVO();
//									pagoServicioVO = new PagoServicioVO();																		
									operacionPendiente = false;
									pagoServicioEstatusVO.getPagoServicio().setNombre(reqPagoServicioRequestVO.getNombre());
									pagoServicioEstatusVO.getPagoServicio().setApellidoPaterno(reqPagoServicioRequestVO.getApPat());
									pagoServicioEstatusVO.getPagoServicio().setApellidoMaterno(reqPagoServicioRequestVO.getApMat());
									pagoServicioEstatusVO.getPagoServicio().setRfc(reqPagoServicioRequestVO.getRfc());
									pagoServicioEstatusVO.getPagoServicio().setCalle(reqPagoServicioRequestVO.getCalle());
									pagoServicioEstatusVO.getPagoServicio().setColonia(reqPagoServicioRequestVO.getColonia());
									pagoServicioEstatusVO.getPagoServicio().setCiudad(reqPagoServicioRequestVO.getCiudad());
									pagoServicioEstatusVO.getPagoServicio().setEstado(reqPagoServicioRequestVO.getEstado());
									pagoServicioEstatusVO.getPagoServicio().setCodigoPostal(reqPagoServicioRequestVO.getCodigoPostal());
									pagoServicioEstatusVO.getPagoServicio().setTelefono(reqPagoServicioRequestVO.getTelefono());
									pagoServicioEstatusVO.getPagoServicio().setFechaNacimiento(reqPagoServicioRequestVO.getFechaNacimiento());
									pagoServicioVO =  pagoServicioEstatusVO;
									pagoServicioVO.getPagoServicio().setOperador(reqPagoServicioRequestVO.getOperador());
									//como excedio los 80 seg se debe colocar como operacion exitosa
									pagoServicioVO.setIdError(0);
								}
							}
							else{
								
								LOGGER.info("pagoServicioEstatusVO.getIdError(): "+pagoServicioEstatusVO.getIdError());
								pagoServicioEstatusVO.getPagoServicio().setNombre(reqPagoServicioRequestVO.getNombre());
								pagoServicioEstatusVO.getPagoServicio().setApellidoPaterno(reqPagoServicioRequestVO.getApPat());
								pagoServicioEstatusVO.getPagoServicio().setApellidoMaterno(reqPagoServicioRequestVO.getApMat());
								pagoServicioEstatusVO.getPagoServicio().setRfc(reqPagoServicioRequestVO.getRfc());
								pagoServicioEstatusVO.getPagoServicio().setCalle(reqPagoServicioRequestVO.getCalle());
								pagoServicioEstatusVO.getPagoServicio().setColonia(reqPagoServicioRequestVO.getColonia());
								pagoServicioEstatusVO.getPagoServicio().setCiudad(reqPagoServicioRequestVO.getCiudad());
								pagoServicioEstatusVO.getPagoServicio().setEstado(reqPagoServicioRequestVO.getEstado());
								pagoServicioEstatusVO.getPagoServicio().setCodigoPostal(reqPagoServicioRequestVO.getCodigoPostal());
								pagoServicioEstatusVO.getPagoServicio().setTelefono(reqPagoServicioRequestVO.getTelefono());
								pagoServicioEstatusVO.getPagoServicio().setFechaNacimiento(reqPagoServicioRequestVO.getFechaNacimiento());
								pagoServicioVO =  pagoServicioEstatusVO;
								pagoServicioVO.getPagoServicio().setOperador(reqPagoServicioRequestVO.getOperador());
							}
							
						}
						
						LOGGER.info("operacionPendiente "+operacionPendiente);
						
//						if (operacionPendiente == false) {
//							LOGGER.info("ENTRE AL IF");
//							pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
//						}
						
					}
					
					else if  (resPagoServicio.getClave().equals("23") || resPagoServicio.getClave().equals("24")) {
						LOGGER.info("El usuario o el passwd son incorrectos, favor de verificar el dato enviado.");
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
						pagoServicioVO.getPagoServicio().setStatus(-2);
					
					}
					else {				
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
						pagoServicioVO.getPagoServicio().setStatus(-1);
						pagoServicioVO.getPagoServicio().setPrincipalPesos("");
					}
					
				}
				
				if ((pagoServicioVO.getPagoServicio().getStatus() == 0) || (pagoServicioVO.getPagoServicio().getStatus() == -1)) {
					
					LOGGER.info("getStatus() == 0 o  getStatus() == -1");
					bitacoraVO = insertaBitacoraTransaccion(pagoServicioVO,reqPagoServicioRequestVO);
					pagoServicioVO.getPagoServicio().setIdBitacora(bitacoraVO.getIdBitacora());
					mapper.insertBitacoraPagoServicio(pagoServicioVO);
				}
				
			}
			
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR getPeticionPS - ERROR: "+e);

		} 		
		finally {
			LOGGER.info("antes del return pagoServicioVO "+pagoServicioVO.getIdError()+"  "+pagoServicioVO.getMensajeError());
			return pagoServicioVO;
		}
		
		
	} 
	/****************************************************************************************/
	
	public PagoServicioVO getEstatusPS(ReqEstatusServicioVO reqEstatusServicioVO) {
		
		LOGGER.info("getEstatusPS");
		GetEstatusPS getEstatusPS = new GetEstatusPS();
		ReqConsultaServicio reqConsultaServicio = new ReqConsultaServicio();
		ObjectFactory factory = new ObjectFactory();
		GetEstatusPSResponse response = null;
		ResPagoServicio resPagoServicio = new ResPagoServicio();
		PagoServicioVO pagoServicioVO = new PagoServicioVO();
		
		ReqEstatusServicio reqEstatusServicio = new ReqEstatusServicio();
		ValidacionVO validacionVO;
		
		try{
			 
			validacionVO = service.validarEstatusPS(reqEstatusServicioVO);
			if (validacionVO.isValida())	{
			
				getEstatusPS.setAutentificacion(service.getAutentifica());
				
				reqEstatusServicio.setFolioCliente(reqEstatusServicioVO.getFolioCliente());
				LOGGER.info("reqEstatusServicioVO.getFolioTelecomm(): "+reqEstatusServicioVO.getFolioTelecomm());
				
				if (reqEstatusServicioVO.getFolioTelecomm().equals("")) {
					reqEstatusServicio.setFolioTelecomm("0");
					LOGGER.info("IGUAL A 0");
				}
				else {
					reqEstatusServicio.setFolioTelecomm(reqEstatusServicioVO.getFolioTelecomm());
					LOGGER.info("DIFERENTE A 0 ");
				}
				LOGGER.info("reqEstatusServicioVO.getIntento()" +reqEstatusServicioVO.getIntento());
				reqEstatusServicio.setIntento(reqEstatusServicioVO.getIntento());
				
				getEstatusPS.setReqEstatusServicio(reqEstatusServicio);
				
				LOGGER.info("ANTES DE ENVIAR AL WS");
				
				response = (GetEstatusPSResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetEstatusPS(getEstatusPS));
				
				LOGGER.info("DESPUES DE ENVIAR AL WS");
				LOGGER.info("response: "+response);
				resPagoServicio = response.getReturn();
				
				if (resPagoServicio.getClave().equals("0")) {
						pagoServicioVO = new PagoServicioVO(Integer.parseInt(resPagoServicio.getClave()), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());						
									
				}
				
				else if ((resPagoServicio.getClave().equals("23")) || (resPagoServicio.getClave().equals("24"))) {				
					pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
					pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
				}
				
				else{				
					pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
					pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
				}
			}
			else{
				pagoServicioVO = service.setPeticionVO(validacionVO);
				
			}
			
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR getEstatusPS - ERROR: "+desc);

		} 	
		
		return pagoServicioVO;
		
		
	}
	
	
	public Bitacora insertaBitacoraTransaccion(PagoServicioVO pagoServicioVO,ReqPagoServicioRequestVO reqPagoServicioRequestVO) {
		Bitacora bitacora = new Bitacora();
		try {
			
			LOGGER.info("insertaBitacoraTransaccion");
			/** 
			 * id_usuario, id_proveedor, id_producto, bit_fecha, bit_hora, bit_concepto,
			bit_cargo, bit_ticket, bit_no_autorizacion, bit_codigo_error, bit_card_id, bit_status,
			imei, destino, tarjeta_compra, tipo, software, modelo, wkey, pase	
			 */
			
			
			
			LOGGER.info("insertaBitacoraTransaccion 2");
			bitacora.setIdUsuario(reqPagoServicioRequestVO.getIdUsuario());
			LOGGER.info("insertaBitacoraTransaccion 3 ");
			bitacora.setConcepto("Telecom pago de servicio ");
			LOGGER.info("insertaBitacoraTransaccion 4.1");
			
			LOGGER.info("pagoServicioVO.getPrincipalPesos(): "+pagoServicioVO.getPagoServicio().getPrincipalPesos());
			
			if (!pagoServicioVO.getPagoServicio().getPrincipalPesos().equals("")){
				bitacora.setCargo(Double.valueOf(pagoServicioVO.getPagoServicio().getPrincipalPesos()));
			}
			
			LOGGER.info("insertaBitacoraTransaccion 5");
			bitacora.setCodigoError(String.valueOf(pagoServicioVO.getIdError()));
			LOGGER.info("insertaBitacoraTransaccion 6");
			bitacora.setStatus(pagoServicioVO.getPagoServicio().getStatus());
			bitacora.setImei(reqPagoServicioRequestVO.getImei());
			LOGGER.info("insertaBitacoraTransaccion 7");
			bitacora.setSoftware(reqPagoServicioRequestVO.getSoftware());
			LOGGER.info("insertaBitacoraTransaccion 8");
			bitacora.setModelo(reqPagoServicioRequestVO.getModelo());
			LOGGER.info("insertaBitacoraTransaccion 9");
			bitacora.setWkey(reqPagoServicioRequestVO.getWkey());
			LOGGER.info("insertaBitacoraTransaccion 10");
			bitacora.setPase(0);
			LOGGER.info("insertaBitacoraTransaccion 11");
			mapper.insertaBitacoraTransaccionPS(bitacora);			
			
			
			
			LOGGER.info("TRANSACCION REGISTRADA: ID BITACORA - "+bitacora.getIdBitacora());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitacora;
	}

	
	/****************************************************************************************/

	public String quitarAcento(String cadena)	{
		LOGGER.info("quitarAcento");
		try{
			if(cadena != null){
				LOGGER.info("entre al if");
				cadena = cadena.replaceAll("á", "a");
				cadena = cadena.replaceAll("é", "e");
				cadena = cadena.replaceAll("í", "i");
				cadena = cadena.replaceAll("ó", "o");
				cadena = cadena.replaceAll("ú", "u");
			}
		}catch(Exception e){
			LOGGER.error("Error al quitar acentos: " + e.getMessage());
		}
		LOGGER.info("cadena: "+cadena);
		return cadena;
	} 

}
