package com.addcel.telecom.pago.servicios.model.vo;

public class PagoTelecomDetalle {

	private long idTransaccion;
	
	private String descripcion;
	
	private int tipoTarjeta;
	
	private double total;
	
	private double comision;
	
	private String referencia;
	
	private String emisor;
	
	private String codigoRespuesta;
	
	private String idTransaccionAntad;
	
	private String numAutorizacion;
	
	
	/**Ajuste para telecom*/
    private String apMat ="";
    private String apPat ="";
    private String calle ="";
    private String ciudad ="";
    private String codigoPostal ="";
    private String colonia ="";
    private String disponible1 ="";
    private String disponible2 ="";
    private String disponible3 ="";
    private String estado ="";
    private String fechaNacimiento ="";
    private String fechaOperacion ="";
    private String folioCliente ="";
    private String h2h ="";
    private String idProductoTerceros ="";
    private String nombre ="";
    private String operador ="";
    private String precio ="";
    private String precio2 ="";
    private String principalLocal ="";
    private String principalPesos ="";
    private String referencia1 ="";
    private String referencia2 ="";
    private String referencia3 ="";
    private String rfc ="";
    private String telefono ="";
    private String terminal ="";
    private String tienda ="";
    private String tipoCambio ="";
    private String tipoMoneda ="";
    
    
    
	public String getApMat() {
		return apMat;
	}

	public void setApMat(String apMat) {
		this.apMat = apMat;
	}

	public String getApPat() {
		return apPat;
	}

	public void setApPat(String apPat) {
		this.apPat = apPat;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getDisponible1() {
		return disponible1;
	}

	public void setDisponible1(String disponible1) {
		this.disponible1 = disponible1;
	}

	public String getDisponible2() {
		return disponible2;
	}

	public void setDisponible2(String disponible2) {
		this.disponible2 = disponible2;
	}

	public String getDisponible3() {
		return disponible3;
	}

	public void setDisponible3(String disponible3) {
		this.disponible3 = disponible3;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getFolioCliente() {
		return folioCliente;
	}

	public void setFolioCliente(String folioCliente) {
		this.folioCliente = folioCliente;
	}

	public String getH2h() {
		return h2h;
	}

	public void setH2h(String h2h) {
		this.h2h = h2h;
	}

	public String getIdProductoTerceros() {
		return idProductoTerceros;
	}

	public void setIdProductoTerceros(String idProductoTerceros) {
		this.idProductoTerceros = idProductoTerceros;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getPrecio2() {
		return precio2;
	}

	public void setPrecio2(String precio2) {
		this.precio2 = precio2;
	}

	public String getPrincipalLocal() {
		return principalLocal;
	}

	public void setPrincipalLocal(String principalLocal) {
		this.principalLocal = principalLocal;
	}

	public String getPrincipalPesos() {
		return principalPesos;
	}

	public void setPrincipalPesos(String principalPesos) {
		this.principalPesos = principalPesos;
	}

	public String getReferencia1() {
		return referencia1;
	}

	public void setReferencia1(String referencia1) {
		this.referencia1 = referencia1;
	}

	public String getReferencia2() {
		return referencia2;
	}

	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	public String getReferencia3() {
		return referencia3;
	}

	public void setReferencia3(String referencia3) {
		this.referencia3 = referencia3;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getTienda() {
		return tienda;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

	public String getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	/**Fin de Ajuste**/

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getIdTransaccionAntad() {
		return idTransaccionAntad;
	}

	public void setIdTransaccionAntad(String idTransaccionAntad) {
		this.idTransaccionAntad = idTransaccionAntad;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	
}
