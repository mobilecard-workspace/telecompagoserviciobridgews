package com.addcel.telecom.pago.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReqConsultaServicioRequestVO {

    private String disponible1 ="";
    private String disponible2 ="";
    private String disponible3 = "";
    private String fechaOperacion ="";
    private String idProductoTerceros ="";
    private String principalPesos ="";
    private String referencia1 ="";
    private String referencia2 ="";
    private String referencia3 ="";
    
	public String getDisponible1() {
		return disponible1;
	}
	public void setDisponible1(String disponible1) {
		this.disponible1 = disponible1;
	}
	public String getDisponible2() {
		return disponible2;
	}
	public void setDisponible2(String disponible2) {
		this.disponible2 = disponible2;
	}
	public String getDisponible3() {
		return disponible3;
	}
	public void setDisponible3(String disponible3) {
		this.disponible3 = disponible3;
	}
	public String getFechaOperacion() {
		return fechaOperacion;
	}
	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	public String getIdProductoTerceros() {
		return idProductoTerceros;
	}
	public void setIdProductoTerceros(String idProductoTerceros) {
		this.idProductoTerceros = idProductoTerceros;
	}
	public String getPrincipalPesos() {
		return principalPesos;
	}
	public void setPrincipalPesos(String principalPesos) {
		this.principalPesos = principalPesos;
	}
	public String getReferencia1() {
		return referencia1;
	}
	public void setReferencia1(String referencia1) {
		this.referencia1 = referencia1;
	}
	public String getReferencia2() {
		return referencia2;
	}
	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}
	public String getReferencia3() {
		return referencia3;
	}
	public void setReferencia3(String referencia3) {
		this.referencia3 = referencia3;
	}
}
