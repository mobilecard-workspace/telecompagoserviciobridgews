package com.addcel.telecom.pago.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CatalogoLeyendaVO extends BaseVO{
	
	
	/*private String clave;
	
	private String mensaje;*/
	
	@SerializedName("servicio")
	@Expose
	private List<ServicioLeyendaVO> servicios;
	
	
	/*public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}*/

	public List<ServicioLeyendaVO> getServicios() {
		return servicios;
	}

	public void setServicios(List<ServicioLeyendaVO> servicios) {
		this.servicios = servicios;
	}

}
