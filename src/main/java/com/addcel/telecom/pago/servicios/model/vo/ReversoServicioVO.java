package com.addcel.telecom.pago.servicios.model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReversoServicioVO  extends BaseVO{
	
	@SerializedName("ReversoServicio")
	@Expose
	private ReversoServicio reversoServicio;
	
	
	public ReversoServicioVO()	{
		
		this.reversoServicio = new ReversoServicio();
	}
	
	public ReversoServicioVO(int clave,String mensaje,String folioCliente,String folioTelecomm,String idProductoTerceros,String principalPesos,String fechaOperacion,int esReverso)	{
		
		this.idError = clave;
		this.mensajeError = mensaje;
		this.reversoServicio = new ReversoServicio(folioCliente, folioTelecomm, idProductoTerceros, principalPesos, fechaOperacion,esReverso);
		
	}
	
	public ReversoServicio getReversoServicio() {
		return reversoServicio;
	}

	public void setReversoServicio(ReversoServicio reversoServicio) {
		this.reversoServicio = reversoServicio;
	}
}
