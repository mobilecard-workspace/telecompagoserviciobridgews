package com.addcel.telecom.pago.servicios.model.vo;

public class ReversoServicio {

	private long idBitacora;	
    private String folioCliente;
    private String folioTelecomm;
    private String idProductoTerceros;    
    private String principalPesos;
    private int status; 
    private String fechaOperacion;
    private int esReverso;
  
	public ReversoServicio() {
    	
	}
    public ReversoServicio(String folioCliente,String folioTelecomm,String idProductoTerceros,String principalPesos,String fechaOperacion,int esReverso) {
    	
    	this.folioCliente = folioCliente;
    	this.folioTelecomm = folioTelecomm;
    	this.idProductoTerceros = idProductoTerceros;    	
    	this.principalPesos = principalPesos;
    	this.fechaOperacion = fechaOperacion;
    	this.esReverso = esReverso;
    	
    }
    
    public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}    

	public String getFolioCliente() {
		return folioCliente;
	}
	public void setFolioCliente(String folioCliente) {
		this.folioCliente = folioCliente;
	}
	public String getFolioTelecomm() {
		return folioTelecomm;
	}
	public void setFolioTelecomm(String folioTelecomm) {
		this.folioTelecomm = folioTelecomm;
	}
	public String getIdProductoTerceros() {
		return idProductoTerceros;
	}
	public void setIdProductoTerceros(String idProductoTerceros) {
		this.idProductoTerceros = idProductoTerceros;
	}

	public String getPrincipalPesos() {
		return principalPesos;
	}
	
	public void setPrincipalPesos(String principalPesos) {
		this.principalPesos = principalPesos;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
    
	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}
	public int getEsReverso() {
		return esReverso;
	}
	public void setEsReverso(int esReverso) {
		this.esReverso = esReverso;
	}

}
