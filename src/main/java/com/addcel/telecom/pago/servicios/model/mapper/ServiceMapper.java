package com.addcel.telecom.pago.servicios.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.telecom.pago.servicios.model.vo.RuleVO;
import com.addcel.telecom.pago.servicios.model.vo.Servicio;
import com.addcel.telecom.pago.servicios.model.vo.ServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.Bitacora;
import com.addcel.telecom.pago.servicios.model.vo.PagoServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.ReversoServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.UsuarioVO;
import com.addcel.telecom.pago.servicios.model.vo.AfiliacionVO;
import com.addcel.telecom.pago.servicios.model.vo.BitacoraVO;
import com.addcel.telecom.pago.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.telecom.pago.servicios.model.vo.PagoTelecomDetalle;
import com.addcel.telecom.pago.servicios.model.vo.PagoTelecomInfo;
import com.addcel.telecom.pago.servicios.model.vo.TBitacoraVO;



public interface ServiceMapper {
	
	
	
	public int insertBitacoraPagoServicio(PagoServicioVO pagoServicioVO);

	public int insertaBitacoraTransaccionPS(Bitacora bitacora);	
	
	
	public int insertBitacoraReversoServicio(ReversoServicioVO reversoServicioVO);
	
	public String getParametro(@Param(value = "clave") String valor);
	
	public UsuarioVO getUsuario(@Param(value = "id") long idUsuario, @Param(value="idTarjeta") int idTarjeta);	
	
	public RuleVO selectRule(@Param(value = "idUsuario") long idUsuario, @Param(value = "tarjeta") String tarjeta,
			@Param(value = "idProveedores") String idProveedores, @Param(value = "parametro") String parametro);
	
	public int getBloqueoTarjeta(@Param(value = "tarjeta") String tarjeta);	
	
	public AfiliacionVO buscaAfiliacion(@Param(value="id") String id);
	
	public int guardaTBitacoraPIN(@Param(value="idBitacora") long idBitacora, @Param(value="pin") String pin);
	
	public int insertaBitacoraTransaccion(BitacoraVO bitacora);
	
	public int addBitacoraProsa(TBitacoraProsaVO b);
	
	public void insertaPagoTelecomDetalle(PagoTelecomDetalle antadDetalle);
	
	public String buscarTBitacoraPIN(@Param(value="idBitacora")String idBitacora);
	
	public PagoTelecomInfo buscarDetallePago(@Param(value="idBitacora") String idBitacora);
	
	public int updateBitacora(TBitacoraVO b);
	
	public int updateBitacoraProsa(TBitacoraProsaVO b);
	
	public double obtenDivisa(@Param(value = "id") String string);
	
	public Servicio ConsultaServicioByIdProducto(@Param(value = "idProducto") String  idProducto);

}
