package com.addcel.telecom.pago.servicios.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.axis.AxisFault;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import com.google.gson.Gson;
import com.sun.xml.bind.v2.runtime.reflect.opt.Const;

import java.util.regex.*;

import com.addcel.telecom.pago.client.telecom.wsservicios.Autentifica;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetCatalogoPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetCatalogoPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetConsultaPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetConsultaPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetEco;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetEcoResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetEstatusPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetEstatusPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetLeyendaPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetLeyendaPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetPeticionPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetPeticionPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetReversoPS;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetReversoPSResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetSaldo;
import com.addcel.telecom.pago.client.telecom.wsservicios.GetSaldoResponse;
import com.addcel.telecom.pago.client.telecom.wsservicios.ObjectFactory;
import com.addcel.telecom.pago.client.telecom.wsservicios.ReqConsultaServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ReqEstatusServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ReqPagoServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ReqReversoServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResCatalogo;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResConsultaServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResLeyenda;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResPagoServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResReversoServicio;
import com.addcel.telecom.pago.client.telecom.wsservicios.ResSaldoServicio;
import com.addcel.telecom.pago.servicios.model.mapper.ServiceMapper;
import com.addcel.telecom.pago.servicios.model.vo.Bitacora;
import com.addcel.telecom.pago.servicios.model.vo.CatalogoLeyendaVO;
import com.addcel.telecom.pago.servicios.model.vo.CatalogoVO;
import com.addcel.telecom.pago.servicios.model.vo.ConsultaPS;
import com.addcel.telecom.pago.servicios.model.vo.ConsultapsVO;
import com.addcel.telecom.pago.servicios.model.vo.PagoServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqConsultaServicioRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqEcoVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqEstatusServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqPagoServicioRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqReversoServicioRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.ReversoServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.SaldoVO;
import com.addcel.telecom.pago.servicios.model.vo.EcoVO;
import com.addcel.telecom.pago.servicios.model.vo.TiempoAireEnum;
import com.addcel.telecom.pago.servicios.model.vo.ValidacionVO;
import com.addcel.telecom.pago.servicios.utils.Constantes;
import com.addcel.telecom.pago.servicios.utils.UtilsService;



@Service
public class payService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(payService.class);
	private Autentifica autentifica;
    private Gson gson = new Gson();
	@Autowired
	private ServiceMapper mapper;
	

	
	
	private UtilsService utilsService = new UtilsService(); 
	

	
	//PROD https://collectpay.princetonecom.com/pa/xml/createBankPayment2.do
	// QA https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do
	
//	@Autowired
//	private ServiceMapper mapper;
//	
//	private static PagoServicio_Service proxy = null;
//	
//	
//	
//	static {
//		proxy = new PagoServicio_Service();
//	}
	
	@Autowired
    @Qualifier("webServicePagoServicioBridge")
    private transient WebServiceTemplate webServiceTemplate;
//	
	public EcoVO getEco(ReqEcoVO reqEcoVO) {
		
		GetEcoResponse response = null;
		GetEco getEcoVal = new GetEco();
		EcoVO ecoVO = new EcoVO();
		try {
			getCurrentTimeStamp();
			LOGGER.info("1");
			ObjectFactory factory = new ObjectFactory();
			getEcoVal.setEco(reqEcoVO.getMensajeDesde());
			response = (GetEcoResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createGetEco(getEcoVal));
			LOGGER.info("2");
			LOGGER.info("response: "+response);
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR GET ECO - ERROR: "+desc);
//			recipient.setIdError(-1);
//			recipient.setMensajeError(desc);
		} finally{
			LOGGER.info("finally");
		} 
		LOGGER.info("response.getEco(): "+response.getReturn());
		
		
		ecoVO.setMensaje(response.getReturn());
		return ecoVO;
		
	}
	
	
	public CatalogoVO getCatalogoPS() {
		
		
		GetCatalogoPSResponse response = null;
		GetCatalogoPS getCatalogoPs = new GetCatalogoPS();
		Autentifica autentificacion = new Autentifica(); 
		CatalogoVO catalogoVO = new CatalogoVO();
		ResCatalogo resCatalogo = null;
		JSONObject catalogoObj = new JSONObject();
		
		try {
			LOGGER.info("getCatalogoPS");
			ObjectFactory factory = new ObjectFactory();
//			autentificacion.setCodigo("2002");
//			autentificacion.setSocio("AddCel");
//			autentificacion.setUsuario("u5U4r104C");
//			autentificacion.setPasswd("p455@w0rd");
			
			getCatalogoPs.setAutentificacion(this.getAutentifica());
			
			
			response = (GetCatalogoPSResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetCatalogoPS(getCatalogoPs));
					
			LOGGER.info("2");
			LOGGER.info("response: "+response);
			LOGGER.info("response.getReturn(): "+response.getReturn());
			
			resCatalogo = response.getReturn();
			
			
			
			if (resCatalogo != null) {
				LOGGER.info("resCatalogo.getCatalogoXML(): "+resCatalogo.getCatalogoXML());
				LOGGER.info("response.getReturn().getMensaje(): "+resCatalogo.getMensaje());
				LOGGER.info("response.getReturn().getClave: "+resCatalogo.getClave());
				LOGGER.info("catalogoXML 2: "+(resCatalogo.getCatalogoXML()==null));
				
				
				if (resCatalogo.getCatalogoXML()!=null) {
					String catalogoXML = resCatalogo.getCatalogoXML();
					LOGGER.info("catalogoXML: "+catalogoXML);
					
					JSONObject xmlJSONObj = this.toJSONObject(catalogoXML);
					LOGGER.info("xmlJSONObj: "+xmlJSONObj);
					if (xmlJSONObj != null) {					
						catalogoObj = this.getJSONObject(xmlJSONObj, "catalogo");
						
					}				
				}
				
				if (resCatalogo.getClave().equals("0")) {
					catalogoVO = gson.fromJson(catalogoObj.toString(), CatalogoVO.class);
					catalogoVO.setIdError(Integer.parseInt(resCatalogo.getClave()));
					catalogoVO.setMensajeError(resCatalogo.getMensaje());
					
				}
				else if ((resCatalogo.getClave().equals("23")) || (resCatalogo.getClave().equals("24"))){
					catalogoVO.setIdError(Integer.parseInt(resCatalogo.getClave()));
					catalogoVO.setMensajeError(resCatalogo.getMensaje());
				}
				else {
					catalogoVO.setIdError(Integer.parseInt(resCatalogo.getClave()));
					catalogoVO.setMensajeError(resCatalogo.getMensaje());
				} 
			}
			

			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR GET ECO - ERROR: "+desc);
//			recipient.setIdError(-1);
//			recipient.setMensajeError(desc);
		}
		
		return catalogoVO;
		
	}
	
	
	public CatalogoLeyendaVO getLeyendaPS()  {
		
		LOGGER.info("getLeyendaPS");
		GetLeyendaPSResponse response = null;
		GetLeyendaPS getLeyendaPs = new GetLeyendaPS();
		Autentifica autentificacion = new Autentifica(); 
		CatalogoLeyendaVO catalogoLeyendaVO = new CatalogoLeyendaVO();
		ResLeyenda resLeyenda = null;
		JSONObject catalogoObj = new JSONObject();
		
		try {
			LOGGER.info("1");
			ObjectFactory factory = new ObjectFactory();
//			autentificacion.setCodigo("2002");
//			autentificacion.setSocio("AddCel");
//			autentificacion.setUsuario("u5U4r104C");
//			autentificacion.setPasswd("p455@w0rd");
			
			getLeyendaPs.setAutentificacion(this.getAutentifica());			
			
			response = (GetLeyendaPSResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetLeyendaPS(getLeyendaPs));
													
			LOGGER.info("2");
			LOGGER.info("response: "+response);
			
			LOGGER.info("response.getReturn(): "+response.getReturn());
			
			resLeyenda = response.getReturn();
			
			LOGGER.info("response.getReturn(): "+response.getReturn().toString());
			
			if (resLeyenda != null) {
				LOGGER.info("NO ES NULO");
				
				LOGGER.info("response.getReturn().getLeyendaXML(): "+response.getReturn().getLeyendaXML());
				if (resLeyenda.getLeyendaXML()!=null) {					
					LOGGER.info("response.getReturn().getMensaje(): "+response.getReturn().getMensaje());
					LOGGER.info("response.getReturn().getClave: "+response.getReturn().getClave());
					String catalogoXML = response.getReturn().getLeyendaXML();
					
					JSONObject xmlJSONObj = this.toJSONObject(catalogoXML);
					LOGGER.info("xmlJSONObj.toString(): "+xmlJSONObj.toString());
					
					if (xmlJSONObj != null) {	
						catalogoObj = this.getJSONObject(xmlJSONObj, "catalogo");
					}
					
				}
				
				LOGGER.info("resLeyenda.getClave: "+resLeyenda.getClave());
				if (resLeyenda.getClave().equals("0")) {
					
					catalogoLeyendaVO = gson.fromJson(catalogoObj.toString(), CatalogoLeyendaVO.class);
					catalogoLeyendaVO.setIdError(Integer.parseInt(resLeyenda.getClave()));
					catalogoLeyendaVO.setMensajeError(resLeyenda.getMensaje());
				}
				else if ((resLeyenda.getClave().equals("23")) || (resLeyenda.getClave().equals("24"))){
					
					catalogoLeyendaVO.setIdError(Integer.parseInt(resLeyenda.getClave()));					
					catalogoLeyendaVO.setMensajeError(resLeyenda.getMensaje());
					
				}
				else {
					catalogoLeyendaVO.setIdError(Integer.parseInt(resLeyenda.getClave()));					
					catalogoLeyendaVO.setMensajeError(resLeyenda.getMensaje());
				}			
				
				
				
			}			
			
		} catch (Exception ex) {
			LOGGER.error("ERROR GET ECO - ERROR: "+ex.toString());			
		} 
		
		
		return catalogoLeyendaVO;
		
	}
	
	
	public SaldoVO getSaldo() {
		
		LOGGER.info("getSaldo");
		GetSaldoResponse response = null;
		GetSaldo getSaldo = new GetSaldo();
		Autentifica autentificacion = new Autentifica(); 
		SaldoVO saldoVO = new SaldoVO();
		UtilsService utilsService = new UtilsService();
		ResSaldoServicio resSaldoServicio = new ResSaldoServicio();
		try {
			LOGGER.info("1");
			ObjectFactory factory = new ObjectFactory();
//			autentificacion.setCodigo("2002");
//			autentificacion.setSocio("AddCel");
//			autentificacion.setUsuario("u5U4r104C");
//			autentificacion.setPasswd("p455@w0rd");
			
			getSaldo.setAutentificacion(this.getAutentifica());
			
			
			response = (GetSaldoResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetSaldo(getSaldo));
					
			LOGGER.info("2");
			LOGGER.info("response: "+response);
			resSaldoServicio = response.getReturn();
			
			LOGGER.info("response.getReturn(): "+response.getReturn().toString());
			
			if (resSaldoServicio.getClave().equals("0")) {
				saldoVO = new SaldoVO(resSaldoServicio.getClave(), resSaldoServicio.getMensaje(), resSaldoServicio.getSaldo());				
			}
			
			else if ((resSaldoServicio.getClave().equals("23")) || (resSaldoServicio.getClave().equals("24"))){
				
				//saldoVO.setClave(resSaldoServicio.getClave());
				saldoVO.setIdError(Integer.parseInt(resSaldoServicio.getClave()));
				saldoVO.setMensajeError(resSaldoServicio.getMensaje());
			}
			else {				
				saldoVO.setIdError(Integer.parseInt(resSaldoServicio.getClave()));
				saldoVO.setMensajeError(resSaldoServicio.getMensaje());
			}
				

			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR GET ECO - ERROR: "+desc);
//			recipient.setIdError(-1);
//			recipient.setMensajeError(desc);
		} 

		return saldoVO;
		
	}

	
	
	public ConsultapsVO getConsultaPS(ReqConsultaServicioRequestVO request) {
		
		LOGGER.info("getConsultaPS");
		GetConsultaPSResponse response = null;
		GetConsultaPS getConsultaPS = new GetConsultaPS(); 
		ReqConsultaServicio reqConsultaServicio = new ReqConsultaServicio();
		ConsultapsVO consultapsVO = new ConsultapsVO();
		ResConsultaServicio resConsultaServicio = new  ResConsultaServicio();
		ValidacionVO validacionVO = new ValidacionVO();
		String mensaje ="";
		int idProducto =0;
		
		LOGGER.info("request.getReferencia1()"+request.getReferencia1());	
		LOGGER.info("request.getIdProductoTerceros()"+request.getIdProductoTerceros());
		LOGGER.info("request.getPrincipalPesos()"+request.getPrincipalPesos());
		
		try {
			LOGGER.info("1");
			ObjectFactory factory = new ObjectFactory();		
			getConsultaPS.setAutentificacion(this.getAutentifica());
			
//			if ((request.getIdProductoTerceros() != "") && UtilsService.validarNumero(request.getIdProductoTerceros()))  
//				idProducto = utilsService.convertToInt(request.getIdProductoTerceros());			
				
			
			validacionVO = validacionIdProductoTerceros(request.getIdProductoTerceros());
			
			if (validacionVO.isValida()) {
				idProducto = utilsService.convertToInt(request.getIdProductoTerceros());
			}
			else	{
				LOGGER.info("no es valido");
				consultapsVO = setConsultaVO(validacionVO);
				return consultapsVO;
			}			
			

			ConsultapsVO consultapsVOConsulta = ConsultaServicio(idProducto,request);			
			
			
			//if (consultapsVOConsulta.getClave().equals("-1"))
			if (consultapsVOConsulta.getIdError() == -1)				
				return consultapsVOConsulta;
			
			
				
			reqConsultaServicio = setReqConsultaServicio(request);			
			getConsultaPS.setReqConsultaServicio(reqConsultaServicio);
			
			LOGGER.info("ANTES DE ENVIAR AL WS");
			
			response = (GetConsultaPSResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetConsultaPS(getConsultaPS));
			
			LOGGER.info("DESPUES DE ENVIAR AL WS");
			LOGGER.info("response: "+response);
			resConsultaServicio = response.getReturn();
			
			if (resConsultaServicio != null) {
				
				consultapsVO = 	this.getConsultapsVO(resConsultaServicio);
			
			}
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR getConsultaPS - ERROR: "+desc);
		} 		

		return consultapsVO;
		
	}
	
	
	public PagoServicioVO getPeticionPS(ReqPagoServicioRequestVO reqPagoServicioRequestVO) {
		LOGGER.info("getPeticionPS");
		GetPeticionPS getPeticionPS = new GetPeticionPS();
		PagoServicioVO pagoServicioVO = new PagoServicioVO();
		ObjectFactory factory = new ObjectFactory();
		ReqPagoServicio reqPagoServicio = new ReqPagoServicio(); 
		GetPeticionPSResponse response = null;
		ResPagoServicio resPagoServicio = new ResPagoServicio();
		PagoServicioVO pagoServicioEstatusVO = new PagoServicioVO();
		boolean operacionPendiente = false;
		ReqConsultaServicioRequestVO reqConsultaServicioRequestVO = new ReqConsultaServicioRequestVO();
		ConsultapsVO consultapsVO = new ConsultapsVO(); 
		Bitacora bitacoraVO = new Bitacora();
		ValidacionVO validacionVO = new ValidacionVO();
		int intento = 0;
		int idProducto =0;
		
		try{
			LOGGER.info(" idUsuario: "+Long.toString(reqPagoServicioRequestVO.getIdUsuario()));
			LOGGER.info("reqPagoServicioRequestVO.getImei(): "+reqPagoServicioRequestVO.getImei());
			String fecha = this.getCurrentTimeStamp();
			LOGGER.info("fecha: "+fecha);
			getPeticionPS.setAutentificacion(this.getAutentifica());
			
			//if ((reqPagoServicioRequestVO.getIdProductoTerceros() != "") && UtilsService.validarNumero(reqPagoServicioRequestVO.getIdProductoTerceros()))  
				//idProducto = utilsService.convertToInt(reqPagoServicioRequestVO.getIdProductoTerceros());
			
			validacionVO = validacionIdProductoTerceros(reqPagoServicioRequestVO.getIdProductoTerceros());
			
			if (validacionVO.isValida()) {
				idProducto = utilsService.convertToInt(reqPagoServicioRequestVO.getIdProductoTerceros());
			}
			else	{
				LOGGER.info("no es valido");
				pagoServicioVO = setPeticionVO(validacionVO);				
				return pagoServicioVO;
			}			

			pagoServicioVO = pagoServicio(idProducto,reqPagoServicioRequestVO);
			
			//pagoServicioVO = pagoServicio(idProducto, reqPagoServicioRequestVO.getReferencia1(),reqPagoServicioRequestVO.getReferencia2(),reqPagoServicioRequestVO.getReferencia3());
			
			if (pagoServicioVO.getIdError() ==-1) 
				return pagoServicioVO;
			
			if (pagoServicioVO.getPagoServicio().isConsultaWS()) {
				
				LOGGER.info("TRUE");			
				
//				reqConsultaServicioRequestVO = setReqConsultaServicio(request)
				
				reqPagoServicioRequestVO.setFechaOperacion(fecha);
				reqConsultaServicioRequestVO = setReqConsultaServicioRequestVO(reqPagoServicioRequestVO);
				consultapsVO = this.getConsultaPS(reqConsultaServicioRequestVO);
				
//				if (!consultapsVO.getClave().equals("0")) {
					
				if (consultapsVO.getIdError() != 0)	{
					pagoServicioVO.setIdError(consultapsVO.getIdError());
					pagoServicioVO.setMensajeError(consultapsVO.getMensajeError());
					
					return pagoServicioVO;
				}
				else{
					LOGGER.info("consultapsVO.getConsultaPS().getPrincipalPesos(): "+consultapsVO.getConsultaPS().getPrincipalPesos());
					reqPagoServicioRequestVO.setPrincipalPesos(consultapsVO.getConsultaPS().getPrincipalPesos());
					
					if (consultapsVO.getConsultaPS().getIdProductoTerceros().equals("210")) {
						reqPagoServicioRequestVO.setReferencia3(consultapsVO.getConsultaPS().getReferencia3());
					}
				}
			}			

			LOGGER.info("pagoServicioVO.isConsultaWS(): "+pagoServicioVO.getPagoServicio().isConsultaWS());
			
			LOGGER.info("reqPagoServicioRequestVO.getPrincipalLocal(): "+reqPagoServicioRequestVO.getPrincipalLocal());
			reqPagoServicio = setReqPagoServicio(reqPagoServicioRequestVO);			
			getPeticionPS.setReqPagoServicio(reqPagoServicio);
			
			LOGGER.info("ANTES DE ENVIAR AL WS");
			
			response = (GetPeticionPSResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createGetPeticionPS(getPeticionPS));
			
			LOGGER.info("DESPUES DE ENVIAR AL WS");
			LOGGER.info("response: "+response);
			
			resPagoServicio = response.getReturn();
			
			if (resPagoServicio != null) { 
				
			
				LOGGER.info("resPagoServicio.getClave():"+resPagoServicio.getClave());
				LOGGER.info("reqPagoServicioRequestVO.getH2h(): "+reqPagoServicioRequestVO.getH2h());
				
				
				if (reqPagoServicioRequestVO.getH2h().equals("false")) {
					
					
					
					if (resPagoServicio.getClave().equals("0")) {
						
						pagoServicioVO = new PagoServicioVO(Integer.parseInt(resPagoServicio.getClave()), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal(),reqPagoServicioRequestVO.getRfc(),reqPagoServicioRequestVO.getCalle(),reqPagoServicioRequestVO.getColonia(),reqPagoServicioRequestVO.getCiudad(),reqPagoServicioRequestVO.getEstado(),reqPagoServicioRequestVO.getCodigoPostal(),reqPagoServicioRequestVO.getTelefono());
						pagoServicioVO.getPagoServicio().setStatus(0);
						LOGGER.info("resPagoServicio.getPrincipalLocal(): "+resPagoServicio.getPrincipalLocal());
						
					}
					
					else if  (resPagoServicio.getClave().equals("23") || resPagoServicio.getClave().equals("24")) {
						LOGGER.info("El usuario o el passwd son incorrectos, favor de verificar el dato enviado.");
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
						pagoServicioVO.getPagoServicio().setStatus(-2);
					
					}
					else {				
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());						
						pagoServicioVO.getPagoServicio().setStatus(-1);
						pagoServicioVO.getPagoServicio().setPrincipalPesos("");

					}					
					
				}
				
				else if (reqPagoServicioRequestVO.getH2h().equals("true")) {
					
					//prueba controlada 
					/*Inicio de prueba*/
					//resPagoServicio.setClave("1");
					/*Fin prueba */
					if (resPagoServicio.getClave().equals("0")) {
						
						//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
						//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
						pagoServicioVO = new PagoServicioVO(Integer.parseInt(resPagoServicio.getClave()), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal(),reqPagoServicioRequestVO.getRfc(),reqPagoServicioRequestVO.getCalle(),reqPagoServicioRequestVO.getColonia(),reqPagoServicioRequestVO.getCiudad(),reqPagoServicioRequestVO.getEstado(),reqPagoServicioRequestVO.getCodigoPostal(),reqPagoServicioRequestVO.getTelefono());
						pagoServicioVO.getPagoServicio().setStatus(0);
					}					
					else if  (resPagoServicio.getClave().equals("1")) {
						
						LOGGER.info("ES IGUAL A 1");
						ReqEstatusServicioVO reqEstatusServicioVO = new ReqEstatusServicioVO();
						reqEstatusServicioVO.setFolioCliente(resPagoServicio.getFolioCliente());
						reqEstatusServicioVO.setFolioTelecomm(resPagoServicio.getFolioTelecomm());	
						++intento; 
						reqEstatusServicioVO.setIntento((String.valueOf(intento)));
						pagoServicioEstatusVO = this.getEstatusPS(reqEstatusServicioVO);
						
						LOGGER.info("/*******************************************************/");
						LOGGER.info("pagoServicioEstatusVO.getIdError() "+pagoServicioEstatusVO.getIdError());
						/*prueba controlada*/
						//pagoServicioEstatusVO.setClave("2");
						
						/*fin prueba controlada*/
						if (pagoServicioEstatusVO.getIdError()== 0) {
							
							LOGGER.info("ESTATUS 0");
							//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
							//pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
							/*Coloco la clave de pagoServicioEstatusVO ya que el metodo getEstatusPS la clave = 0 deberia responder 0 en vez de 1 que es la clave pendiente*/
							pagoServicioVO = new PagoServicioVO(pagoServicioEstatusVO.getIdError(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(),reqPagoServicioRequestVO.getNombre(),reqPagoServicioRequestVO.getFechaNacimiento(),reqPagoServicioRequestVO.getApPat(),reqPagoServicioRequestVO.getApMat(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal(),reqPagoServicioRequestVO.getRfc(),reqPagoServicioRequestVO.getCalle(),reqPagoServicioRequestVO.getColonia(),reqPagoServicioRequestVO.getCiudad(),reqPagoServicioRequestVO.getEstado(),reqPagoServicioRequestVO.getCodigoPostal(),reqPagoServicioRequestVO.getTelefono());
							/*Le coloco Status == 0 ya que cuando fui contra el metodo getEstatusPS dio clave == 0*/
							pagoServicioVO.getPagoServicio().setStatus(0);
							
						}
						// este else es mientras resulevo lo del ciclo que esta abajo
						else{
							pagoServicioVO.setIdError(-1);
							pagoServicioVO.setMensajeError("Error, operacion denegada");						
							pagoServicioVO.getPagoServicio().setStatus(-1);
							pagoServicioVO.getPagoServicio().setPrincipalPesos("");
						}
						
						
						long tiempoInicio = System.currentTimeMillis();
						operacionPendiente = ((pagoServicioEstatusVO.getIdError() ==2) == true);
						/*Comente mientras resuelvo lo de intentar por 80 segundos*/
						while (operacionPendiente) {							
							operacionPendiente = false;	
							pagoServicioEstatusVO = new PagoServicioVO();
//							//Clave == 2 Operacion Pendiente
							LOGGER.info("ENTRE AL WHILE");
							intento = Integer.parseInt(reqEstatusServicioVO.getIntento());
							++intento;
							reqEstatusServicioVO.setIntento((String.valueOf(intento)));
							pagoServicioEstatusVO = this.getEstatusPS(reqEstatusServicioVO);
//							/*prueba controlada*/
							//pagoServicioEstatusVO.setClave("2");	
//							/*fin prueba controlada*/
							if (pagoServicioEstatusVO.getIdError() == 2) {
//								
								long totalTiempo = (System.currentTimeMillis() - tiempoInicio)/1000;
								LOGGER.info("totalTiempo: "+totalTiempo);
//								// Solo se puede volver a ejecutar siempre y cuando la respuesta sea
//								// igual a 2 el tiempo de ejecucion del metodo getEstatus menor o igual
//								// a 80 segundos
								if (totalTiempo <= 80)	{
									operacionPendiente = true;
								}
								else {
									LOGGER.info("EL TIEMPO ES MAYOR A 80 SEGUNDOS");
									pagoServicioVO = new PagoServicioVO();
//									pagoServicioVO = new PagoServicioVO();																		
									operacionPendiente = false;
									pagoServicioEstatusVO.getPagoServicio().setNombre(reqPagoServicioRequestVO.getNombre());
									pagoServicioEstatusVO.getPagoServicio().setApellidoPaterno(reqPagoServicioRequestVO.getApPat());
									pagoServicioEstatusVO.getPagoServicio().setApellidoMaterno(reqPagoServicioRequestVO.getApMat());
									pagoServicioEstatusVO.getPagoServicio().setRfc(reqPagoServicioRequestVO.getRfc());
									pagoServicioEstatusVO.getPagoServicio().setCalle(reqPagoServicioRequestVO.getCalle());
									pagoServicioEstatusVO.getPagoServicio().setColonia(reqPagoServicioRequestVO.getColonia());
									pagoServicioEstatusVO.getPagoServicio().setCiudad(reqPagoServicioRequestVO.getCiudad());
									pagoServicioEstatusVO.getPagoServicio().setEstado(reqPagoServicioRequestVO.getEstado());
									pagoServicioEstatusVO.getPagoServicio().setCodigoPostal(reqPagoServicioRequestVO.getCodigoPostal());
									pagoServicioEstatusVO.getPagoServicio().setTelefono(reqPagoServicioRequestVO.getTelefono());
									pagoServicioEstatusVO.getPagoServicio().setFechaNacimiento(reqPagoServicioRequestVO.getFechaNacimiento());
									pagoServicioVO =  pagoServicioEstatusVO;
									pagoServicioVO.getPagoServicio().setOperador(reqPagoServicioRequestVO.getOperador());
									//como excedio los 80 seg se debe colocar como operacion exitosa
									pagoServicioVO.setIdError(0);
								}
							}
							else{
								
								LOGGER.info("pagoServicioEstatusVO.getIdError(): "+pagoServicioEstatusVO.getIdError());
								pagoServicioEstatusVO.getPagoServicio().setNombre(reqPagoServicioRequestVO.getNombre());
								pagoServicioEstatusVO.getPagoServicio().setApellidoPaterno(reqPagoServicioRequestVO.getApPat());
								pagoServicioEstatusVO.getPagoServicio().setApellidoMaterno(reqPagoServicioRequestVO.getApMat());
								pagoServicioEstatusVO.getPagoServicio().setRfc(reqPagoServicioRequestVO.getRfc());
								pagoServicioEstatusVO.getPagoServicio().setCalle(reqPagoServicioRequestVO.getCalle());
								pagoServicioEstatusVO.getPagoServicio().setColonia(reqPagoServicioRequestVO.getColonia());
								pagoServicioEstatusVO.getPagoServicio().setCiudad(reqPagoServicioRequestVO.getCiudad());
								pagoServicioEstatusVO.getPagoServicio().setEstado(reqPagoServicioRequestVO.getEstado());
								pagoServicioEstatusVO.getPagoServicio().setCodigoPostal(reqPagoServicioRequestVO.getCodigoPostal());
								pagoServicioEstatusVO.getPagoServicio().setTelefono(reqPagoServicioRequestVO.getTelefono());
								pagoServicioEstatusVO.getPagoServicio().setFechaNacimiento(reqPagoServicioRequestVO.getFechaNacimiento());
								pagoServicioVO =  pagoServicioEstatusVO;
								pagoServicioVO.getPagoServicio().setOperador(reqPagoServicioRequestVO.getOperador());
							}
							
						}
						
						LOGGER.info("operacionPendiente "+operacionPendiente);
						
//						if (operacionPendiente == false) {
//							LOGGER.info("ENTRE AL IF");
//							pagoServicioVO = new PagoServicioVO(resPagoServicio.getClave(), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());
//						}
						
					}
					
					else if  (resPagoServicio.getClave().equals("23") || resPagoServicio.getClave().equals("24")) {
						LOGGER.info("El usuario o el passwd son incorrectos, favor de verificar el dato enviado.");
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
						pagoServicioVO.getPagoServicio().setStatus(-2);
					
					}
					else {				
						pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
						pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
						pagoServicioVO.getPagoServicio().setStatus(-1);
						pagoServicioVO.getPagoServicio().setPrincipalPesos("");
					}
					
				}
				
				if ((pagoServicioVO.getPagoServicio().getStatus() == 0) || (pagoServicioVO.getPagoServicio().getStatus() == -1)) {
					
					LOGGER.info("getStatus() == 0 o  getStatus() == -1");
					bitacoraVO = insertaBitacoraTransaccion(pagoServicioVO,reqPagoServicioRequestVO);
					pagoServicioVO.getPagoServicio().setIdBitacora(bitacoraVO.getIdBitacora());
					mapper.insertBitacoraPagoServicio(pagoServicioVO);
				}
				
			}
			
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR getPeticionPS - ERROR: "+e);

		} 		
		finally {
			LOGGER.info("antes del return pagoServicioVO "+pagoServicioVO.getIdError()+"  "+pagoServicioVO.getMensajeError());
			return pagoServicioVO;
		}
		
		
	} 
	
	public ReversoServicioVO getReversoPS(ReqReversoServicioRequestVO reqReversoServicioRequestVO) {
		
		LOGGER.info("getReversoPS");
		GetReversoPS getReversoPS = new GetReversoPS();
		ReqReversoServicio reqReversoServicio = new ReqReversoServicio();		
		ObjectFactory factory = new ObjectFactory();	
		GetReversoPSResponse response = null;		
		ResReversoServicio resReversoServicio = new ResReversoServicio();
		ReversoServicioVO reversoServicioVO = new ReversoServicioVO(); 
		Bitacora bitacoraVO = new Bitacora();
		ValidacionVO validacionVO;
		try{
			
			validacionVO = validarReversoPS(reqReversoServicioRequestVO);
			if (validacionVO.isValida())	{
				
			
				String fecha = this.getCurrentTimeStamp();
				getReversoPS.setAutentificacion(this.getAutentifica());
				
				reqReversoServicio.setFolioTelecomm(reqReversoServicioRequestVO.getFolioTelecomm());
				reqReversoServicio.setIdProductoTerceros(reqReversoServicioRequestVO.getIdProductoTerceros());
				reqReversoServicio.setPrincipalPesos(reqReversoServicioRequestVO.getPrincipalPesos());
				reqReversoServicio.setReferencia1(reqReversoServicioRequestVO.getReferencia1());
				reqReversoServicio.setReferencia2(reqReversoServicioRequestVO.getReferencia2());
				reqReversoServicio.setReferencia3(reqReversoServicioRequestVO.getReferencia3());
				
				
				getReversoPS.setReqReversoServicio(reqReversoServicio);
				
				LOGGER.info("ANTES DE ENVIAR AL WS");
				
				response = (GetReversoPSResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetReversoPS(getReversoPS));
				
				LOGGER.info("DESPUES DE ENVIAR AL WS");
				LOGGER.info("response: "+response);
				
				resReversoServicio = response.getReturn();
				
				if (resReversoServicio != null) { 
					
					
					LOGGER.info("resPagoServicio.getClave():"+resReversoServicio.getClave());
					
					if (resReversoServicio.getClave().equals("0")){
						//le coloco el valor = 1 ya que es un reverso
						int esReverso = 1;
						reversoServicioVO = new ReversoServicioVO(Integer.parseInt(resReversoServicio.getClave()), resReversoServicio.getFolioCliente(), resReversoServicio.getFolioTelecomm(), resReversoServicio.getIdProductoTerceros(), resReversoServicio.getMensaje(), resReversoServicio.getPrincipalPesos(),fecha,esReverso);
					}
					
					else if  (resReversoServicio.getClave().equals("23") || resReversoServicio.getClave().equals("24")){
						LOGGER.info("El usuario o el passwd son incorrectos, favor de verificar el dato enviado.");
						reversoServicioVO.setIdError(Integer.parseInt(resReversoServicio.getClave()));
						reversoServicioVO.setMensajeError(resReversoServicio.getMensaje());
					
					}
					else {		
						int esReverso = 1;
						reversoServicioVO.setIdError(Integer.parseInt(resReversoServicio.getClave()));
						reversoServicioVO.setMensajeError(resReversoServicio.getMensaje());
						reversoServicioVO.getReversoServicio().setEsReverso(esReverso);
					}
					
					if (reversoServicioVO.getIdError() == 0)	{
						LOGGER.info("Guardo el reverso en la bitacora");
						bitacoraVO = insertaBitacoraTransaccionReverso(reversoServicioVO,reqReversoServicioRequestVO);
						reversoServicioVO.getReversoServicio().setIdBitacora(bitacoraVO.getIdBitacora());
						mapper.insertBitacoraReversoServicio(reversoServicioVO);
					}
					
				}
			
			}
			else	{
				reversoServicioVO = setReversoVO(validacionVO);
				
			}
			
			
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR getReversoPS - ERROR: "+desc);

		} 	
		
		return reversoServicioVO;		
		
	}
	
	
	public PagoServicioVO getEstatusPS(ReqEstatusServicioVO reqEstatusServicioVO) {
		
		LOGGER.info("getEstatusPS");
		GetEstatusPS getEstatusPS = new GetEstatusPS();
		ReqConsultaServicio reqConsultaServicio = new ReqConsultaServicio();
		ObjectFactory factory = new ObjectFactory();
		GetEstatusPSResponse response = null;
		ResPagoServicio resPagoServicio = new ResPagoServicio();
		PagoServicioVO pagoServicioVO = new PagoServicioVO();
		
		ReqEstatusServicio reqEstatusServicio = new ReqEstatusServicio();
		ValidacionVO validacionVO;
		
		try{
			 
			validacionVO = validarEstatusPS(reqEstatusServicioVO);
			if (validacionVO.isValida())	{
			
				getEstatusPS.setAutentificacion(this.getAutentifica());
				
				reqEstatusServicio.setFolioCliente(reqEstatusServicioVO.getFolioCliente());
				LOGGER.info("reqEstatusServicioVO.getFolioTelecomm(): "+reqEstatusServicioVO.getFolioTelecomm());
				
				if (reqEstatusServicioVO.getFolioTelecomm().equals("")) {
					reqEstatusServicio.setFolioTelecomm("0");
					LOGGER.info("IGUAL A 0");
				}
				else {
					reqEstatusServicio.setFolioTelecomm(reqEstatusServicioVO.getFolioTelecomm());
					LOGGER.info("DIFERENTE A 0 ");
				}
				LOGGER.info("reqEstatusServicioVO.getIntento()" +reqEstatusServicioVO.getIntento());
				reqEstatusServicio.setIntento(reqEstatusServicioVO.getIntento());
				
				getEstatusPS.setReqEstatusServicio(reqEstatusServicio);
				
				LOGGER.info("ANTES DE ENVIAR AL WS");
				
				response = (GetEstatusPSResponse) webServiceTemplate.marshalSendAndReceive(
						factory.createGetEstatusPS(getEstatusPS));
				
				LOGGER.info("DESPUES DE ENVIAR AL WS");
				LOGGER.info("response: "+response);
				resPagoServicio = response.getReturn();
				
				if (resPagoServicio.getClave().equals("0")) {
						pagoServicioVO = new PagoServicioVO(Integer.parseInt(resPagoServicio.getClave()), resPagoServicio.getDisponible1(), resPagoServicio.getDisponible2(), resPagoServicio.getDisponible3(), resPagoServicio.getFechaOperacion(), resPagoServicio.getFolioCliente(), resPagoServicio.getFolioProveedor(), resPagoServicio.getFolioTelecomm(), resPagoServicio.getIdProductoTerceros(), resPagoServicio.getIva(), resPagoServicio.getMensaje(), resPagoServicio.getOperador(), resPagoServicio.getPrecio(), resPagoServicio.getPremio(), resPagoServicio.getPrincipalLocal(), resPagoServicio.getPrincipalPesos(), resPagoServicio.getRedondeo(), resPagoServicio.getReferencia1(), resPagoServicio.getReferencia2(), resPagoServicio.getReferencia3(), resPagoServicio.getSubTotal(), resPagoServicio.getTerminal(), resPagoServicio.getTienda(), resPagoServicio.getTipoCambio(), resPagoServicio.getTipoMoneda(), resPagoServicio.getTotal());						
									
				}
				
				else if ((resPagoServicio.getClave().equals("23")) || (resPagoServicio.getClave().equals("24"))) {				
					pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
					pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
				}
				
				else{				
					pagoServicioVO.setIdError(Integer.parseInt(resPagoServicio.getClave()));
					pagoServicioVO.setMensajeError(resPagoServicio.getMensaje());
				}
			}
			else{
				pagoServicioVO = setPeticionVO(validacionVO);
				
			}
			
			
		} catch (Exception e) {
			String desc  = e.getMessage();
			LOGGER.error("ERROR getEstatusPS - ERROR: "+desc);

		} 	
		
		return pagoServicioVO;
		
		
	}
	
	public Autentifica getAutentifica() {
		return getCredential();
	}


	public void setAutentifica(Autentifica autentifica) {
		this.autentifica = autentifica;
	}
	
	
	private Autentifica getCredential(){
		
		Autentifica autentificacion = new Autentifica();
		autentificacion.setCodigo(Constantes.CODIGO);
		autentificacion.setSocio(Constantes.SOCIO);
		autentificacion.setUsuario(Constantes.USUARIO);
		autentificacion.setPasswd(Constantes.PASSWD);
//		autentificacion.setPasswd("123456");
		return autentificacion;
	}
	
	public ModelAndView procesaPago(String json,ModelMap modelo)	{
		
		Pago3DS pago3DS = new Pago3DS();
		return pago3DS.procesaPago(json, modelo);
		
	}
	
	public JSONObject toJSONObject(String LabelXML) {
		try {
			return XML.toJSONObject(LabelXML);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public JSONObject getJSONObject(JSONObject xmlJSONObj ,String LabelXML) {
		try {
			return xmlJSONObj.getJSONObject(LabelXML);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONArray getJSONArray(JSONObject JSONObj ,String LabelXML) {
		try {
			return JSONObj.getJSONArray(LabelXML);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}	
	
	
	public static String getCurrentTimeStamp() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        String formatDateTime = now.format(formatter);

        System.out.println("After : " + formatDateTime);
        
        return formatDateTime.toString();

	}
	

	
//	public boolean  validarCFE(String numero) {
//		
//		boolean valida = false;
//		
//		ValidacionVO validacionVO = new ValidacionVO();
//		
//		
////		valida = ((!utilsService.cadenaVacia(numero)) && (UtilsService.validarNumero(numero)) && (utilsService.longitudCadena(numero, 30)));
////		LOGGER.info("mensaje: "+mensaje);
//		return valida;
//		
//		
//	}
	
	
	
	public ValidacionVO  validarSoloRefencia(String numero,int longitudPermitida) {		
		
		ValidacionVO validacionVO = new ValidacionVO();		
		validacionVO = utilsService.referenciaVacia(numero);
		LOGGER.info("validacionVO.isValida(): "+validacionVO.isValida());
		
		if (validacionVO.isValida()) {
			
			validacionVO = utilsService.validarNumero(numero,Constantes.REFERENCIA_INVALIDA);
			LOGGER.info("validacionVO.isValida()2 : "+validacionVO.isValida());
			
			if (validacionVO.isValida()) {
				
				validacionVO = utilsService.validarLongitudReferencia1(numero, longitudPermitida);
				LOGGER.info("validacionVO.isValida()3: "+validacionVO.isValida());
			} 
		}		
		
		return validacionVO;		
	}
	
	public ValidacionVO  validarSoloRefencia2(String numero,int longitudPermitida) {		
		
		ValidacionVO validacionVO = new ValidacionVO();		
		validacionVO = utilsService.referencia2Vacia(numero);
		LOGGER.info("validacionVO.isValida(): "+validacionVO.isValida());
		
		if (validacionVO.isValida()) {
			
			validacionVO = utilsService.validarNumero(numero,Constantes.REFERENCIA2_INVALIDA);
			LOGGER.info("validacionVO.isValida()2 : "+validacionVO.isValida());
			
			if (validacionVO.isValida()) {
				
				validacionVO = utilsService.validarLongitudReferencia2(numero, longitudPermitida);
				LOGGER.info("validacionVO.isValida()3: "+validacionVO.isValida());
			} 
		}		
		
		return validacionVO;		
	}
	
	
	public ValidacionVO  validarRefenciasInterjet(String referencia1,String referencia3,int longitudPermitida) {
		
		
		LOGGER.info("validarRefenciasInterjet 3 parametros");
		ValidacionVO validacionVO = new ValidacionVO();
		validacionVO = validarSoloRefenciaAlfanumerica(referencia1,longitudPermitida);
		if ( validacionVO.isValida()) {
			validacionVO = utilsService.referencia3Vacia(referencia3);
		}
		return validacionVO;
	}
	
	public ValidacionVO  validarSoloRefenciaAlfanumerica(String numero,int longitudPermitida) {		
		
		LOGGER.info("validarSoloRefenciaAlfanumerica!!!!!!!!!!!!! ");
		ValidacionVO validacionVO = new ValidacionVO();		
		validacionVO = utilsService.referenciaVacia(numero);
		LOGGER.info("validacionVO.isValida(): "+validacionVO.isValida());
		
		if (validacionVO.isValida()) {
			
			validacionVO = utilsService.validarNumero(numero,Constantes.REFERENCIA_INVALIDA);
			LOGGER.info("validacionVO.isValida()2 : "+validacionVO.isValida());
			
			/*Si es solo numero en esta caso no es valido ya que es obligatorio que haya numero y letras*/
			if (validacionVO.isValida() == false) {
				
				validacionVO = utilsService.validarNumeroYLetra(numero,Constantes.REFERENCIA_ALFANUMERICA_INVALIDA_CE);
				LOGGER.info("validacionVO.isValida()3: "+validacionVO.isValida());
				
				if (validacionVO.isValida()) {
					validacionVO = utilsService.validarLongitudReferencia1(numero, longitudPermitida);
					LOGGER.info("validacionVO.isValida()4: "+validacionVO.isValida());
				}
				
			}
			else {
				validacionVO.setClave("-1");
				validacionVO.setMensaje(Constantes.REFERENCIA_ALFANUMERICA_INVALIDA);
				validacionVO.setValida(false);				
			}
		}		
		
		return validacionVO;
		
		
	}
	
	
	public ValidacionVO  validarSoloRefencia2Alfanumerica(String numero,int longitudPermitida) {		
		
		LOGGER.info("validarSoloRefenciaAlfanumerica ");
		ValidacionVO validacionVO = new ValidacionVO();		
		validacionVO = utilsService.referencia2Vacia(numero);
		LOGGER.info("validacionVO.isValida(): "+validacionVO.isValida());
		
		if (validacionVO.isValida()) {
			
			validacionVO = utilsService.validarNumero(numero,Constantes.REFERENCIA2_INVALIDA);
			LOGGER.info("validacionVO.isValida()2 : "+validacionVO.isValida());
			
			/*Si es solo numero en esta caso no es valido ya que es obligatorio que haya numero y letras*/
			if (validacionVO.isValida() == false) {
				
				validacionVO = utilsService.validarNumeroYLetra(numero,Constantes.REFERENCIA2_ALFANUMERICA_INVALIDA_CE);
				LOGGER.info("validacionVO.isValida()3: "+validacionVO.isValida());
				
				if (validacionVO.isValida()) {
					validacionVO = utilsService.validarLongitudReferencia2(numero, longitudPermitida);
					LOGGER.info("validacionVO.isValida()4: "+validacionVO.isValida());
				}
				
			}
			else {
				validacionVO.setClave("-1");
				validacionVO.setMensaje(Constantes.REFERENCIA2_ALFANUMERICA_INVALIDA);
				validacionVO.setValida(false);				
			}
		}		
		
		return validacionVO;
		
		
	}
	
	
	public ValidacionVO  validarDosRefencia(String numero,int longitudPermitida1,int longitudPermitida2) {		
		
		ValidacionVO validacionVO = new ValidacionVO();		
		validacionVO = utilsService.referenciaVacia(numero);
		LOGGER.info("validacionVO.isValida(): "+validacionVO.isValida());
		
		if (validacionVO.isValida()) {
			
			validacionVO = utilsService.validarNumero(numero,"La Referencia1 no es un numero valido");
			LOGGER.info("validacionVO.isValida()2 : "+validacionVO.isValida());
			
			if (validacionVO.isValida()) {
				
				validacionVO = utilsService.validaDosLongitudReferencia1(numero, longitudPermitida1,longitudPermitida2);
				LOGGER.info("validacionVO.isValida()3: "+validacionVO.isValida());
			} 
		}		
		
		return validacionVO;
		
		
	}
	
	
	public ValidacionVO  validarfechaReferencia(String fecha,int longitudPermitida) {		
		
		ValidacionVO validacionVO = new ValidacionVO();		
		validacionVO = utilsService.referencia2Vacia(fecha);
		LOGGER.info("validacionVO.isValida(): "+validacionVO.isValida());
		
		if (validacionVO.isValida()) {
			
			validacionVO = utilsService.validarFecha(fecha,Constantes.REFERENCIA_FECHA_INVALIDA);
			LOGGER.info("validacionVO.isValida()2 : "+validacionVO.isValida());
			
			if (validacionVO.isValida()) {
				
				validacionVO = utilsService.validarLongitudReferencia2(fecha, longitudPermitida);
				LOGGER.info("validacionVO.isValida()3: "+validacionVO.isValida());
			} 
		}		
		
		return validacionVO;
		
		
	}
	
//	public ValidacionVO  validarTELMEX(String numero) {		
//		
//		ValidacionVO validacionVO = new ValidacionVO();		
//		validacionVO = utilsService.cadenaVacia2(numero);
//		LOGGER.info("validacionVO.isValida(): "+validacionVO.isValida());
//		
//		if (validacionVO.isValida()) {
//			
//			validacionVO = utilsService.validarNumero(numero,"La Referencia1 no es un numero valido");
//			LOGGER.info("validacionVO.isValida()2 : "+validacionVO.isValida());
//			
//			if (validacionVO.isValida()) {
//				
//				validacionVO = utilsService.validaDosLongitudReferencia1(numero, 10,20);
//				LOGGER.info("validacionVO.isValida()3: "+validacionVO.isValida());
//			} 
//		}		
//		
//		return validacionVO;
//		
//		
//	}
	
	
	public PagoServicioVO validarCFEPeticion(String numero) {
		
		LOGGER.info("validarCFEPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarCFE(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarTELMEXPeticion(String numero) {
		
		LOGGER.info("validarTELMEXPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarTELMEX(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarTELCELPeticion(String numero) {
		
		LOGGER.info("validarTELCELPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarTELCEL(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarMEGACABLEPeticion(String numero) {
		
		LOGGER.info("validarMEGACABLEPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarMEGACABLE(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarSKYPeticion(String numero) {
		
		LOGGER.info("validarSKYPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarSKY(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarDISHPeticion(String numero) {
		
		LOGGER.info("validarDISHPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarDISH(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarMETROGASPeticion(String numero) {
		
		LOGGER.info("validarMETROGASPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarMETROGAS(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarGASNATURALPeticion(String numero) {
		
		LOGGER.info("validarGASNATURALPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarGASNATURAL(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarVOLARISPeticion(String numero) {
		
		LOGGER.info("validarVOLARISPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarVOLARIS(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarSUAUTOPeticion(String numero) {
		
		LOGGER.info("validarSUAUTOPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarSUAUTO(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarFONACOTPeticion(String numero) {
		
		LOGGER.info("validarFONACOTPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarFONACOT(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarINTERJETPeticion(String referencia1,String referencia3) {
		
		LOGGER.info("validarINTERJETPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarINTERJET(referencia1,referencia3);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarAEROMEXICOPeticion(String numero) {
		
		LOGGER.info("validarAEROMEXICOPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarAEROMEXICO(numero);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarTIEMPOAIREPeticion(String referencia1,String referencia2) {
		
		LOGGER.info("validarTiempoAirePeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarTiempoAire(referencia1, referencia2);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public PagoServicioVO validarEDOMEXICOPeticion(String referencia1,String referencia2) {
		
		LOGGER.info("validarEDOMEXICOPeticion: ");
		ValidacionVO validacionVO;		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();		
		validacionVO = validarEDOMEXICO(referencia1, referencia2);	
		pagoServicioVO = setPeticionVO(validacionVO);		
		return pagoServicioVO;
	}
	
	public ConsultapsVO validarTELMEXConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarTELMEXConsulta: ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarTELMEX(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarTELCELConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarTELCELConsulta: ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarTELCEL(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}	
	
	public ConsultapsVO validarMEGACABLEConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarMEGACABLEConsulta: ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarMEGACABLE(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}	


	
	public ConsultapsVO validarCFEConsulta(ReqConsultaServicioRequestVO request) {
		
		LOGGER.info("validarCFEConsulta: ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarCFE(request);	
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarSKYConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarSKYConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarSKY(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarDISHConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarDISHConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarDISH(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarMETROGASConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarMETROGASConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarMETROGAS(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarGASNATURALConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarGASNATURALConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarGASNATURAL(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarVOLARISConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarVOLARISConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarVOLARIS(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}	
	
	public ConsultapsVO validarSUAUTOConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarSUAUTOConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarSUAUTO(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarFONACOTConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarFONACOTConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarFONACOT(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarINTERJETConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarINTERJETConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarINTERJET(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarAEROMEXICOConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarAEROMEXICOConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarAEROMEXICO(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	
	public ConsultapsVO validarTIEMPOAIREConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarTIEMPOAIREConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();		
		validacionVO = validarTiempoAire(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	public ConsultapsVO validarEDOMEXICOConsulta(ReqConsultaServicioRequestVO request) {		
		
		LOGGER.info("validarEDOMEXICOConsulta ");
		ValidacionVO validacionVO;		
		ConsultapsVO consultapsVO = new ConsultapsVO();			
		validacionVO = validarEDOMEXICO(request);		
		consultapsVO = setConsultaVO(validacionVO);		
		return consultapsVO;
	}
	
	
	/*public ValidacionVO validarCFE(String numero) {		
		
		if (validarIdProductoTerceros())	{
			
		}
	}*/
	
	
	public boolean validarPrincipalPesos(String monto)	{
		
		return (utilsService.cadenaVacia(monto) && utilsService.validarMonto(monto));			
		
	}
	
	public boolean validarIdProductoTerceros(String idProductoTerceros)	{
		
		return (!utilsService.cadenaVacia(idProductoTerceros) && utilsService.validarNumero(idProductoTerceros));			
		
	}
	
	public ValidacionVO validacionIdProductoTerceros(String idProductoTerceros)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(idProductoTerceros)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.ID_PRODUCTO_VACIO, false);			
		}
		else  {
			if (!utilsService.validarNumero(idProductoTerceros))	{
				
				validacionVO = utilsService.setValidacionVO("-1", Constantes.ID_PRODUCTO_INVALIDO, false);
			}
			
			else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
			}
		}

		
		return validacionVO;
		
	}
	
	public ValidacionVO validacionPrincipalPesos(String principalPesos)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(principalPesos)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.PRICIPAL_PESOS_VACIO, false);			
		}
		else  {
			if (!utilsService.validarMonto(principalPesos))	{
				
				validacionVO = utilsService.setValidacionVO("-1", Constantes.PRICIPAL_PESOS_IVALIDO, false);
			}
			
			else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
			}
		}

		
		return validacionVO;
		
	}
	
	public ValidacionVO validacionFolioCliente(String folioCliente)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(folioCliente)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.FOLIO_CLIENTE_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	
	
	public ValidacionVO validacionFolioTelecomm(String folioTelecomm)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(folioTelecomm)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.FOLIO_TELECOM_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	
	public ValidacionVO validacionIntento(String intento)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(intento)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.INTENTO_VACIO, false);			
		}
		else  {
			if (!utilsService.validarNumero(intento))	{
				
				validacionVO = utilsService.setValidacionVO("-1", Constantes.INTENTO_INVALIDO, false);
			}
			
			else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
			}
		}

		
		return validacionVO;
		
	}

	

	public ValidacionVO validacionTienda(String tienda)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(tienda)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.TIENDA_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	
	public ValidacionVO validacionTerminal(String terminal)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(terminal)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.TERMINAL_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	

	public ValidacionVO validacionOperador(String operador)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(operador)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.OPERADOR_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	
	public ValidacionVO validacionNombre(String nombre)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(nombre)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.NOMBRE_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	
	public ValidacionVO validacionApPat(String apPat)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(apPat)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.APPAT_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	
	public ValidacionVO validacionApMat(String apMat)	{
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (utilsService.cadenaVacia(apMat)) 	{
			
			validacionVO = utilsService.setValidacionVO("-1", Constantes.APMAT_VACIO, false);			
		}				
		else	{
				validacionVO = utilsService.setValidacionVO("", "", true);
		}		
		return validacionVO;
		
	}
	
	public ValidacionVO validarCFE(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			
			validacionVO =  validarSoloRefencia(request.getReferencia1(),Constantes.LONGITUD_REFERENCIA_CFE);
		}		
		
		return validacionVO;
	}
	
	public PagoServicioVO validarCFEPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validarCFE(request.getReferencia1());
						}
					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	
	
	public PagoServicioVO validarTELMEXPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validarTELMEX(request.getReferencia1());
						}
					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	public PagoServicioVO validarTELCELPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validarTELCEL(request.getReferencia1());
						}
					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	
	public PagoServicioVO validarMEGACABLEPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
						
							validacionVO = validacionNombre(request.getNombre());
							
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{
									
									validacionVO = validacionApMat(request.getApMat());
									
									if (validacionVO.isValida())	{									
										
										validacionVO = validarMEGACABLE(request.getReferencia1());
										
									}
								}
							}
						}

					}//if
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	
	public PagoServicioVO validarTIEMPOAIREPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validarTiempoAire(request.getReferencia1(),request.getReferencia2());
						}
					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	
	public PagoServicioVO validarEDOMEXICOPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validarEDOMEXICO(request.getReferencia1(), request.getReferencia2());
						}
					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}

	
	public PagoServicioVO validarINTERJETPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		
		if (validacionVO.isValida())	{
		
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
		
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
					
						validacionVO = validacionOperador(request.getOperador());						
						
						if (validacionVO.isValida())	{
					
							validacionVO = validacionNombre(request.getNombre());
						
							if (validacionVO.isValida())	{
					
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{
					
									validacionVO = validacionApMat(request.getApMat());
									
									if (validacionVO.isValida())	{
					
					
										validacionVO = validarINTERJET(request.getReferencia1(),request.getReferencia3());
										
									}
								}
							}
						}

					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	public PagoServicioVO validarSKYPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
						
							validacionVO = validacionNombre(request.getNombre());
						
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{								
										
									validacionVO = validarSKY(request.getReferencia1());									
									
								}
							}
						}

					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}	
	
	
	public PagoServicioVO validarDISHPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
						
							validacionVO = validacionNombre(request.getNombre());
						
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{								
										
									validacionVO = validarDISH(request.getReferencia1());									
									
								}
							}
						}
					}					
				}
			}			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}

	
	public PagoServicioVO validarMETROGASPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validacionNombre(request.getNombre());
							
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{								
										
									validacionVO = validarMETROGAS(request.getReferencia1());									
									
								}
							}
						}
					}					
				}
			}			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	

	
	public PagoServicioVO validarGASNATURALPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validacionNombre(request.getNombre());
							
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{								
										
									validacionVO = validarGASNATURAL(request.getReferencia1());									
									
								}
							}	
						}
					}					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}	
	
	
	
	public PagoServicioVO validarAEROMEXICOPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validacionNombre(request.getNombre());
							
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{								
										
									validacionVO = validarAEROMEXICO(request.getReferencia1());									
									
								}
							}
						}
					}					
				}
			}			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}

	
	
	
	public PagoServicioVO validarVOLARISPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{							
						
							validacionVO = validacionNombre(request.getNombre());
						
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{								
										
									validacionVO = validarVOLARIS(request.getReferencia1());									
									
								}
							}
						}

					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	public PagoServicioVO validarSUAUTOPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{
							
							validacionVO = validarSUAUTO(request.getReferencia1());
							
						}
					}
					
				}
			}
			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	public PagoServicioVO validarFONACOTPeticion(ReqPagoServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		PagoServicioVO pagoServicioVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionTienda(request.getTienda());
				if (validacionVO.isValida())	{
					
					validacionVO = validacionTerminal(request.getTerminal());
					
					if (validacionVO.isValida())	{
						
						validacionVO = validacionOperador(request.getOperador());
						
						if (validacionVO.isValida())	{							
						
							validacionVO = validacionNombre(request.getNombre());
							
							if (validacionVO.isValida())	{
								
								validacionVO = validacionApPat(request.getApPat());
								
								if (validacionVO.isValida())	{								
										
									validacionVO = validarFONACOT(request.getReferencia1());									
									
								}
							}
						}
					}				
				}
			}			
		}		
		
		pagoServicioVO = setPeticionVO(validacionVO);
		
		return pagoServicioVO;
	}
	
	
	public ValidacionVO validarCFE(String numero) {		
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_CFE);
	}
	
	public ValidacionVO validarTELCEL(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_TELCEL);
	}
	
	public ValidacionVO validarTELCEL(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{	
		
			validacionVO = validarTELCEL(request.getReferencia1());
		}
		
		return validacionVO;
	}
	
	public ValidacionVO validarTELMEX(String numero) {
		
		return validarDosRefencia(numero, Constantes.LONGITUD1_REFERENCIA_TELMEX, Constantes.LONGITUD2_REFERENCIA_TELMEX);
	}
	
	public ValidacionVO validarTELMEX(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
		
			validacionVO =  validarDosRefencia(request.getReferencia1(), Constantes.LONGITUD1_REFERENCIA_TELMEX, Constantes.LONGITUD2_REFERENCIA_TELMEX);
		}
		return validacionVO;
	}
	
	public ValidacionVO validarMEGACABLE(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO = validarMEGACABLE(request.getReferencia1());
		}
		
		return validacionVO;
		
	}
	
	public ValidacionVO validarMEGACABLE(String numero) {
		
		return validarDosRefencia(numero, Constantes.LONGITUD1_REFERENCIA_MEGACABLE, Constantes.LONGITUD2_REFERENCIA_MEGACABLE);
	}
	
	public ValidacionVO validarSKY(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_SKY);
	}
	
	public ValidacionVO validarSKY(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO =  validarSKY(request.getReferencia1());
		}
		
		return validacionVO;
		
	}
	
	public ValidacionVO validarDISH(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_DISH);
	}
	
	
	public ValidacionVO validarDISH(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO =  validarDISH(request.getReferencia1());
		}
		
		return validacionVO;		
		
	}
	
	public ValidacionVO validarMETROGAS(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_METRO_GAS);
	}
	
	public ValidacionVO validarMETROGAS(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO =  validarMETROGAS(request.getReferencia1());
		}
		
		return validacionVO;
		
		
	}
	
	public ValidacionVO validarGASNATURAL(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_GAS_NATURAL);
	}
	
	public ValidacionVO validarGASNATURAL(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO =  validarGASNATURAL(request.getReferencia1());
		}
		
		return validacionVO;		
		
	}
	
	public ValidacionVO validarVOLARIS(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_VOLARIS);
	}
	
	public ValidacionVO validarVOLARIS(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO =  validarVOLARIS(request.getReferencia1());
		}
		
		return validacionVO;		
		
	}
	
	public ValidacionVO validarSUAUTO(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_SUAUTO);
	}
	
	public ValidacionVO validarSUAUTO(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO =  validarSUAUTO(request.getReferencia1());
		}
		
		return validacionVO;
		
		
	}
	
	public ValidacionVO validarFONACOT(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_FONACOT);
	}
	
	public ValidacionVO validarFONACOT(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO =  validarFONACOT(request.getReferencia1());
		}
		
		return validacionVO;		
	}
	
	public ValidacionVO validarINTERJET(String referencia1) {
		
		return validarSoloRefenciaAlfanumerica(referencia1,Constantes.LONGITUD_REFERENCIA_INTERJET);
		
	}
	
	public ValidacionVO validarINTERJET(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			
			validacionVO = validarINTERJET(request.getReferencia1());
		}
		return validacionVO;
		
	}
	
	
	public ValidacionVO validarINTERJET(String referencia1,String referencia3) {
		
		//return validarSoloRefenciaAlfanumerica(referencia1,Constantes.LONGITUD_REFERENCIA_INTERJET);
		LOGGER.info("validarINTERJET");
		return validarRefenciasInterjet(referencia1,referencia3,Constantes.LONGITUD_REFERENCIA_INTERJET);
		
	}
	
	
	
	public ValidacionVO validarAEROMEXICO(String numero) {
		
		return validarSoloRefenciaAlfanumerica(numero,Constantes.LONGITUD_REFERENCIA_AEROMEXICO);
	}	
	
	public ValidacionVO validarAEROMEXICO(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO;
		
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			
			validacionVO = validarAEROMEXICO(request.getReferencia1());
		}
		return validacionVO;
		
		
	}	
	
	
//	public ValidacionVO validarTIEMPOAIRE(String numero) {
//		
//		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_AEROMEXICO);
//	}
	
	public ValidacionVO validarTIEMPOAIREDifServ(String numero) {
	
		return validarSoloRefencia2(numero,Constantes.LONGITUD_REFERENCIA_TIEMPOAIRE_DIFSERV);
	
	}
	
	public ValidacionVO validarTIEMPOAIREBicentenario(String numero) {
		
		return validarSoloRefencia2(numero,Constantes.LONGITUD_REFERENCIA_TIEMPOAIRE_BICENTENARIO);
	
	}
	
	public ValidacionVO validarEDOMEXICOReferencia1(String numero) {
		
		return validarSoloRefencia(numero,Constantes.LONGITUD_REFERENCIA_EDOMEXICO1);
	
	}
	
	public ValidacionVO validarTIEMPOAIREIave(String numero) {
		
		ValidacionVO validacionVO;
		
		
		validacionVO = validarSoloRefencia2Alfanumerica(numero,Constantes.LONGITUD_REFERENCIA_TIEMPOAIRE_IAVE1);
		
		if (validacionVO.isValida() == false) {
			
			validacionVO = validarSoloRefencia2Alfanumerica(numero,Constantes.LONGITUD_REFERENCIA_TIEMPOAIRE_IAVE2);
			
			if (validacionVO.isValida() == false) {
				
				validacionVO.setMensaje(Constantes.REFERENCIA2_LONGITUD+Constantes.LONGITUD_REFERENCIA_TIEMPOAIRE_IAVE1 + " o de "+Constantes.LONGITUD_REFERENCIA_TIEMPOAIRE_IAVE2 +" digitos");
			}
		}
		
		return validacionVO;
		
	
	}
	
	public boolean encotrarReferenciaTiempoAire(String referencia1) {
		boolean encontrado = false;
		String enumName;
		int i = 0;
		for(TiempoAireEnum tiempoAireEnum: TiempoAireEnum.values()){
		
			enumName = tiempoAireEnum.name();
			
			LOGGER.info(tiempoAireEnum.name());
			LOGGER.info("referencia1 "+referencia1);
			encontrado = (tiempoAireEnum.name().equals(referencia1));
			LOGGER.info("encontrado "+encontrado);
			if (encontrado) 
				break;
//			encontrado = (referencia1 == enumName);
		}
		
		return encontrado;
	}
	
	
	public ValidacionVO validarTiempoAire(String referencia1,String referencia2) {
		
		ValidacionVO validacionVO = new ValidacionVO();
		validacionVO = utilsService.referenciaVacia(referencia1);
		
		if (validacionVO.isValida()) {
			if (encotrarReferenciaTiempoAire(referencia1)) {
			
				validacionVO =  validarReferencia2(referencia1,referencia2);
			}
		
			else {
				LOGGER.info("NO ENCONTRAMOS REFERENCIAS VALIDAS");
				validacionVO = new ValidacionVO("-1", "La Referencia1 no es valida", false);
			}
		}
		
		return validacionVO; 
				
				
	}
	
	
	public ValidacionVO validarTiempoAire(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO = new ValidacionVO();
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			validacionVO = validarTiempoAire(request.getReferencia1(),request.getReferencia2());
		}
 
		return validacionVO;
				
	}
	
	public ValidacionVO validarReferencia2(String referencia1,String referencia2) {
		
		if ((referencia1.equals("TELCEL")) || (referencia1.equals("IUSACELL"))  || (referencia1.equals("MOVISTAR")) || (referencia1.equals("UNEFON")) || (referencia1.equals("NEXTEL"))) {
			
			return validarTIEMPOAIREDifServ(referencia2);
		}
		
		else if (referencia1.equals("BICENTENARIO")) {
			
			return validarTIEMPOAIREBicentenario(referencia2);
		}
		
		else{
			return validarTIEMPOAIREIave(referencia2);
		}
		
	}
	public boolean validarReferencia1TiempoAire(String referencia1) {
		
		return encotrarReferenciaTiempoAire(referencia1);	

	} 
	
	
	public ValidacionVO validarEDOMEXICO(String referencia1,String referencia2) {
		
		ValidacionVO validacionVO = new ValidacionVO(); 
		
		validacionVO = validarEDOMEXICOReferencia1(referencia1); 
		
		if (validacionVO.isValida()) {
			
			validacionVO =  validarfechaReferencia(referencia2, Constantes.LONGITUD_REFERENCIA_EDOMEXICO2);
		}		
//		else {
//			LOGGER.info("NO ENCONTRAMOS REFERENCIAS VALIDAS");
//			validacionVO = new ValidacionVO("-1", "La Referencia1 no es valida", false);
//		}
		
		return validacionVO; 
				
				
	}
	
	public ValidacionVO validarEDOMEXICO(ReqConsultaServicioRequestVO request) {
		
		ValidacionVO validacionVO = new ValidacionVO();
		validacionVO = validacionPrincipalPesos(request.getPrincipalPesos());
		
		if (validacionVO.isValida())	{
			
			validacionVO = validarEDOMEXICO(request.getReferencia1(),request.getReferencia2());
		}
		
		return validacionVO;
	}
	
	public ConsultapsVO setConsultaVO(ValidacionVO validacionVO) {		

		ConsultapsVO consultapsVO = new ConsultapsVO();
		
		if (validacionVO.isValida() == false){
			LOGGER.info("setConsultaVO: ");
			
			consultapsVO.setIdError(Integer.parseInt(validacionVO.getClave()));
			consultapsVO.setMensajeError(validacionVO.getMensaje());		
		}	
		else{
			consultapsVO.setIdError(0);
			consultapsVO.setMensajeError("");
		}
		
		return consultapsVO;
		
	}
	
	
	public PagoServicioVO setPeticionVO(ValidacionVO validacionVO) {
		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();
		
		if (validacionVO.isValida() == false){
			LOGGER.info("SetPeticionVO: ");
			pagoServicioVO.setIdError(Integer.parseInt(validacionVO.getClave()));
			pagoServicioVO.setMensajeError(validacionVO.getMensaje());		
		}	
		else{
			pagoServicioVO.setIdError(0);
			pagoServicioVO.setMensajeError("");
		}
		
		return pagoServicioVO;
		
	}
	
	
	public ReversoServicioVO setReversoVO(ValidacionVO validacionVO) {
		
		ReversoServicioVO reversoServicioVO = new ReversoServicioVO();
		
		if (validacionVO.isValida() == false){
			LOGGER.info("SetPeticionVO: ");
			reversoServicioVO.setIdError(Integer.parseInt(validacionVO.getClave()));
			reversoServicioVO.setMensajeError(validacionVO.getMensaje());		
		}	
		else{
			reversoServicioVO.setIdError(0);
			reversoServicioVO.setMensajeError("");
		}
		
		return reversoServicioVO;
		
	}
	
	
		
	public ConsultapsVO ConsultaServicio(int idProductoTerceros,ReqConsultaServicioRequestVO request) {
		
		ConsultapsVO consultapsVO = new ConsultapsVO();
		
		switch (idProductoTerceros) {
		 
	        case 27://CFE
	        	consultapsVO = this.validarCFEConsulta(request);
	        	break;
	 
	        case 2://TELMEX
	        	consultapsVO = this.validarTELMEXConsulta(request);
	            break;
	        
	        case 227://TIEMPO AIRE
	        	consultapsVO = this.validarTIEMPOAIREConsulta(request);
	            break;
	        
	        case 93://TELCEL RADIO MÓVIL DIPSA
	        	consultapsVO = this.validarTELCELConsulta(request);
	            break;
	        
	        case 284://MEGACABLE (TELEFONÍA POR CABLE, INTERNET Y TV)
	        	consultapsVO = this.validarMEGACABLEConsulta(request);
	        	break;
	        
	        case 261://ESTADO DE MÉXICO (PREDIAL, EROGACIONES, ORGANISMOS AUXILIARES Y OTROS)
	        	consultapsVO = this.validarEDOMEXICOConsulta(request);
	        	break;
	        
	        case 210://INTERJET
	        	consultapsVO = this.validarINTERJETConsulta(request);
	        	break;
	        	
	        case 1://SKY
	        	consultapsVO = this.validarSKYConsulta(request);
	        	break;
	        
        	case 88://DISH
	        	consultapsVO = this.validarDISHConsulta(request);
	        	break;
	        
	        case 83://METRO GAS
	        	consultapsVO = this.validarMETROGASConsulta(request);
	        	break;
	        
	        case 77://GAS NATURAL
	        	consultapsVO = this.validarGASNATURALConsulta(request);
	        	break;
	        
	        case 97://AEROMÉXICO
	        	consultapsVO = this.validarAEROMEXICOConsulta(request);
	        	break;
	        
	        case 61://VOLARIS
	        	consultapsVO = this.validarVOLARISConsulta(request);
	        	break;
	        
	        case 202://SU AUTO
	        	consultapsVO = this.validarSUAUTOConsulta(request);
	        	break;
	        
	        case 84://FONACOT
	        	consultapsVO = this.validarFONACOTConsulta(request);
	        	break;
	        	
	        case 0:// el idProductoTerceros no es valido
	        	consultapsVO = this.idProductoNoValido();
	        	break;
 
		}
		return consultapsVO;
	}
	
	
	/*public ConsultapsVO ConsultaServicio(int idProductoTerceros,String referencia1,String referencia2) {
		
		ConsultapsVO consultapsVO = new ConsultapsVO();
		
		switch (idProductoTerceros) {
		 
	        case 27://CFE
	        	consultapsVO = this.validarCFEConsulta(referencia1);
	        	break;
	 
	        case 2://TELMEX
	        	consultapsVO = this.validarTELMEXConsulta(referencia1);
	            break;
	        
	        case 227://TIEMPO AIRE
	        	consultapsVO = this.validarTIEMPOAIREConsulta(referencia1, referencia2);
	            break;
	        
	        case 93://TELCEL RADIO MÓVIL DIPSA
	        	consultapsVO = this.validarTELCELConsulta(referencia1);
	            break;
	        
	        case 284://MEGACABLE (TELEFONÍA POR CABLE, INTERNET Y TV)
	        	consultapsVO = this.validarMEGACABLEConsulta(referencia1);
	        	break;
	        
	        case 261://ESTADO DE MÉXICO (PREDIAL, EROGACIONES, ORGANISMOS AUXILIARES Y OTROS)
	        	consultapsVO = this.validarEDOMEXICOConsulta(referencia1, referencia2);
	        	break;
	        
	        case 210://INTERJET
	        	consultapsVO = this.validarINTERJETConsulta(referencia1);
	        	break;
	        	
	        case 1://SKY
	        	consultapsVO = this.validarSKYConsulta(referencia1);
	        	break;
	        
	        case 88://DISH
	        	consultapsVO = this.validarDISHConsulta(referencia1);
	        	break;
	        
	        case 83://METRO GAS
	        	consultapsVO = this.validarMETROGASConsulta(referencia1);
	        	break;
	        
	        case 77://GAS NATURAL
	        	consultapsVO = this.validarGASNATURALConsulta(referencia1);
	        	break;
	        
	        case 97://AEROMÉXICO
	        	consultapsVO = this.validarAEROMEXICOConsulta(referencia1);
	        	break;
	        
	        case 61://VOLARIS
	        	consultapsVO = this.validarVOLARISConsulta(referencia1);
	        	break;
	        
	        case 202://SU AUTO
	        	consultapsVO = this.validarSUAUTOConsulta(referencia1);
	        	break;
	        
	        case 84://FONACOT
	        	consultapsVO = this.validarFONACOTConsulta(referencia1);
	        	break;
	        	
	        case 0:// el idProductoTerceros no es valido
	        	consultapsVO = this.idProductoNoValido();
	        	break;
 
		}
		return consultapsVO;
	}*/

	
public PagoServicioVO pagoServicio(int idProductoTerceros,String referencia1,String referencia2,String referencia3) {
		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();
		
		switch (idProductoTerceros) {
		 
	        case 27://CFE
	        	pagoServicioVO = this.validarCFEPeticion(referencia1);
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);	
	        	break;
	 
	        case 2://TELMEX
	        	pagoServicioVO = this.validarTELMEXPeticion(referencia1);
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);	
	            break;
	        
	        case 227://TIEMPO AIRE
	        	pagoServicioVO = this.validarTIEMPOAIREPeticion(referencia1, referencia2);
	        	//Monto a pagar por el cliente final al proveedor.
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	            break;
	        
	        case 93://TELCEL RADIO MÓVIL DIPSA
	        	pagoServicioVO = this.validarTELCELPeticion(referencia1);
	        	//Importe indicado por el cliente.
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	            break;
	        
	        case 284://MEGACABLE (TELEFONÍA POR CABLE, INTERNET Y TV)
	        	//Importe devuelto por el sistema?
	        	pagoServicioVO = this.validarMEGACABLEPeticion(referencia1);
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);
	        	break;
	        
	        case 261://ESTADO DE MÉXICO (PREDIAL, EROGACIONES, ORGANISMOS AUXILIARES Y OTROS)
	        	pagoServicioVO = this.validarEDOMEXICOPeticion(referencia1, referencia2);
	        	//Importe indicado en el recibo.
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 210://INTERJET
	        	//Hacer lo del token de la referencia3
	        	pagoServicioVO = this.validarINTERJETPeticion(referencia1,referencia3); 
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);
	        	break;
	        	
	        case 1://SKY
	        	pagoServicioVO = this.validarSKYPeticion(referencia1);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 88://DISH
	        	pagoServicioVO = this.validarDISHPeticion(referencia1);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 83://METRO GAS
	        	pagoServicioVO = this.validarMETROGASPeticion(referencia1);
	        	//Monto indicado en el recibo
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 77://GAS NATURAL
	        	pagoServicioVO = this.validarGASNATURALPeticion(referencia1);
	        	//Monto indicado en el recibo
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 97://AEROMÉXICO
	        	pagoServicioVO = this.validarAEROMEXICOPeticion(referencia1);
	        	break;
	        
	        case 61://VOLARIS
	        	pagoServicioVO = this.validarVOLARISPeticion(referencia1);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 202://SU AUTO
	        	pagoServicioVO = this.validarSUAUTOPeticion(referencia1);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 84://FONACOT
	        	pagoServicioVO = this.validarFONACOTPeticion(referencia1);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        	
	        case 0:// el idProductoTerceros no es valido
	        	pagoServicioVO = this.idProductoNoValidoPeticion();
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
 
		}
		return pagoServicioVO;
	}
	
	
	public PagoServicioVO pagoServicio(int idProductoTerceros,ReqPagoServicioRequestVO request) {
		
		PagoServicioVO pagoServicioVO = new PagoServicioVO();
		
		switch (idProductoTerceros) {
		 
	        case 27://CFE
	        	pagoServicioVO = this.validarCFEPeticion(request);
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);	
	        	break;
	 
	        case 2://TELMEX
	        	pagoServicioVO = this.validarTELMEXPeticion(request);
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);	
	            break;
	        
	        case 227://TIEMPO AIRE
	        	pagoServicioVO = this.validarTIEMPOAIREPeticion(request);
	        	//Monto a pagar por el cliente final al proveedor.
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	            break;
	        
	        case 93://TELCEL RADIO MÓVIL DIPSA
	        	pagoServicioVO = this.validarTELCELPeticion(request);
	        	//Importe indicado por el cliente.
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	            break;
	        
	        case 284://MEGACABLE (TELEFONÍA POR CABLE, INTERNET Y TV)
	        	//Importe devuelto por el sistema?
	        	pagoServicioVO = this.validarMEGACABLEPeticion(request);
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);
	        	break;
	        
	        case 261://ESTADO DE MÉXICO (PREDIAL, EROGACIONES, ORGANISMOS AUXILIARES Y OTROS)
	        	pagoServicioVO = this.validarEDOMEXICOPeticion(request);
	        	//Importe indicado en el recibo.
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 210://INTERJET
	        	//Hacer lo del token de la referencia3
	        	pagoServicioVO = this.validarINTERJETPeticion(request); 
	        	pagoServicioVO.getPagoServicio().setConsultaWS(true);
	        	break;
	        	
        	case 1://SKY
	        	pagoServicioVO = this.validarSKYPeticion(request);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 88://DISH
	        	pagoServicioVO = this.validarDISHPeticion(request);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 83://METRO GAS
	        	pagoServicioVO = this.validarMETROGASPeticion(request);
	        	//Monto indicado en el recibo
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 77://GAS NATURAL
	        	pagoServicioVO = this.validarGASNATURALPeticion(request);
	        	//Monto indicado en el recibo
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 97://AEROMÉXICO
	        	pagoServicioVO = this.validarAEROMEXICOPeticion(request);
	        	break;
	        
	        case 61://VOLARIS
	        	pagoServicioVO = this.validarVOLARISPeticion(request);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 202://SU AUTO
	        	pagoServicioVO = this.validarSUAUTOPeticion(request);
	        	//Monto indicado por el cliente        	
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
	        
	        case 84://FONACOT
	        	pagoServicioVO = this.validarFONACOTPeticion(request);
	        	//Monto indicado por el cliente
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;	
	        	
	        case 0:// el idProductoTerceros no es valido
	        	pagoServicioVO = this.idProductoNoValidoPeticion();
	        	pagoServicioVO.getPagoServicio().setConsultaWS(false);
	        	break;
 
		}
		return pagoServicioVO;
	}


	
	public ConsultapsVO idProductoNoValido() {
		
		ConsultapsVO consultapsVO = new ConsultapsVO(); 
		consultapsVO.setIdError(-1);
		consultapsVO.setMensajeError(Constantes.ID_PRODUCTO_INVALIDO);		
		return consultapsVO;
	}
	
	public PagoServicioVO idProductoNoValidoPeticion() {
		
		PagoServicioVO pagoServicioVO = new PagoServicioVO(); 
		pagoServicioVO.setIdError(-1);
		pagoServicioVO.setMensajeError(Constantes.ID_PRODUCTO_INVALIDO);
		return pagoServicioVO;
	}
	
	public ReqConsultaServicio setReqConsultaServicio(ReqConsultaServicioRequestVO request) {
		
		ReqConsultaServicio reqConsultaServicio = new ReqConsultaServicio();
		String fecha = this.getCurrentTimeStamp();
		LOGGER.info("fecha: "+fecha);
		reqConsultaServicio.setReferencia1(request.getReferencia1());
		reqConsultaServicio.setIdProductoTerceros(request.getIdProductoTerceros());
		reqConsultaServicio.setReferencia2(request.getReferencia2());
		reqConsultaServicio.setReferencia3(request.getReferencia3());
		reqConsultaServicio.setFechaOperacion(fecha);
		reqConsultaServicio.setPrincipalPesos(request.getPrincipalPesos());
		reqConsultaServicio.setDisponible1(request.getDisponible1());
		reqConsultaServicio.setDisponible2(request.getDisponible2());
		reqConsultaServicio.setDisponible3(request.getDisponible3());	
		
		return reqConsultaServicio; 
	}
	
	
	public ReqPagoServicio setReqPagoServicio(ReqPagoServicioRequestVO reqPagoServicioRequestVO) {
		
		ReqPagoServicio reqPagoServicio = new ReqPagoServicio();
		String fecha = this.getCurrentTimeStamp();
		LOGGER.info("fecha: "+fecha);
		reqPagoServicio.setApMat(reqPagoServicioRequestVO.getApMat());
		reqPagoServicio.setApPat(reqPagoServicioRequestVO.getApPat());
		reqPagoServicio.setCalle(reqPagoServicioRequestVO.getCalle());
		reqPagoServicio.setCiudad(reqPagoServicioRequestVO.getCiudad());
		reqPagoServicio.setCodigoPostal(reqPagoServicioRequestVO.getCodigoPostal());
		reqPagoServicio.setColonia(reqPagoServicioRequestVO.getColonia());
		reqPagoServicio.setDisponible1(reqPagoServicioRequestVO.getDisponible1());
		reqPagoServicio.setDisponible2(reqPagoServicioRequestVO.getDisponible2());
		reqPagoServicio.setDisponible3(reqPagoServicioRequestVO.getDisponible3());
		reqPagoServicio.setEstado(reqPagoServicioRequestVO.getEstado());
		reqPagoServicio.setFechaNacimiento(reqPagoServicioRequestVO.getFechaNacimiento());
		reqPagoServicio.setFechaOperacion(fecha);
		reqPagoServicio.setFolioCliente(reqPagoServicioRequestVO.getFolioCliente());
		reqPagoServicio.setIdProductoTerceros(reqPagoServicioRequestVO.getIdProductoTerceros());
		reqPagoServicio.setNombre(reqPagoServicioRequestVO.getNombre());
		reqPagoServicio.setOperador(reqPagoServicioRequestVO.getOperador());
		reqPagoServicio.setPrecio(reqPagoServicioRequestVO.getPrecio());
		reqPagoServicio.setPrecio2(reqPagoServicioRequestVO.getPrecio2());
		reqPagoServicio.setPrincipalLocal(reqPagoServicioRequestVO.getPrincipalLocal());
		LOGGER.info("reqPagoServicioRequestVO.getPrincipalPesos(): "+reqPagoServicioRequestVO.getPrincipalPesos());
		reqPagoServicio.setPrincipalPesos(reqPagoServicioRequestVO.getPrincipalPesos());
		reqPagoServicio.setReferencia1(reqPagoServicioRequestVO.getReferencia1());
		reqPagoServicio.setReferencia2(reqPagoServicioRequestVO.getReferencia2());
		reqPagoServicio.setReferencia3(reqPagoServicioRequestVO.getReferencia3());
		reqPagoServicio.setRfc(reqPagoServicioRequestVO.getRfc());
		reqPagoServicio.setTelefono(reqPagoServicioRequestVO.getTelefono());
		reqPagoServicio.setTerminal(reqPagoServicioRequestVO.getTerminal());
		reqPagoServicio.setTienda(reqPagoServicioRequestVO.getTienda());
		reqPagoServicio.setTipoCambio(reqPagoServicioRequestVO.getTipoCambio());
		reqPagoServicio.setTipoMoneda(reqPagoServicioRequestVO.getTipoMoneda());		
		
		return reqPagoServicio; 
	}
	
	public ReqConsultaServicioRequestVO setReqConsultaServicioRequestVO(ReqPagoServicioRequestVO reqPagoServicioRequestVO) {
		
		ReqConsultaServicioRequestVO reqConsultaServicioRequestVO = new ReqConsultaServicioRequestVO();
		
		reqConsultaServicioRequestVO.setReferencia1(reqPagoServicioRequestVO.getReferencia1());
		reqConsultaServicioRequestVO.setIdProductoTerceros(reqPagoServicioRequestVO.getIdProductoTerceros());
		reqConsultaServicioRequestVO.setReferencia2(reqPagoServicioRequestVO.getReferencia2());
		reqConsultaServicioRequestVO.setReferencia3(reqPagoServicioRequestVO.getReferencia3());
		reqConsultaServicioRequestVO.setFechaOperacion(reqPagoServicioRequestVO.getFechaOperacion());
		reqConsultaServicioRequestVO.setPrincipalPesos(reqPagoServicioRequestVO.getPrincipalPesos());
		reqConsultaServicioRequestVO.setDisponible1(reqPagoServicioRequestVO.getDisponible1());
		reqConsultaServicioRequestVO.setDisponible2(reqPagoServicioRequestVO.getDisponible2());
		reqConsultaServicioRequestVO.setDisponible3(reqPagoServicioRequestVO.getDisponible3());	
		
		return reqConsultaServicioRequestVO;
	}

	public ConsultapsVO getConsultapsVO(ResConsultaServicio resConsultaServicio) {
		
		ConsultapsVO consultapsVO = new ConsultapsVO();
		
		LOGGER.info("resConsultaServicio.getClave():"+resConsultaServicio.getClave());
		if (resConsultaServicio.getClave().equals("0")){
			LOGGER.info("OPERACION EXITOSA");
			consultapsVO = new ConsultapsVO(resConsultaServicio.getClave(), resConsultaServicio.getMensaje(), resConsultaServicio.getIdProductoTerceros(), resConsultaServicio.getIva(), resConsultaServicio.getPremio(), resConsultaServicio.getRedondeo(),resConsultaServicio.getPrincipalPesos(),resConsultaServicio.getReferencia1(), resConsultaServicio.getReferencia2(), resConsultaServicio.getReferencia3(), resConsultaServicio.getSubTotal(), resConsultaServicio.getTotal());
		}
		else if  (resConsultaServicio.getClave().equals("23") || resConsultaServicio.getClave().equals("24")){
			LOGGER.info("El usuario o el passwd son incorrectos, favor de verificar el dato enviado.");
			consultapsVO.setIdError(Integer.parseInt(resConsultaServicio.getClave()));			
			consultapsVO.setMensajeError(resConsultaServicio.getMensaje());
		
		}
		else {				
			consultapsVO.setIdError(Integer.parseInt(resConsultaServicio.getClave()));
			consultapsVO.setMensajeError(resConsultaServicio.getMensaje());
		}
		
		return consultapsVO;
		
	}
	
	public Bitacora insertaBitacoraTransaccion(PagoServicioVO pagoServicioVO,ReqPagoServicioRequestVO reqPagoServicioRequestVO) {
		Bitacora bitacora = new Bitacora();
		try {
			
			LOGGER.info("insertaBitacoraTransaccion");
			/** 
			 * id_usuario, id_proveedor, id_producto, bit_fecha, bit_hora, bit_concepto,
			bit_cargo, bit_ticket, bit_no_autorizacion, bit_codigo_error, bit_card_id, bit_status,
			imei, destino, tarjeta_compra, tipo, software, modelo, wkey, pase	
			 */
			
			
			
			LOGGER.info("insertaBitacoraTransaccion 2");
			bitacora.setIdUsuario(reqPagoServicioRequestVO.getIdUsuario());
			LOGGER.info("insertaBitacoraTransaccion 3 ");
			bitacora.setConcepto("Telecom pago de servicio ");
			LOGGER.info("insertaBitacoraTransaccion 4.1");
			
			LOGGER.info("pagoServicioVO.getPrincipalPesos(): "+pagoServicioVO.getPagoServicio().getPrincipalPesos());
			
			if (!pagoServicioVO.getPagoServicio().getPrincipalPesos().equals("")){
				bitacora.setCargo(Double.valueOf(pagoServicioVO.getPagoServicio().getPrincipalPesos()));
			}
			
			LOGGER.info("insertaBitacoraTransaccion 5");
			bitacora.setCodigoError(String.valueOf(pagoServicioVO.getIdError()));
			LOGGER.info("insertaBitacoraTransaccion 6");
			bitacora.setStatus(pagoServicioVO.getPagoServicio().getStatus());
			bitacora.setImei(reqPagoServicioRequestVO.getImei());
			LOGGER.info("insertaBitacoraTransaccion 7");
			bitacora.setSoftware(reqPagoServicioRequestVO.getSoftware());
			LOGGER.info("insertaBitacoraTransaccion 8");
			bitacora.setModelo(reqPagoServicioRequestVO.getModelo());
			LOGGER.info("insertaBitacoraTransaccion 9");
			bitacora.setWkey(reqPagoServicioRequestVO.getWkey());
			LOGGER.info("insertaBitacoraTransaccion 10");
			bitacora.setPase(0);
			LOGGER.info("insertaBitacoraTransaccion 11");
			mapper.insertaBitacoraTransaccionPS(bitacora);			
			
			
			
			LOGGER.info("TRANSACCION REGISTRADA: ID BITACORA - "+bitacora.getIdBitacora());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitacora;
	}
	
	
	private Bitacora insertaBitacoraTransaccionReverso(ReversoServicioVO reversoServicioVO,ReqReversoServicioRequestVO reqReversoServicioRequestVO) {
		Bitacora bitacora = new Bitacora();
		try {
			
			LOGGER.info("insertaBitacoraTransaccionReverso");
			/** 
			 * id_usuario, id_proveedor, id_producto, bit_fecha, bit_hora, bit_concepto,
			bit_cargo, bit_ticket, bit_no_autorizacion, bit_codigo_error, bit_card_id, bit_status,
			imei, destino, tarjeta_compra, tipo, software, modelo, wkey, pase	
			 */
			
			
			
			LOGGER.info("insertaBitacoraTransaccion 2");
			bitacora.setIdUsuario(reqReversoServicioRequestVO.getIdUsuario());
			LOGGER.info("insertaBitacoraTransaccion 3 ");
			bitacora.setConcepto("Telecom reverso de servicio ");
			LOGGER.info("insertaBitacoraTransaccion 4.1");
			
			LOGGER.info("pagoServicioVO.getReversoServicio().getPrincipalPesos(): "+reversoServicioVO.getReversoServicio().getPrincipalPesos());
			
			if ((reversoServicioVO.getReversoServicio().getPrincipalPesos()!= null) && (!reversoServicioVO.getReversoServicio().getPrincipalPesos().equals(""))){
				bitacora.setCargo(Double.valueOf(reversoServicioVO.getReversoServicio().getPrincipalPesos()));
			}
			
			LOGGER.info("insertaBitacoraTransaccion 5");
			bitacora.setCodigoError(String.valueOf(reversoServicioVO.getIdError()));
			LOGGER.info("insertaBitacoraTransaccion 6");
			bitacora.setStatus(reversoServicioVO.getReversoServicio().getStatus());
			bitacora.setImei(reqReversoServicioRequestVO.getImei());
			LOGGER.info("insertaBitacoraTransaccion 7");
			bitacora.setSoftware(reqReversoServicioRequestVO.getSoftware());
			LOGGER.info("insertaBitacoraTransaccion 8");
			bitacora.setModelo(reqReversoServicioRequestVO.getModelo());
			LOGGER.info("insertaBitacoraTransaccion 9");
			bitacora.setWkey(reqReversoServicioRequestVO.getWkey());
			LOGGER.info("insertaBitacoraTransaccion 10");
			bitacora.setPase(0);
			LOGGER.info("insertaBitacoraTransaccion 11");
			mapper.insertaBitacoraTransaccionPS(bitacora);			
			
			
			
			LOGGER.info("TRANSACCION REGISTRADA: ID BITACORA - "+bitacora.getIdBitacora());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitacora;
	}
	
	
	public ValidacionVO validarEstatusPS(ReqEstatusServicioVO request) {
		
		ValidacionVO validacionVO; 
		
		validacionVO = validacionFolioCliente(request.getFolioCliente());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionFolioTelecomm(request.getFolioTelecomm());
			if (validacionVO.isValida())	{
				
				validacionVO = validacionIntento(request.getIntento());
			}
			
		}		

		return validacionVO;
	}
	
	public ValidacionVO validarReversoPS(ReqReversoServicioRequestVO request) {
		
		ValidacionVO validacionVO; 
		
		validacionVO = validacionIdProductoTerceros(request.getIdProductoTerceros());
		if (validacionVO.isValida())	{
			
			validacionVO = validacionFolioTelecomm(request.getFolioTelecomm());			
			
		}		

		return validacionVO;
	}

	
}
