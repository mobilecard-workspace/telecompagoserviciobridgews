package com.addcel.telecom.pago.servicios.model.vo;

public class BaseVO {
	
	protected int idError ;
	protected String mensajeError;
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

}
