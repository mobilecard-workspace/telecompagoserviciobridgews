package com.addcel.telecom.pago.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MobilePaymentRequestVO extends ResponseConsultaLCVO{
	
	
private long idTransaccion;
	
	private String email;
	
	private String firmaBase64;
	
	//private String nombre;
	
	private int tipoTarjeta;
	
	private String tarjeta;
	
	private String vigencia;
	
	private String mes;
	
	private String anio;
	
	private String cvv2;
	
	private String imei;
	
	private String tipo;
	
	private String software;
	
	private String modelo;
	
	private AfiliacionVO afiliacion;
	
	private String referencia;
	
	private String idUsuario;
	
	private String concepto;
	
	private String idProveedor;
	
	private int emisor;
	
	private String operacion;
	
	private double comision;
	
	private String tarjetaT;
	
	private String clave;
	
	private String claveWS;
	
	private String proveedor;
	
	private String cx;
	
	private String cy;
	
	private String pin;
	
	private String login;
	
	private String idPais;
	
	private String tipoPago;
	
	private int idTarjeta;
	
	
	/*Cambios Para Telecomm*/
	
	
    private String apMat ="";
    private String apPat ="";
    private String calle ="";
    private String ciudad ="";
    private String codigoPostal ="";
    private String colonia ="";
    private String disponible1 ="";
    private String disponible2 ="";
    private String disponible3 ="";
    private String estado ="";
    private String fechaNacimiento ="";
    private String fechaOperacion ="";
    private String folioCliente ="";
    private String h2h ="";
    private String idProductoTerceros ="";
    private String nombre ="";
    private String operador ="";
    private String precio ="";
    private String precio2 ="";
    private String principalLocal ="";
    private String principalPesos ="";
    private String referencia1 ="";
    private String referencia2 ="";
    private String referencia3 ="";
    private String rfc ="";
    private String telefono ="";
    private String terminal ="";
    private String tienda ="";
    private String tipoCambio ="";
    private String tipoMoneda ="";
    private String total ="";
    private String nombreTarjeta;
	
	
	/*FIN Cambios Para Telecomm*/
	
	public String getApMat() {
		return apMat;
	}

	public void setApMat(String apMat) {
		this.apMat = apMat;
	}

	public String getApPat() {
		return apPat;
	}

	public void setApPat(String apPat) {
		this.apPat = apPat;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getDisponible1() {
		return disponible1;
	}

	public void setDisponible1(String disponible1) {
		this.disponible1 = disponible1;
	}

	public String getDisponible2() {
		return disponible2;
	}

	public void setDisponible2(String disponible2) {
		this.disponible2 = disponible2;
	}

	public String getDisponible3() {
		return disponible3;
	}

	public void setDisponible3(String disponible3) {
		this.disponible3 = disponible3;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getFolioCliente() {
		return folioCliente;
	}

	public void setFolioCliente(String folioCliente) {
		this.folioCliente = folioCliente;
	}

	public String getH2h() {
		return h2h;
	}

	public void setH2h(String h2h) {
		this.h2h = h2h;
	}

	
	
	public MobilePaymentRequestVO() {	
	}
	
	public MobilePaymentRequestVO(int idError, String mensajeError) {	
		super(idError, mensajeError);
	}
	
	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirmaBase64() {
		return firmaBase64;
	}
	public void setFirmaBase64(String firmaBase64) {
		this.firmaBase64 = firmaBase64;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public int getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(int tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public AfiliacionVO getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(AfiliacionVO afiliacion) {
		this.afiliacion = afiliacion;
	}
	
	public String toTrace(){
		return "[ID BITACORA: "+idTransaccion
				+", NOMBRE: "+nombre
				+", TIPO TARJETA: "+tipoTarjeta+"]";
//				+", AFILIACION: "+afiliacion.toString()+"]";
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

	public int getEmisor() {
		return emisor;
	}

	public void setEmisor(int emisor) {
		this.emisor = emisor;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getTarjetaT() {
		return tarjetaT;
	}

	public void setTarjetaT(String tarjetaT) {
		this.tarjetaT = tarjetaT;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getClaveWS() {
		return claveWS;
	}

	public void setClaveWS(String claveWS) {
		this.claveWS = claveWS;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getCx() {
		return cx;
	}

	public void setCx(String cx) {
		this.cx = cx;
	}

	public String getCy() {
		return cy;
	}

	public void setCy(String cy) {
		this.cy = cy;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getIdPais() {
		return idPais;
	}

	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public int getIdTarjeta() {
		return idTarjeta;
	}

	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}
	
	/*Cambios Para Telecomm*/

	public String getIdProductoTerceros() {
		return idProductoTerceros;
	}

	public void setIdProductoTerceros(String idProductoTerceros) {
		this.idProductoTerceros = idProductoTerceros;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getPrecio2() {
		return precio2;
	}

	public void setPrecio2(String precio2) {
		this.precio2 = precio2;
	}

	public String getPrincipalLocal() {
		return principalLocal;
	}

	public void setPrincipalLocal(String principalLocal) {
		this.principalLocal = principalLocal;
	}

	public String getPrincipalPesos() {
		return principalPesos;
	}

	public void setPrincipalPesos(String principalPesos) {
		this.principalPesos = principalPesos;
	}

	public String getReferencia1() {
		return referencia1;
	}

	public void setReferencia1(String referencia1) {
		this.referencia1 = referencia1;
	}

	public String getReferencia2() {
		return referencia2;
	}

	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	public String getReferencia3() {
		return referencia3;
	}

	public void setReferencia3(String referencia3) {
		this.referencia3 = referencia3;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getTienda() {
		return tienda;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

	public String getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public String getNombreTarjeta() {
		return nombreTarjeta;
	}

	public void setNombreTarjeta(String nombreTarjeta) {
		this.nombreTarjeta = nombreTarjeta;
	}
	
	/*FIN Cambios Para Telecomm*/
	
}
