package com.addcel.telecom.pago.servicios.model.vo;

public class ValidacionVO {
	
	String clave;
	String mensaje;
	boolean valida;
	
	
	public ValidacionVO() {
		
	}
	
	public ValidacionVO(String clave,String mensaje,boolean valida) {
		
		this.clave = clave;
		this.mensaje = mensaje;
		this.valida = valida;
		
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isValida() {
		return valida;
	}
	public void setValida(boolean valida) {
		this.valida = valida;
	}

}
