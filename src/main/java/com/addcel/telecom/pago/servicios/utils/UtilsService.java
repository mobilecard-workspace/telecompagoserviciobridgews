package com.addcel.telecom.pago.servicios.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.telecom.pago.servicios.model.vo.ValidacionVO;

import java.util.regex.*;

@Component
public class UtilsService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsService.class);
	
	@Autowired
	private ObjectMapper mapperJk; //= new ObjectMapper();	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json=mapperJk.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			LOGGER.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			LOGGER.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			LOGGER.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			LOGGER.info("jsonToObject ");			
			obj = mapperJk.readValue(json, clase);
			LOGGER.info("obj "+obj);
		} catch (JsonParseException e) {
			LOGGER.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			LOGGER.error("Error al parsear cadena json 3: {}", e);
		}
		LOGGER.info("obj2 "+obj);
		return obj;
	}
	
	private static final String patron_large = "yyyy/MM/dd HH:mm:ss";
	
	private static final SimpleDateFormat formato_large = new SimpleDateFormat(patron_large, new Locale("ES", "co"));
	
	public static String getFechaActual(){
		String resp = null;
		try{
			resp = formato_large.format(new Date());
		}catch(Exception e){
			LOGGER.error("Error en getFechaActual: {}",e );
		}
		return resp;
	}	
	
    public static boolean patternMatches(String cadena, String pattern){
    	boolean flag = false;
    	
    	Pattern pat = Pattern.compile(pattern); //".*@(hotmail|live|msn|outlook)\\..*"
	     Matcher mat = pat.matcher(cadena.toLowerCase());
	     if (mat.matches()) {
	    	 flag = true;
	     }
    	return flag;
    }
    
    
    public static boolean validarNumero(String numero){
    	
    	String pattern = "[0-9]*";    	
    	return UtilsService.patternMatches(numero,pattern);    	
    	 
    }
    
    
    public static boolean validarMonto(String monto){
    	
    	String pattern = "^[0-9]\\d*(\\.\\d+)?$";   	
    	LOGGER.info("pattern: "+UtilsService.patternMatches(monto,pattern));
    	return UtilsService.patternMatches(monto,pattern);    	
    	 
    }
    
    public ValidacionVO validarLongitudReferencia1(String numero,int longitudPermitida) {
    	
    	ValidacionVO validacionVO = new ValidacionVO();
    	
    	if (longitudCadena(numero,longitudPermitida) == false) {
    	
    		validacionVO.setClave("-1");
			validacionVO.setMensaje(Constantes.REFERENCIA_LONGITUD+longitudPermitida);
			validacionVO.setValida(false);
    	}
    	else{
			
			validacionVO = new ValidacionVO("", "", true);
		}
    	
    	return validacionVO;  
    }
    
    public ValidacionVO validarLongitudReferencia2(String numero,int longitudPermitida) {
    	
    	ValidacionVO validacionVO = validarLongitudReferencia1(numero,longitudPermitida);
    	
    	if (validacionVO.isValida() == false)
    		validacionVO.setMensaje(Constantes.REFERENCIA2_LONGITUD+longitudPermitida);
    	
    	return validacionVO;
    		
    }
    
    public ValidacionVO validaDosLongitudReferencia1(String numero,int longitudPermitida1, int longitudPermitida2) {
    	
    	ValidacionVO validacionVO = new ValidacionVO();
    	
    	if ((longitudCadena(numero,longitudPermitida1) == true) || (longitudCadena(numero,longitudPermitida2) == true)){
    	
    		validacionVO = new ValidacionVO("", "", true);			
    	}
    	else{
    		validacionVO.setClave("-1");
			validacionVO.setMensaje(Constantes.REFERENCIA_LONGITUD+longitudPermitida1 + " o de "+longitudPermitida2 +" digitos");
			validacionVO.setValida(false);
		}
    	
    	return validacionVO;  
    }
    
    
    
    public ValidacionVO validarNumero(String numero,String mensajeError){
    	
    	   
    	ValidacionVO validacionVO = new ValidacionVO();
    	
    	if (validarNumero(numero) == false) {    	
    		
    		validacionVO.setClave("-1");
			validacionVO.setMensaje(mensajeError);
			validacionVO.setValida(false);
		}
		else{
			
			validacionVO = new ValidacionVO("", "", true);
		}
    	
    	return validacionVO;    	
    	 
    }
    
    
    public ValidacionVO validarMonto(String monto,String mensajeError){
    	
 	   
    	ValidacionVO validacionVO = new ValidacionVO();
    	
    	if (validarMonto(monto) == false) {    	
    		
    		validacionVO.setClave("-1");
			validacionVO.setMensaje(mensajeError);
			validacionVO.setValida(false);
		}
		else{
			
			validacionVO = new ValidacionVO("", "", true);
		}
    	
    	return validacionVO;    	
    	 
    }
    
    
    
    public boolean validarNumeroYLetra(String numero){
    	
//    	String pattern = "[a-z1-9]";    	
    	String pattern = "^[a-zA-Z0-9]+$";
    	return UtilsService.patternMatches(numero,pattern);    	
    	 
    }
    
    public ValidacionVO validarNumeroYLetra(String numero,String mensajeError){
    	
 	   
    	ValidacionVO validacionVO = new ValidacionVO();
    	
    	if (validarNumeroYLetra(numero) == false) {    	
    		
    		validacionVO.setClave("-1");
			validacionVO.setMensaje(mensajeError);
			validacionVO.setValida(false);
		}
		else{
			
			validacionVO = new ValidacionVO("", "", true);
		}
    	
    	return validacionVO;    	
    	 
    }
    
	public boolean longitudCadena(String cadena,int longitudPermitida) {
		
		LOGGER.info("cadena.length(): "+cadena.length());
		LOGGER.info("longitudPermitida: "+longitudPermitida);
		return (cadena.length() == longitudPermitida);
	}
	
	public ValidacionVO longitudCadena2(String cadena,int longitudPermitida) {
		
		ValidacionVO validacionVO = new ValidacionVO();
		if (cadena.length() <= longitudPermitida) {
			
			validacionVO.setClave("-1");
			validacionVO.setMensaje("La Referencia1 debe tener una longitud de 30");
			validacionVO.setValida(false);
		}
		else{
			
			validacionVO = new ValidacionVO("", "", true);
		}
		return validacionVO;
	}
	
	public boolean cadenaVacia(String cadena) {		
		LOGGER.info("VALOR cadena "+cadena);
		LOGGER.info("cadenaVacia "+((cadena.equals("")) || (cadena == "")));
		return ((cadena.equals("")) || (cadena == ""));		
	}
	
	
	public ValidacionVO referenciaVacia(String referencia) {	
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (cadenaVacia(referencia)) {
			
			validacionVO.setClave("-1");
			validacionVO.setMensaje(Constantes.REFERENCIA_VACIA);
			validacionVO.setValida(false);
		} 
		else{
			
			validacionVO = new ValidacionVO("", "", true);
		}
		
		return validacionVO;
	}
	
	
	public ValidacionVO referencia2Vacia(String referencia) {	
		
		ValidacionVO validacionVO;
		
		validacionVO = referenciaVacia(referencia);
		
		if (validacionVO.isValida() == false)
			validacionVO.setMensaje(Constantes.REFERENCIA2_VACIA);
		return validacionVO;
	}
	
	public ValidacionVO referencia3Vacia(String referencia) {	
		
		ValidacionVO validacionVO = new ValidacionVO();
		
		if (cadenaVacia(referencia)) {
			
			validacionVO.setClave("-1");
			validacionVO.setMensaje(Constantes.REFERENCIA3_VACIA);
			validacionVO.setValida(false);
		} 
		else{
			
			validacionVO = new ValidacionVO("", "", true);
		}
		
		return validacionVO;
	}
    
	public boolean validarFecha(String fecha) {
        try {
            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
            formatoFecha.setLenient(false);
            formatoFecha.parse(fecha);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
	
	 public ValidacionVO validarFecha(String fecha,String mensajeError){
	    	
  	   
	    	ValidacionVO validacionVO = new ValidacionVO();
	    	
	    	if (validarFecha(fecha) == false) {    	
	    		
	    		validacionVO.setClave("-1");
				validacionVO.setMensaje(mensajeError);
				validacionVO.setValida(false);
			}
			else{
				
				validacionVO = new ValidacionVO("", "", true);
			}
	    	
	    	return validacionVO;    	
	    	 
	    }
	 
	 public int convertToInt(String numeroString) {
		 
		return  Integer.parseInt(numeroString);
	 }
	 
	 
	public ValidacionVO setValidacionVO(String clave,String mensaje,boolean valida)	{		
		
		return new ValidacionVO(clave, mensaje, valida);
		
	}
	
	public static String getCurrentTimeStamp() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        String formatDateTime = now.format(formatter);

        System.out.println("After : " + formatDateTime);
        
        return formatDateTime.toString();

	}

	
}
