package com.addcel.telecom.pago.servicios.model.vo;

public class ReqEstatusServicioVO {
	
	private String folioCliente ="";
	private String folioTelecomm ="";
	private String intento ="";
	
	
	public ReqEstatusServicioVO() {}
	
	public ReqEstatusServicioVO(String folioCliente,String folioTelecomm,String intento) {
		
		this.folioCliente = folioCliente;
		this.folioTelecomm = folioTelecomm;
		this.intento = intento;
	}
	
	public String getFolioCliente() {
		return folioCliente;
	}
	public void setFolioCliente(String folioCliente) {
		this.folioCliente = folioCliente;
	}
	public String getFolioTelecomm() {
		return folioTelecomm;
	}
	public void setFolioTelecomm(String folioTelecomm) {
		this.folioTelecomm = folioTelecomm;
	}
	public String getIntento() {
		return intento;
	}
	public void setIntento(String intento) {
		this.intento = intento;
	}
	
	
	

}
