package com.addcel.telecom.pago.servicios.model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConsultapsVO extends BaseVO {
	
	@SerializedName("ConsultaPS")
	@Expose
	private ConsultaPS consultaPS;
	
	public ConsultapsVO(){}
	
	public ConsultapsVO(String clave,String mensaje,String idProductoTerceros,String iva,String premio,String redondeo,String principalPesos, String referencia1,String referencia2,String referencia3,String subTotal,String total)	{
		
		this.idError = Integer.parseInt(clave);
		this.mensajeError = mensaje;
		this.consultaPS = new ConsultaPS(idProductoTerceros, iva, premio, redondeo,principalPesos, referencia1, referencia2, referencia3, subTotal, total);
		
	}
	

	public ConsultaPS getConsultaPS() {
		return consultaPS;
	}

	public void setConsultaPS(ConsultaPS consultaPS) {
		this.consultaPS = consultaPS;
	}
}
