package com.addcel.telecom.pago.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaPS {
	
//	private String clave;
//	private String mensaje;
	private String idProductoTerceros;
	private String iva;
	private String premio;
	private String redondeo;
	private String principalPesos;

	private String referencia1;
	private String referencia2;
	private String referencia3;
	private String subTotal;
	private String total;
	private String h2h;
	
	
	public ConsultaPS(){
		
	}	
	
	public ConsultaPS(String idProductoTerceros,String iva,String premio,String redondeo,String principalPesos,String referencia1,String referencia2,String referencia3,String subTotal,String total){
//		this.clave = clave;
//		this.mensaje = mensaje;
		this.idProductoTerceros = idProductoTerceros;
		this.iva = iva;
		this.premio = premio;
		this.redondeo = redondeo;
		this.principalPesos = principalPesos;
		this.referencia1 = referencia1;
		this.referencia2 = referencia2;
		this.referencia3 = referencia3;
		this.subTotal = subTotal;
		this.total = total;
		
	}
	
//	public String getClave() {
//		return clave;
//	}
//	public void setClave(String clave) {
//		this.clave = clave;
//	}
//	public String getMensaje() {
//		return mensaje;
//	}
//	public void setMensaje(String mensaje) {
//		this.mensaje = mensaje;
//	}

	public String getIdProductoTerceros() {
		return idProductoTerceros;
	}

	public void setIdProductoTerceros(String idProductoTerceros) {
		this.idProductoTerceros = idProductoTerceros;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getPremio() {
		return premio;
	}

	public void setPremio(String premio) {
		this.premio = premio;
	}

	public String getRedondeo() {
		return redondeo;
	}

	public void setRedondeo(String redondeo) {
		this.redondeo = redondeo;
	}

	public String getReferencia1() {
		return referencia1;
	}

	public void setReferencia1(String referencia1) {
		this.referencia1 = referencia1;
	}

	public String getReferencia2() {
		return referencia2;
	}

	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}

	public String getReferencia3() {
		return referencia3;
	}

	public void setReferencia3(String referencia3) {
		this.referencia3 = referencia3;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
	
	public String getPrincipalPesos() {
		return principalPesos;
	}

	public void setPrincipalPesos(String principalPesos) {
		this.principalPesos = principalPesos;
	}

	public String getH2h() {
		return h2h;
	}

	public void setH2h(String h2h) {
		this.h2h = h2h;
	}

	
}
