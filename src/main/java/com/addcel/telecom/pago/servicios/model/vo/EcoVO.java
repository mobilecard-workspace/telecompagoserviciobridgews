package com.addcel.telecom.pago.servicios.model.vo;

public class EcoVO {
	
	private String clave;	
	
	private String mensaje; 
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
