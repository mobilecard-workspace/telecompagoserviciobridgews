package com.addcel.telecom.pago.servicios.model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PagoServicioVO extends BaseVO {	
	
	@SerializedName("PagoServicio")
	@Expose
	private PagoServicio pagoServicio;
	
	public PagoServicioVO(){
		this.pagoServicio = new PagoServicio();
	}
	
	
	public PagoServicioVO(int clave,String disponible1,String disponible2,String disponible3,String fechaOperacion,String folioCliente,String folioProveedor,String folioTelecomm,String idProductoTerceros,String nombre,String fechaNacimiento,String apellidoPaterno,String apellidoMaterno,String iva,String mensaje ,String operador,String precio,String premio,String principalLocal,String principalPesos,String redondeo,String referencia1,String referencia2,String referencia3,String subTotal,String terminal,String tienda,String tipoCambio,String tipoMoneda,String total,String rfc, String calle,String colonia,String ciudad,String estado,String codigoPostal,String telefono)	{
		
		this.idError = clave;
		this.mensajeError = mensaje;
		this.pagoServicio = new PagoServicio(disponible1,disponible2,disponible3,fechaOperacion,folioCliente,folioProveedor,folioTelecomm,idProductoTerceros,nombre,fechaNacimiento,apellidoPaterno,apellidoMaterno,iva,operador,precio,premio,principalLocal,principalPesos,redondeo,referencia1,referencia2,referencia3,subTotal,terminal,tienda,tipoCambio,tipoMoneda,total,rfc, calle,colonia,ciudad,estado,codigoPostal,telefono);
		
	}
	
	
	public PagoServicioVO(int clave,String disponible1,String disponible2,String disponible3,String fechaOperacion,String folioCliente,String folioProveedor,String folioTelecomm,String idProductoTerceros,String iva,String mensaje,String operador,String precio,String premio,String principalLocal,String principalPesos,String redondeo,String referencia1,String referencia2,String referencia3,String subTotal,String terminal,String tienda,String tipoCambio,String tipoMoneda,String total) {
		
		this.idError = clave;
		this.mensajeError = mensaje;
		this.pagoServicio = new PagoServicio(disponible1, disponible2, disponible3, fechaOperacion, folioCliente, folioProveedor, folioTelecomm, idProductoTerceros, iva, operador, precio, premio, principalLocal, principalPesos, redondeo, referencia1, referencia2, referencia3, subTotal, terminal, tienda, tipoCambio, tipoMoneda, total);
	}


	public PagoServicio getPagoServicio() {
		return pagoServicio;
	}


	public void setPagoServicio(PagoServicio pagoServicio) {
		this.pagoServicio = pagoServicio;
	}
	
	
}
