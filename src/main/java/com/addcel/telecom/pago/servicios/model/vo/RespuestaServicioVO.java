package com.addcel.telecom.pago.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class RespuestaServicioVO {
		private int idError;
		private String msgError;
		private double tipoCambio;
		private Object datos;
		public int getIdError() {
			return idError;
		}
		public void setIdError(int idError) {
			this.idError = idError;
		}
		public String getMsgError() {
			return msgError;
		}
		public void setMsgError(String msgError) {
			this.msgError = msgError;
		}
		public Object getDatos() {
			return datos;
		}
		public void setDatos(Object datos) {
			this.datos = datos;
		}
		public double getTipoCambio() {
			return tipoCambio;
		}
		public void setTipoCambio(double tipoCambio) {
			this.tipoCambio = tipoCambio;
		}
		
	}
