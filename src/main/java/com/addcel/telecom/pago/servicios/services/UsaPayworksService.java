package com.addcel.telecom.pago.servicios.services;


import static com.addcel.telecom.pago.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;


import com.addcel.telecom.pago.servicios.model.vo.PagoTelecomInfo;

import com.addcel.telecom.pago.servicios.model.vo.AprovisionamientoResponse;
import com.addcel.telecom.pago.servicios.model.vo.DatosCorreoVO;
import com.addcel.telecom.pago.servicios.model.vo.ResponseConsultaLCVO;
import com.addcel.telecom.pago.servicios.model.vo.ResponsePagoMovilesVO;
import com.addcel.telecom.pago.servicios.controller.TelecomPagoServicioController;
import com.addcel.telecom.pago.servicios.model.mapper.ServiceMapper;
import com.addcel.telecom.pago.servicios.utils.Utils;
import com.addcel.telecom.pago.servicios.utils.UtilsService;
import com.addcel.telecom.pago.servicios.utils.Constantes;
import com.addcel.telecom.pago.servicios.model.vo.TBitacoraProsaVO;
import com.addcel.telecom.pago.servicios.model.vo.TBitacoraVO;
import com.addcel.telecom.pago.servicios.utils.AddCelGenericMail;

import com.addcel.utils.AddcelCrypto;

@Service
public class UsaPayworksService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UsaPayworksService.class);
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
	private ServiceMapper mapper;
	
	
	public ModelAndView payworks2Respuesta(String NUMERO_CONTROL,
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW, String RESULTADO_AUT, String BANCO_EMISOR,
			String ID_AFILIACION, String TIPO_TARJETA, String MARCA_TARJETA,
			ModelMap modelo, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("exito");
		ResponsePagoMovilesVO datosResp = null;
		ResponseConsultaLCVO respServ = new ResponseConsultaLCVO();
		DatosCorreoVO datosCorreoVO=null;
		String msg = null;
		AprovisionamientoResponse respuesta = null;
		try{
			LOGGER.info("PAGO USA SERVICIOS MEX - RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
					+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
					+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
					+" CODIGO_PAYW: " + CODIGO_PAYW+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
					+" MARCA_TARJETA: " + MARCA_TARJETA);			
			if(MARCA_TARJETA != null){
				MARCA_TARJETA = MARCA_TARJETA.replaceAll("-", "");
				TIPO_TARJETA = MARCA_TARJETA + "/" + TIPO_TARJETA;
			}
			//Comentar datos cuando en PROD, cuando paywork regrese informacion
			if (StringUtils.equals(Constantes.MODO, Constantes.AUT)) {
				BANCO_EMISOR = "BANORTE";
				TIPO_TARJETA = "VISA/CREDITO";
			}
			TEXTO = Utils.cambioAcento(TEXTO);
			
			PagoTelecomInfo pagoInfo = mapper.buscarDetallePago(NUMERO_CONTROL);
			LOGGER.info("Consulta detalle pago: "+pagoInfo.toTrace());
			datosResp = new ResponsePagoMovilesVO();
			datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
			datosResp.setReferencia(pagoInfo.getReferencia());
			datosResp.setMonto(pagoInfo.getMonto());
			pagoInfo.setTarjeta(AddcelCrypto.decryptTarjeta(pagoInfo.getTarjeta()));
			datosResp.setTarjeta(pagoInfo.getTarjeta().substring(0, 6) + " XXXXXX " + pagoInfo.getTarjeta().substring(pagoInfo.getTarjeta().length() - 4));
			datosResp.setBancoTarjeta(BANCO_EMISOR);
			
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				msg = "EXITO PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": pagoInfo.getTipoTarjeta() == 2? "MASTER": "VISA") 
						+ " "+BANCO_EMISOR+", "+TIPO_TARJETA;
				datosResp.setAutorizacion(CODIGO_AUT);
	//			mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setUsuario(String.valueOf(pagoInfo.getIdUsuario()));
				datosResp.setFecha(respServ.getFecha());
				datosResp.setHora(respServ.getHora());
				datosResp.setReferen(pagoInfo.getReferencia());
				datosResp.setMedioPresentacion("Internet");
				datosResp.setIdTransaccion(pagoInfo.getIdTransaccion());
				datosResp.setTipo(TIPO_TARJETA);
				mav = new ModelAndView("payworks/exito");
				datosCorreoVO = new DatosCorreoVO();
				datosCorreoVO.setEmail(pagoInfo.geteMail());
				double divisa = mapper.obtenDivisa("1");
				pagoInfo.setMonto(pagoInfo.getMonto() * divisa);
				pagoInfo.setComision(pagoInfo.getComision() * divisa);
				pagoInfo.setIdPais("2");
				LOGGER.info("CALCULANDO MONTOS DE DOLARES - MONTO: "+pagoInfo.getMonto()+" - COMSION: "+pagoInfo.getComision());
				/**************** * 
				 * Por aqui envio la notificacion que se hizo el cobro
				 * 
				 * 
				 * 
				 * *******/
				
				
				
				//respuesta = notificacionComercio(pagoInfo);
				/*if(respuesta.getIdError() == 0){
					double total = respuesta.getMonto() + respuesta.getComision();
					try {
						enviaCorreo(respuesta, total, CODIGO_AUT, String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), pagoInfo.getConcepto());
					} catch (Exception e) {
						LOGGER.error("No se pudo enviar el correo..");
					}
					datosResp.setIdError(0);
					updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, 
								msg+" "+pagoInfo.getConcepto());
					mav.addObject("mensajeError", "<BR>Estimado usuario, su pago ha sido exitoso." 
							+"<br><BR><BR> Pago: "+pagoInfo.getConcepto()
							+"<br>Fecha: "+FECHA_REQ_CTE
							+"<br>Referencia: "+REFERENCIA
							+"<br>Folio: "+RESULTADO_PAYW
							+"<br>Sucursal: 1"
							+"<br>Caja: 1"
							+"<br>Forma de pago: Tarjeta de credito"
							+"<br>Numero de Autorizacion: "+CODIGO_AUT
							+"<br>Folio MC: "+respuesta.getNumAuth()
							+"<br>Importe: "+respuesta.getMonto()
							+"<br>Comision: "+respuesta.getComision()
							+"<br>Total (incluida comision): "+total
							+"<BR><BR>Servicio operado por Plataforma MC. "
							+ "El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas. "
							+ "Para aclaraciones marque 01-800-925-5001. Favor de guardar este comprobante de pago para "
							+ "posibles aclaraciones.");
				} else {
					datosResp.setConcepto("Solicitud será aplicada en las próximas 24 hrs.");
					datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
					datosResp.setIdError(respuesta.getIdError());
					datosResp.setMensajeError(respuesta.getMensajeAntad());
					updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, datosResp.getMensajeError());
					mav = new ModelAndView("payworks/exito");
					mav.addObject("mensajeError", "El cobro fue realizado con éxito con la autorización "+CODIGO_AUT+
							" y tu solicitud será aplicada en las próximas 24 hrs.");
					try {
						double total = respuesta.getMonto() + respuesta.getComision();
						enviaCorreo(respuesta, total, CODIGO_AUT, String.valueOf(pagoInfo.getIdTransaccion()), pagoInfo.getNombre(), pagoInfo.getConcepto());
						LOGGER.info("ENVIO CORRECTAMENTE EL CORREO CUANDO ANTAD FALLO.");
					} catch (Exception e) {
						LOGGER.error("No se pudo enviar el correo..");
						e.printStackTrace();
					}
				}*/
			} else {
//				mapper.borrarTBitacoraPIN(NUMERO_CONTROL);
				msg = "PAGO " + (pagoInfo.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				datosResp.setFechaTran(new SimpleDateFormat(FORMATO_FECHA_ENCRIPT).format(new Date()));
				datosResp.setIdError(-9);
				datosResp.setMensajeError("El pago fue " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADO": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADO":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida") +BANCO_EMISOR+", "+TIPO_TARJETA+
					" . " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW:RESULTADO_AUT != null? RESULTADO_AUT: "") + ", Descripcion: " + TEXTO  );
				
				mav = new ModelAndView("payworks/pagina-error");
				mav.addObject("mensajeError", datosResp.getMensajeError());
				updateBitacoras(pagoInfo, datosResp, REFERENCIA, CODIGO_AUT, null, null, BANCO_EMISOR, TIPO_TARJETA, msg);
			}
		}catch(Exception e){
			mav = new ModelAndView("payworks/pagina-error");
			mav.addObject("mensajeError", 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
					+ "Por favor vuelva a intentarlo en unos minutos.");
			LOGGER.error("Error en el proceso de pago: {}", e);
			e.printStackTrace();
			datosResp = new ResponsePagoMovilesVO(100, 
					"Disculpe las molestias, detectamos un error al momento de autorizar su transaccion. "
							+ "Por favor vuelva a intentarlo en unos minutos.");
		}finally{
			modelo.put("datosResp", datosResp);
			mav.addObject("datosResp", datosResp);
		}
		return mav;	
	}
	
	
	private void updateBitacoras(PagoTelecomInfo pago, ResponsePagoMovilesVO datosResp,  String REFERENCIA, String CODIGO_AUT, 
			String REFERENCIA_CAN, String CODIGO_AUT_CAN, 
			String BANCO_EMISOR, String TIPO_TARJETA, String msg){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
        	LOGGER.debug("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraVO.setBitConcepto(msg);
        	tbitacoraVO.setBitTicket(msg);
        	tbitacoraVO.setBitNoAutorizacion(datosResp.getAutorizacion());
        	if(datosResp.getIdError() == 0){
        		tbitacoraVO.setBitStatus(1);
        	} else {
        		tbitacoraVO.setBitStatus(datosResp.getIdError());
        	}
        	tbitacoraVO.setBitCodigoError(datosResp.getIdError() != 0? datosResp.getIdError(): 0);
        	tbitacoraVO.setDestino("Referencia: " + pago.getReferencia() + "-" + TIPO_TARJETA);
        	mapper.updateBitacora(tbitacoraVO);
        	LOGGER.debug("Fin Update TBitacora.");
        	LOGGER.debug("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(pago.getIdTransaccion());
        	tbitacoraProsaVO.setConcepto(msg);
        	tbitacoraProsaVO.setAutorizacion(datosResp.getAutorizacion());
        	tbitacoraProsaVO.setReferenciaPayw(REFERENCIA);
        	mapper.updateBitacoraProsa(tbitacoraProsaVO);
        	LOGGER.debug("Fin Update TBitacoraProsa.");
        } catch(Exception e){
        	LOGGER.error("Error al actualizar las bitacoras: ", e);
        }
	}
	
	private void enviaCorreo(AprovisionamientoResponse response, double total,
			String eM_Auth, String eM_OrderID, String nombre, String concepto) {
		DatosCorreoVO datosCorreoVO = new DatosCorreoVO();
		String ticket = null;
		String bodyMail = null;
		String host = null;
	    int port = 0;
	    String username = null;
	    String password = null;
	    String from = null; 
	    Properties props = null;
		try {
			datosCorreoVO.setDescServicio(response.getConcepto());
			datosCorreoVO.setFecha(UtilsService.getFechaActual());
			datosCorreoVO.setReferenciaServicio(response.getReferencia());
			datosCorreoVO.setIdBitacora(Long.valueOf(eM_OrderID));
			datosCorreoVO.setNoAutorizacion(eM_Auth);
			if(response.getNumAuth() != null){
				datosCorreoVO.setFolioXcd(response.getNumAuth());
			} else {
				datosCorreoVO.setFolioXcd("Pendiente por Aprobar.");
			}
			datosCorreoVO.setImporte(response.getMonto());
			datosCorreoVO.setComision(response.getComision());
			datosCorreoVO.setMonto(total);
			datosCorreoVO.setNombre(nombre);
			datosCorreoVO.setConcepto(concepto);
//			ticket = response.getMensajeTicket().replace("|", "<br>");
//			ticket = ticket.replace("01(81)-8988-0200", "01–800–925–5001");
			ticket = "Plataforma MC. El pago se ha realizado con éxito y quedara aplicado a tu servicio en las próximas horas.";
			datosCorreoVO.setTicket(ticket);
			datosCorreoVO.setEmail(response.geteMail());
			bodyMail = mapper.getParametro("@MENSAJE_COMPRAANTAD");
			host =  mapper.getParametro("@SMTP"); 
            port = Integer.parseInt( mapper.getParametro("@SMTP_PORT") ); 
            username =  mapper.getParametro("@SMTP_USER"); 
            password =  mapper.getParametro("@SMTP_PWD"); 
            from = mapper.getParametro("@SMTP_MAIL_SEND");
            props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            LOGGER.debug("HOST: " + host);
            LOGGER.debug("PORT: " + port);
            LOGGER.debug("USERNAME: " + username);
//            AddCelGenericMail.generatedMailCommons(datosCorreoVO, bodyMail, props, from, host, port, username, password);
			AddCelGenericMail.generatedMail(datosCorreoVO, bodyMail);
		} catch (Exception e) {
			LOGGER.error("Error al mandar el correo: "+e.getCause());
		}	
	}


}
