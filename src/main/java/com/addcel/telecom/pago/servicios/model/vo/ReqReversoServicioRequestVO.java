package com.addcel.telecom.pago.servicios.model.vo;

public class ReqReversoServicioRequestVO {
	
	private String folioTelecomm ="";
    private String idProductoTerceros ="";
    private String principalPesos ="";
    private String referencia1 ="";
    private String referencia2 ="";
    private String referencia3 ="";
     
    
    /*datos para innsertar en la bitacora*/
    
   	private long idUsuario = 0L;     
   	private String imei = "";	
   	private String software = "";	
   	private String modelo = "";	
   	private String wkey = "";	
   	private int pase =0;
    
    
    public ReqReversoServicioRequestVO() {
    	
    }
    
    public ReqReversoServicioRequestVO(String folioTelecomm,String idProductoTerceros,String principalPesos,String referencia1,String referencia2,String referencia3) {
    	
    	this.folioTelecomm = folioTelecomm;
    	this.idProductoTerceros = idProductoTerceros;
    	this.principalPesos = principalPesos;
    	this.referencia1 = referencia1;
    	this.referencia2 = referencia2;
    	this.referencia3 = referencia3;
    }
    
	public String getFolioTelecomm() {
		return folioTelecomm;
	}
	public void setFolioTelecomm(String folioTelecomm) {
		this.folioTelecomm = folioTelecomm;
	}
	public String getIdProductoTerceros() {
		return idProductoTerceros;
	}
	public void setIdProductoTerceros(String idProductoTerceros) {
		this.idProductoTerceros = idProductoTerceros;
	}
	public String getPrincipalPesos() {
		return principalPesos;
	}
	public void setPrincipalPesos(String principalPesos) {
		this.principalPesos = principalPesos;
	}
	public String getReferencia1() {
		return referencia1;
	}
	public void setReferencia1(String referencia1) {
		this.referencia1 = referencia1;
	}
	public String getReferencia2() {
		return referencia2;
	}
	public void setReferencia2(String referencia2) {
		this.referencia2 = referencia2;
	}
	public String getReferencia3() {
		return referencia3;
	}
	public void setReferencia3(String referencia3) {
		this.referencia3 = referencia3;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getWkey() {
		return wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public int getPase() {
		return pase;
	}

	public void setPase(int pase) {
		this.pase = pase;
	}
    
    

}
