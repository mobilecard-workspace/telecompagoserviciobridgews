package com.addcel.telecom.pago.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaldoVO extends BaseVO {	

	private String saldo="0";
	
	
	public SaldoVO(){
		
	}	
	
	public SaldoVO(String clave,String mensaje,String saldo){
		this.idError = Integer.parseInt(clave);
		this.mensajeError = mensaje;
		this.saldo = saldo;
	}	

	public String getSaldo() {
		return saldo;
	}
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	
	
}
