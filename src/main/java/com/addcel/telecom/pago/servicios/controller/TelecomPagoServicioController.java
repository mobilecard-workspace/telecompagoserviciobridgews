package com.addcel.telecom.pago.servicios.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import com.addcel.telecom.pago.servicios.model.vo.CatalogoLeyendaVO;
import com.addcel.telecom.pago.servicios.model.vo.CatalogoVO;
import com.addcel.telecom.pago.servicios.model.vo.ConsultapsVO;
import com.addcel.telecom.pago.servicios.model.vo.EcoVO;
import com.addcel.telecom.pago.servicios.model.vo.PagoServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqConsultaServicioRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqEcoVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqEstatusServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqPagoServicioRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.ReqReversoServicioRequestVO;
import com.addcel.telecom.pago.servicios.model.vo.ReversoServicioVO;
import com.addcel.telecom.pago.servicios.model.vo.SaldoVO;
import com.addcel.telecom.pago.servicios.model.vo.MobilePaymentRequestVO;
import com.addcel.telecom.pago.servicios.services.Pago3DS;
import com.addcel.telecom.pago.servicios.services.UsaPayworksService;
import com.addcel.telecom.pago.servicios.services.payService;

@Controller
public class TelecomPagoServicioController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TelecomPagoServicioController.class);
	
	
	@Autowired
	private payService requestPayService;
	
	@Autowired	
	private Pago3DS pago3DS;
	
	@Autowired	
	private UsaPayworksService usaPayworksService;
	
	@RequestMapping(value = "/index", method=RequestMethod.GET)
	public ModelAndView getToken() {
		
		LOGGER.info("/index");
		ModelAndView mav = new ModelAndView("index");
		return mav;
	}	

	@RequestMapping(value = "/telecom/pagoServicio/getEco", method = RequestMethod.POST, produces = "application/json")	
	public ResponseEntity<EcoVO> getEco(@RequestBody ReqEcoVO request){
		EcoVO ecoVO = requestPayService.getEco(request);
		LOGGER.info("entre al maping de pago de servicio getEco");
		return new ResponseEntity<EcoVO>(ecoVO,HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/telecom/pagoServicio/getCatalogoPS", method = RequestMethod.POST, produces = "application/json")	
	public ResponseEntity<CatalogoVO> getCatalogoPS(){
		LOGGER.info("entre al maping de getCatalogoPS");
		CatalogoVO catalogoVO = requestPayService.getCatalogoPS();
		return new ResponseEntity<CatalogoVO>(catalogoVO,HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/telecom/pagoServicio/getLeyendaPS", method = RequestMethod.POST, produces = "application/json")	
	public ResponseEntity<CatalogoLeyendaVO> getLeyendaPS(){
		LOGGER.info("entre al maping de getLeyendaPS");
		CatalogoLeyendaVO catalogoVO = requestPayService.getLeyendaPS();
		return new ResponseEntity<CatalogoLeyendaVO>(catalogoVO,HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value = "/telecom/pagoServicio/getSaldo", method = RequestMethod.POST, produces = "application/json")	
	public ResponseEntity<SaldoVO> getSaldo(){
		SaldoVO saldoVO = requestPayService.getSaldo();
		LOGGER.info("entre al maping de getSaldo");
		return new ResponseEntity<SaldoVO>(saldoVO,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/telecom/pagoServicio/getConsultaPS", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<ConsultapsVO> getConsultaPS(@RequestBody ReqConsultaServicioRequestVO request) {
		LOGGER.info("entre al maping de getConsultaPS");
		ConsultapsVO consultapsVO = requestPayService.getConsultaPS(request);
		return new ResponseEntity<ConsultapsVO>(consultapsVO,HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value = "/telecom/pagoServicio/getPeticionPS", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<PagoServicioVO> getPeticionPS(@RequestBody ReqPagoServicioRequestVO request) {
		LOGGER.info("entre al maping de getPeticionPS");
		PagoServicioVO pagoServicioVO = requestPayService.getPeticionPS(request);
		return new ResponseEntity<PagoServicioVO>(pagoServicioVO,HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/telecom/pagoServicio/getReversoPS", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<ReversoServicioVO> getReversoPS(@RequestBody ReqReversoServicioRequestVO request) {
		LOGGER.info("entre al maping de getReversoPS");
		ReversoServicioVO reversoServicioVO = requestPayService.getReversoPS(request);
		return new ResponseEntity<ReversoServicioVO>(reversoServicioVO,HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/telecom/pagoServicio/getEstatusPS", method=RequestMethod.POST,produces = "application/json;charset=UTF-8")
	public ResponseEntity<PagoServicioVO> getEstatusPS(@RequestBody ReqEstatusServicioVO request) {
		LOGGER.info("entre al maping de getEstatusPS");
		PagoServicioVO pagoServicioVO = requestPayService.getEstatusPS(request);
		return new ResponseEntity<PagoServicioVO>(pagoServicioVO,HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value = "/telecom/pagoServicio/pagoProsa3DS")
	public ModelAndView procesaPago(@RequestParam("json") String data,  ModelMap modelo) {
		LOGGER.info("entre al maping de procesaPago");
		return pago3DS.procesaPago(data, modelo);
	}
	
	
	@RequestMapping(value = "/telecom/pagoServicio/enviaPago3DS", method = RequestMethod.POST)
	public ModelAndView enviaPago3DS(@ModelAttribute MobilePaymentRequestVO pago,  ModelMap modelo) {
		LOGGER.info("entre al maping de enviaPago3DS");
		return pago3DS.procesaPago(pago, modelo);
	}
	
	
	@RequestMapping(value = "/telecom/pagoServicio/payworksRec3DRespuesta")
	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena, ModelMap modelo) {
		return pago3DS.procesaRespuesta3DPayworks(cadena, modelo);	
	}
	
	@RequestMapping(value = "/telecom/pagoServicio/payworks2RecRespuesta" )
	public ModelAndView payworks2RecRespuesta(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		return pago3DS.payworks2Respuesta(NUMERO_CONTROL,REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, 
				BANCO_EMISOR, ID_AFILIACION, TIPO_TARJETA, MARCA_TARJETA, modelo, request);	
	}
	
	@RequestMapping(value = "/telecom/pagoServicio/payworks/banorte/usa/respuesta2",method = RequestMethod.GET)
	public String payworks2RecBanUsaRespuesta2()	{
		LOGGER.info("payworks2RecBanUsaRespuesta2");
		return "prueba";
	}
			
	
	@RequestMapping(value = "/telecom/pagoServicio/payworks/banorte/usa/respuesta",method = RequestMethod.POST)
	public ModelAndView payworks2RecBanUsaRespuesta(
			
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		LOGGER.info("payworks2RecBanUsaRespuesta!!!!");
		LOGGER.info("NUMERO_CONTROL: "+NUMERO_CONTROL);
		//return "cara e chucky"; 
		return usaPayworksService.payworks2Respuesta(NUMERO_CONTROL,REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW, RESULTADO_AUT, 
				BANCO_EMISOR, ID_AFILIACION, TIPO_TARJETA, MARCA_TARJETA, modelo, request);	
	}
	
}