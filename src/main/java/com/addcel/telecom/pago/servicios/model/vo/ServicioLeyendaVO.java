package com.addcel.telecom.pago.servicios.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicioLeyendaVO {
	private String leyenda1;
	private int idProducto;
	private String subproducto;
	private String leyenda2;
	private String content;
	

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getSubproducto() {
		return subproducto;
	}

	public void setSubproducto(String subproducto) {
		this.subproducto = subproducto;
	}

	public String getLeyenda2() {
		return leyenda2;
	}

	public void setLeyenda2(String leyenda2) {
		this.leyenda2 = leyenda2;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLeyenda1() {
		return leyenda1;
	}

	public void setLeyenda1(String leyenda1) {
		this.leyenda1 = leyenda1;
	}

}
