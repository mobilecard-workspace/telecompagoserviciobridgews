package com.addcel.telecom.pago.servicios.model.vo;

public class UsuarioVO {

	private long idUsuario;
	private String login;
    private String password;
	private String nombreUsuario;
	private String numCelular;
	private String proveedor;
	private String nombre;
	private String nombreTarjeta;
	private String apellidoP;
	private String apellidoM;
	private String fechaNac;
	private String sexo;
	private String telefonoCasa;
	private String telefonoOficina;
	private String email;
	private String estado;
	private String ciudad;
	private String calle;
	private String numExt;
	private String numInt;
	private String colonia;
	private String codigoPostal;
	private String numTarjeta;
	private String tipoTarjeta;
	private String codigoPostalAMEX;
	private String domicilioAMEX;
	private String vigenciaTarjeta;
	private String terminos;
	private String plataforma;
	private String monto;
	private String passMobileCard;
	private String cvv2;
	private int status;
	
	private String nacimiento;
    private String telefono;
    private String registro;
    private String apellido;
    private String direccion;
    private String tarjeta;
    private String vigencia;
    private int banco;
//    private int tipotarjeta;
    private String mail;
    private String passwordS;
    private String imei;
    private int idtiporecargatag;
    private String etiqueta;
    private String numero;
    private int dv;
    private String tipo;
    private String software;
    private String modelo;
    private String key;
    private String materno;
    private String tel_casa;
    private String tel_oficina;
    private int id_estado;
    private int num_ext;
    private String num_interior;
    private String cp;
    private String dom_amex;
    private int tipo_cliente;
    
    
    private String nombreEstado;
    
	
	

	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getNumCelular() {
		return numCelular;
	}
	public void setNumCelular(String numCelular) {
		this.numCelular = numCelular;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getTelefonoCasa() {
		return telefonoCasa;
	}
	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}
	public String getTelefonoOficina() {
		return telefonoOficina;
	}
	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumExt() {
		return numExt;
	}
	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}
	public String getNumInt() {
		return numInt;
	}
	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getNumTarjeta() {
		return numTarjeta;
	}
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getCodigoPostalAMEX() {
		return codigoPostalAMEX;
	}
	public void setCodigoPostalAMEX(String codigoPostalAMEX) {
		this.codigoPostalAMEX = codigoPostalAMEX;
	}
	public String getDomicilioAMEX() {
		return domicilioAMEX;
	}
	public void setDomicilioAMEX(String domicilioAMEX) {
		this.domicilioAMEX = domicilioAMEX;
	}
	public String getVigenciaTarjeta() {
		return vigenciaTarjeta;
	}
	public void setVigenciaTarjeta(String vigenciaTarjeta) {
		this.vigenciaTarjeta = vigenciaTarjeta;
	}
	public String getTerminos() {
		return terminos;
	}
	public void setTerminos(String terminos) {
		this.terminos = terminos;
	}
	public String getPlataforma() {
		return plataforma;
	}
	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getPassMobileCard() {
		return passMobileCard;
	}
	public void setPassMobileCard(String passMobileCard) {
		this.passMobileCard = passMobileCard;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public int getBanco() {
		return banco;
	}
	public void setBanco(int banco) {
		this.banco = banco;
	}
//	public int getTipotarjeta() {
//		return tipotarjeta;
//	}
//	public void setTipotarjeta(int tipotarjeta) {
//		this.tipotarjeta = tipotarjeta;
//	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPasswordS() {
		return passwordS;
	}
	public void setPasswordS(String passwordS) {
		this.passwordS = passwordS;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public int getIdtiporecargatag() {
		return idtiporecargatag;
	}
	public void setIdtiporecargatag(int idtiporecargatag) {
		this.idtiporecargatag = idtiporecargatag;
	}
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public int getDv() {
		return dv;
	}
	public void setDv(int dv) {
		this.dv = dv;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getTel_casa() {
		return tel_casa;
	}
	public void setTel_casa(String tel_casa) {
		this.tel_casa = tel_casa;
	}
	public String getTel_oficina() {
		return tel_oficina;
	}
	public void setTel_oficina(String tel_oficina) {
		this.tel_oficina = tel_oficina;
	}
	public int getId_estado() {
		return id_estado;
	}
	public void setId_estado(int id_estado) {
		this.id_estado = id_estado;
	}
	public int getNum_ext() {
		return num_ext;
	}
	public void setNum_ext(int num_ext) {
		this.num_ext = num_ext;
	}
	public String getNum_interior() {
		return num_interior;
	}
	public void setNum_interior(String num_interior) {
		this.num_interior = num_interior;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getDom_amex() {
		return dom_amex;
	}
	public void setDom_amex(String dom_amex) {
		this.dom_amex = dom_amex;
	}
	public int getTipo_cliente() {
		return tipo_cliente;
	}
	public void setTipo_cliente(int tipo_cliente) {
		this.tipo_cliente = tipo_cliente;
	}
	
	public String getNombreEstado() {
		return nombreEstado;
	}
	public void setNombreEstado(String nombreEstado) {
		this.nombreEstado = nombreEstado;
	}
	public String getNombreTarjeta() {
		return nombreTarjeta;
	}
	public void setNombreTarjeta(String nombreTarjeta) {
		this.nombreTarjeta = nombreTarjeta;
	}

}
