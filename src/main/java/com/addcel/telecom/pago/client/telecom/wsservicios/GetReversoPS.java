
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getReversoPS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getReversoPS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autentificacion" type="{http://wsServicios/}autentifica" minOccurs="0"/>
 *         &lt;element name="reqReversoServicio" type="{http://wsServicios/}reqReversoServicio" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getReversoPS", propOrder = {
    "autentificacion",
    "reqReversoServicio"
})

@XmlRootElement(name="getReversoPS")
public class GetReversoPS {

    protected Autentifica autentificacion;
    protected ReqReversoServicio reqReversoServicio;

    /**
     * Obtiene el valor de la propiedad autentificacion.
     * 
     * @return
     *     possible object is
     *     {@link Autentifica }
     *     
     */
    public Autentifica getAutentificacion() {
        return autentificacion;
    }

    /**
     * Define el valor de la propiedad autentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Autentifica }
     *     
     */
    public void setAutentificacion(Autentifica value) {
        this.autentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad reqReversoServicio.
     * 
     * @return
     *     possible object is
     *     {@link ReqReversoServicio }
     *     
     */
    public ReqReversoServicio getReqReversoServicio() {
        return reqReversoServicio;
    }

    /**
     * Define el valor de la propiedad reqReversoServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqReversoServicio }
     *     
     */
    public void setReqReversoServicio(ReqReversoServicio value) {
        this.reqReversoServicio = value;
    }

}
