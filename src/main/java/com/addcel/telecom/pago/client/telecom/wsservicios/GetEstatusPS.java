
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getEstatusPS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getEstatusPS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autentificacion" type="{http://wsServicios/}autentifica" minOccurs="0"/>
 *         &lt;element name="reqEstatusServicio" type="{http://wsServicios/}reqEstatusServicio" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEstatusPS", propOrder = {
    "autentificacion",
    "reqEstatusServicio"
})
@XmlRootElement(name="getEstatusPS")
public class GetEstatusPS {

    protected Autentifica autentificacion;
    protected ReqEstatusServicio reqEstatusServicio;

    /**
     * Obtiene el valor de la propiedad autentificacion.
     * 
     * @return
     *     possible object is
     *     {@link Autentifica }
     *     
     */
    public Autentifica getAutentificacion() {
        return autentificacion;
    }

    /**
     * Define el valor de la propiedad autentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Autentifica }
     *     
     */
    public void setAutentificacion(Autentifica value) {
        this.autentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad reqEstatusServicio.
     * 
     * @return
     *     possible object is
     *     {@link ReqEstatusServicio }
     *     
     */
    public ReqEstatusServicio getReqEstatusServicio() {
        return reqEstatusServicio;
    }

    /**
     * Define el valor de la propiedad reqEstatusServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqEstatusServicio }
     *     
     */
    public void setReqEstatusServicio(ReqEstatusServicio value) {
        this.reqEstatusServicio = value;
    }

}
