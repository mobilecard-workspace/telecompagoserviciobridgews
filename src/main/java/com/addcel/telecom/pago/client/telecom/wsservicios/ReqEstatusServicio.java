
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para reqEstatusServicio complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="reqEstatusServicio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="folioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioTelecomm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reqEstatusServicio", propOrder = {
    "folioCliente",
    "folioTelecomm",
    "intento"
})

@XmlRootElement(name="reqEstatusServicio")
public class ReqEstatusServicio {

    protected String folioCliente;
    protected String folioTelecomm;
    protected String intento;

    /**
     * Obtiene el valor de la propiedad folioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioCliente() {
        return folioCliente;
    }

    /**
     * Define el valor de la propiedad folioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioCliente(String value) {
        this.folioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad folioTelecomm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioTelecomm() {
        return folioTelecomm;
    }

    /**
     * Define el valor de la propiedad folioTelecomm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioTelecomm(String value) {
        this.folioTelecomm = value;
    }

    /**
     * Obtiene el valor de la propiedad intento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntento() {
        return intento;
    }

    /**
     * Define el valor de la propiedad intento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntento(String value) {
        this.intento = value;
    }

}
