
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "PagoServicio", targetNamespace = "http://wsServicios/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface PagoServicio {


    /**
     * 
     * @param eco
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getEco", targetNamespace = "http://wsServicios/", className = "wsservicios.GetEco")
    @ResponseWrapper(localName = "getEcoResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetEcoResponse")
    @Action(input = "http://wsServicios/PagoServicio/getEcoRequest", output = "http://wsServicios/PagoServicio/getEcoResponse")
    public String getEco(
        @WebParam(name = "eco", targetNamespace = "")
        String eco);

    /**
     * 
     * @param autentificacion
     * @return
     *     returns wsservicios.ResCatalogo
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getCatalogoPS", targetNamespace = "http://wsServicios/", className = "wsservicios.GetCatalogoPS")
    @ResponseWrapper(localName = "getCatalogoPSResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetCatalogoPSResponse")
    @Action(input = "http://wsServicios/PagoServicio/getCatalogoPSRequest", output = "http://wsServicios/PagoServicio/getCatalogoPSResponse")
    public ResCatalogo getCatalogoPS(
        @WebParam(name = "autentificacion", targetNamespace = "")
        Autentifica autentificacion);

    /**
     * 
     * @param autentificacion
     * @return
     *     returns wsservicios.ResLeyenda
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getLeyendaPS", targetNamespace = "http://wsServicios/", className = "wsservicios.GetLeyendaPS")
    @ResponseWrapper(localName = "getLeyendaPSResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetLeyendaPSResponse")
    @Action(input = "http://wsServicios/PagoServicio/getLeyendaPSRequest", output = "http://wsServicios/PagoServicio/getLeyendaPSResponse")
    public ResLeyenda getLeyendaPS(
        @WebParam(name = "autentificacion", targetNamespace = "")
        Autentifica autentificacion);

    /**
     * 
     * @param autentificacion
     * @param reqConsultaServicio
     * @return
     *     returns wsservicios.ResConsultaServicio
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getConsultaPS", targetNamespace = "http://wsServicios/", className = "wsservicios.GetConsultaPS")
    @ResponseWrapper(localName = "getConsultaPSResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetConsultaPSResponse")
    @Action(input = "http://wsServicios/PagoServicio/getConsultaPSRequest", output = "http://wsServicios/PagoServicio/getConsultaPSResponse")
    public ResConsultaServicio getConsultaPS(
        @WebParam(name = "autentificacion", targetNamespace = "")
        Autentifica autentificacion,
        @WebParam(name = "reqConsultaServicio", targetNamespace = "")
        ReqConsultaServicio reqConsultaServicio);

    /**
     * 
     * @param reqPagoServicio
     * @param autentificacion
     * @return
     *     returns wsservicios.ResPagoServicio
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getPeticionPS", targetNamespace = "http://wsServicios/", className = "wsservicios.GetPeticionPS")
    @ResponseWrapper(localName = "getPeticionPSResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetPeticionPSResponse")
    @Action(input = "http://wsServicios/PagoServicio/getPeticionPSRequest", output = "http://wsServicios/PagoServicio/getPeticionPSResponse")
    public ResPagoServicio getPeticionPS(
        @WebParam(name = "autentificacion", targetNamespace = "")
        Autentifica autentificacion,
        @WebParam(name = "reqPagoServicio", targetNamespace = "")
        ReqPagoServicio reqPagoServicio);

    /**
     * 
     * @param autentificacion
     * @param reqEstatusServicio
     * @return
     *     returns wsservicios.ResPagoServicio
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getEstatusPS", targetNamespace = "http://wsServicios/", className = "wsservicios.GetEstatusPS")
    @ResponseWrapper(localName = "getEstatusPSResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetEstatusPSResponse")
    @Action(input = "http://wsServicios/PagoServicio/getEstatusPSRequest", output = "http://wsServicios/PagoServicio/getEstatusPSResponse")
    public ResPagoServicio getEstatusPS(
        @WebParam(name = "autentificacion", targetNamespace = "")
        Autentifica autentificacion,
        @WebParam(name = "reqEstatusServicio", targetNamespace = "")
        ReqEstatusServicio reqEstatusServicio);

    /**
     * 
     * @param reqReversoServicio
     * @param autentificacion
     * @return
     *     returns wsservicios.ResReversoServicio
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getReversoPS", targetNamespace = "http://wsServicios/", className = "wsservicios.GetReversoPS")
    @ResponseWrapper(localName = "getReversoPSResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetReversoPSResponse")
    @Action(input = "http://wsServicios/PagoServicio/getReversoPSRequest", output = "http://wsServicios/PagoServicio/getReversoPSResponse")
    public ResReversoServicio getReversoPS(
        @WebParam(name = "autentificacion", targetNamespace = "")
        Autentifica autentificacion,
        @WebParam(name = "reqReversoServicio", targetNamespace = "")
        ReqReversoServicio reqReversoServicio);

    /**
     * 
     * @param autentificacion
     * @return
     *     returns wsservicios.ResSaldoServicio
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getSaldo", targetNamespace = "http://wsServicios/", className = "wsservicios.GetSaldo")
    @ResponseWrapper(localName = "getSaldoResponse", targetNamespace = "http://wsServicios/", className = "wsservicios.GetSaldoResponse")
    @Action(input = "http://wsServicios/PagoServicio/getSaldoRequest", output = "http://wsServicios/PagoServicio/getSaldoResponse")
    public ResSaldoServicio getSaldo(
        @WebParam(name = "autentificacion", targetNamespace = "")
        Autentifica autentificacion);

}
