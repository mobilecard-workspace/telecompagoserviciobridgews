
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getEco complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getEco">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEco", propOrder = {
    "eco"
})
@XmlRootElement(name="getEco")
public class GetEco {

    protected String eco;

    /**
     * Obtiene el valor de la propiedad eco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEco() {
        return eco;
    }

    /**
     * Define el valor de la propiedad eco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEco(String value) {
        this.eco = value;
    }

}
