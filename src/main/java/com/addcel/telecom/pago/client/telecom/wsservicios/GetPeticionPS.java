
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getPeticionPS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getPeticionPS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autentificacion" type="{http://wsServicios/}autentifica" minOccurs="0"/>
 *         &lt;element name="reqPagoServicio" type="{http://wsServicios/}reqPagoServicio" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPeticionPS", propOrder = {
    "autentificacion",
    "reqPagoServicio"
})
@XmlRootElement(name="getPeticionPS")
public class GetPeticionPS {

    protected Autentifica autentificacion;
    protected ReqPagoServicio reqPagoServicio;

    /**
     * Obtiene el valor de la propiedad autentificacion.
     * 
     * @return
     *     possible object is
     *     {@link Autentifica }
     *     
     */
    public Autentifica getAutentificacion() {
        return autentificacion;
    }

    /**
     * Define el valor de la propiedad autentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Autentifica }
     *     
     */
    public void setAutentificacion(Autentifica value) {
        this.autentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad reqPagoServicio.
     * 
     * @return
     *     possible object is
     *     {@link ReqPagoServicio }
     *     
     */
    public ReqPagoServicio getReqPagoServicio() {
        return reqPagoServicio;
    }

    /**
     * Define el valor de la propiedad reqPagoServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqPagoServicio }
     *     
     */
    public void setReqPagoServicio(ReqPagoServicio value) {
        this.reqPagoServicio = value;
    }

}
