
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para reqConsultaServicio complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="reqConsultaServicio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="disponible1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="disponible2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="disponible3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idProductoTerceros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="principalPesos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referencia1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referencia2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referencia3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reqConsultaServicio", propOrder = {
    "disponible1",
    "disponible2",
    "disponible3",
    "fechaOperacion",
    "idProductoTerceros",
    "principalPesos",
    "referencia1",
    "referencia2",
    "referencia3"
})
@XmlRootElement(name="reqConsultaServicio")
public class ReqConsultaServicio {

    protected String disponible1;
    protected String disponible2;
    protected String disponible3;
    protected String fechaOperacion;
    protected String idProductoTerceros;
    protected String principalPesos;
    protected String referencia1;
    protected String referencia2;
    protected String referencia3;

    /**
     * Obtiene el valor de la propiedad disponible1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisponible1() {
        return disponible1;
    }

    /**
     * Define el valor de la propiedad disponible1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisponible1(String value) {
        this.disponible1 = value;
    }

    /**
     * Obtiene el valor de la propiedad disponible2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisponible2() {
        return disponible2;
    }

    /**
     * Define el valor de la propiedad disponible2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisponible2(String value) {
        this.disponible2 = value;
    }

    /**
     * Obtiene el valor de la propiedad disponible3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisponible3() {
        return disponible3;
    }

    /**
     * Define el valor de la propiedad disponible3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisponible3(String value) {
        this.disponible3 = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Define el valor de la propiedad fechaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaOperacion(String value) {
        this.fechaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idProductoTerceros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProductoTerceros() {
        return idProductoTerceros;
    }

    /**
     * Define el valor de la propiedad idProductoTerceros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProductoTerceros(String value) {
        this.idProductoTerceros = value;
    }

    /**
     * Obtiene el valor de la propiedad principalPesos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrincipalPesos() {
        return principalPesos;
    }

    /**
     * Define el valor de la propiedad principalPesos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrincipalPesos(String value) {
        this.principalPesos = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia1() {
        return referencia1;
    }

    /**
     * Define el valor de la propiedad referencia1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia1(String value) {
        this.referencia1 = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia2() {
        return referencia2;
    }

    /**
     * Define el valor de la propiedad referencia2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia2(String value) {
        this.referencia2 = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia3() {
        return referencia3;
    }

    /**
     * Define el valor de la propiedad referencia3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia3(String value) {
        this.referencia3 = value;
    }
}
