
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para resReversoServicio complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="resReversoServicio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioTelecomm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idProductoTerceros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="principalPesos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resReversoServicio", propOrder = {
    "clave",
    "folioCliente",
    "folioTelecomm",
    "idProductoTerceros",
    "mensaje",
    "principalPesos"
})
@XmlRootElement(name="resReversoServicio")
public class ResReversoServicio {

    protected String clave;
    protected String folioCliente;
    protected String folioTelecomm;
    protected String idProductoTerceros;
    protected String mensaje;
    protected String principalPesos;

    /**
     * Obtiene el valor de la propiedad clave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClave() {
        return clave;
    }

    /**
     * Define el valor de la propiedad clave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClave(String value) {
        this.clave = value;
    }

    /**
     * Obtiene el valor de la propiedad folioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioCliente() {
        return folioCliente;
    }

    /**
     * Define el valor de la propiedad folioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioCliente(String value) {
        this.folioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad folioTelecomm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioTelecomm() {
        return folioTelecomm;
    }

    /**
     * Define el valor de la propiedad folioTelecomm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioTelecomm(String value) {
        this.folioTelecomm = value;
    }

    /**
     * Obtiene el valor de la propiedad idProductoTerceros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProductoTerceros() {
        return idProductoTerceros;
    }

    /**
     * Define el valor de la propiedad idProductoTerceros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProductoTerceros(String value) {
        this.idProductoTerceros = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad principalPesos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrincipalPesos() {
        return principalPesos;
    }

    /**
     * Define el valor de la propiedad principalPesos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrincipalPesos(String value) {
        this.principalPesos = value;
    }

}
