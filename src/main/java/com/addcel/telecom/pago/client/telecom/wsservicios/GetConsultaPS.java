
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getConsultaPS complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getConsultaPS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autentificacion" type="{http://wsServicios/}autentifica" minOccurs="0"/>
 *         &lt;element name="reqConsultaServicio" type="{http://wsServicios/}reqConsultaServicio" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getConsultaPS", propOrder = {
    "autentificacion",
    "reqConsultaServicio"
})
@XmlRootElement(name="getConsultaPS")
public class GetConsultaPS {

    protected Autentifica autentificacion;
    protected ReqConsultaServicio reqConsultaServicio;

    /**
     * Obtiene el valor de la propiedad autentificacion.
     * 
     * @return
     *     possible object is
     *     {@link Autentifica }
     *     
     */
    public Autentifica getAutentificacion() {
        return autentificacion;
    }

    /**
     * Define el valor de la propiedad autentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Autentifica }
     *     
     */
    public void setAutentificacion(Autentifica value) {
        this.autentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad reqConsultaServicio.
     * 
     * @return
     *     possible object is
     *     {@link ReqConsultaServicio }
     *     
     */
    public ReqConsultaServicio getReqConsultaServicio() {
        return reqConsultaServicio;
    }

    /**
     * Define el valor de la propiedad reqConsultaServicio.
     * 
     * @param value
     *     allowed object is
     *     {@link ReqConsultaServicio }
     *     
     */
    public void setReqConsultaServicio(ReqConsultaServicio value) {
        this.reqConsultaServicio = value;
    }

}
