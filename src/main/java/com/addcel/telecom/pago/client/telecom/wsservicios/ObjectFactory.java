
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsservicios package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPeticionPS_QNAME = new QName("http://wsServicios/", "getPeticionPS");
    private final static QName _GetSaldo_QNAME = new QName("http://wsServicios/", "getSaldo");
    private final static QName _GetReversoPS_QNAME = new QName("http://wsServicios/", "getReversoPS");
    private final static QName _GetCatalogoPSResponse_QNAME = new QName("http://wsServicios/", "getCatalogoPSResponse");
    private final static QName _GetEcoResponse_QNAME = new QName("http://wsServicios/", "getEcoResponse");
    private final static QName _GetConsultaPSResponse_QNAME = new QName("http://wsServicios/", "getConsultaPSResponse");
    private final static QName _GetPeticionPSResponse_QNAME = new QName("http://wsServicios/", "getPeticionPSResponse");
    private final static QName _GetEstatusPS_QNAME = new QName("http://wsServicios/", "getEstatusPS");
    private final static QName _GetSaldoResponse_QNAME = new QName("http://wsServicios/", "getSaldoResponse");
    private final static QName _GetConsultaPS_QNAME = new QName("http://wsServicios/", "getConsultaPS");
    private final static QName _GetCatalogoPS_QNAME = new QName("http://wsServicios/", "getCatalogoPS");
    private final static QName _GetLeyendaPSResponse_QNAME = new QName("http://wsServicios/", "getLeyendaPSResponse");
    private final static QName _GetLeyendaPS_QNAME = new QName("http://wsServicios/", "getLeyendaPS");
    private final static QName _GetReversoPSResponse_QNAME = new QName("http://wsServicios/", "getReversoPSResponse");
    private final static QName _GetEco_QNAME = new QName("http://wsServicios/", "getEco");
    private final static QName _GetEstatusPSResponse_QNAME = new QName("http://wsServicios/", "getEstatusPSResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsservicios
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSaldoResponse }
     * 
     */
    public GetSaldoResponse createGetSaldoResponse() {
        return new GetSaldoResponse();
    }

    /**
     * Create an instance of {@link GetEstatusPS }
     * 
     */
    public GetEstatusPS createGetEstatusPS() {
        return new GetEstatusPS();
    }

    /**
     * Create an instance of {@link GetConsultaPSResponse }
     * 
     */
    public GetConsultaPSResponse createGetConsultaPSResponse() {
        return new GetConsultaPSResponse();
    }

    /**
     * Create an instance of {@link GetPeticionPSResponse }
     * 
     */
    public GetPeticionPSResponse createGetPeticionPSResponse() {
        return new GetPeticionPSResponse();
    }

    /**
     * Create an instance of {@link GetCatalogoPSResponse }
     * 
     */
    public GetCatalogoPSResponse createGetCatalogoPSResponse() {
        return new GetCatalogoPSResponse();
    }

    /**
     * Create an instance of {@link GetEcoResponse }
     * 
     */
    public GetEcoResponse createGetEcoResponse() {
        return new GetEcoResponse();
    }

    /**
     * Create an instance of {@link GetReversoPS }
     * 
     */
    public GetReversoPS createGetReversoPS() {
        return new GetReversoPS();
    }

    /**
     * Create an instance of {@link GetPeticionPS }
     * 
     */
    public GetPeticionPS createGetPeticionPS() {
        return new GetPeticionPS();
    }

    /**
     * Create an instance of {@link GetSaldo }
     * 
     */
    public GetSaldo createGetSaldo() {
        return new GetSaldo();
    }

    /**
     * Create an instance of {@link GetEstatusPSResponse }
     * 
     */
    public GetEstatusPSResponse createGetEstatusPSResponse() {
        return new GetEstatusPSResponse();
    }

    /**
     * Create an instance of {@link GetEco }
     * 
     */
    public GetEco createGetEco() {
        return new GetEco();
    }

    /**
     * Create an instance of {@link GetReversoPSResponse }
     * 
     */
    public GetReversoPSResponse createGetReversoPSResponse() {
        return new GetReversoPSResponse();
    }

    /**
     * Create an instance of {@link GetLeyendaPS }
     * 
     */
    public GetLeyendaPS createGetLeyendaPS() {
        return new GetLeyendaPS();
    }

    /**
     * Create an instance of {@link GetLeyendaPSResponse }
     * 
     */
    public GetLeyendaPSResponse createGetLeyendaPSResponse() {
        return new GetLeyendaPSResponse();
    }

    /**
     * Create an instance of {@link GetCatalogoPS }
     * 
     */
    public GetCatalogoPS createGetCatalogoPS() {
        return new GetCatalogoPS();
    }

    /**
     * Create an instance of {@link GetConsultaPS }
     * 
     */
    public GetConsultaPS createGetConsultaPS() {
        return new GetConsultaPS();
    }

    /**
     * Create an instance of {@link ResCatalogo }
     * 
     */
    public ResCatalogo createResCatalogo() {
        return new ResCatalogo();
    }

    /**
     * Create an instance of {@link ResSaldoServicio }
     * 
     */
    public ResSaldoServicio createResSaldoServicio() {
        return new ResSaldoServicio();
    }

    /**
     * Create an instance of {@link ResPagoServicio }
     * 
     */
    public ResPagoServicio createResPagoServicio() {
        return new ResPagoServicio();
    }

    /**
     * Create an instance of {@link ResConsultaServicio }
     * 
     */
    public ResConsultaServicio createResConsultaServicio() {
        return new ResConsultaServicio();
    }

    /**
     * Create an instance of {@link Autentifica }
     * 
     */
    public Autentifica createAutentifica() {
        return new Autentifica();
    }

    /**
     * Create an instance of {@link ReqReversoServicio }
     * 
     */
    public ReqReversoServicio createReqReversoServicio() {
        return new ReqReversoServicio();
    }

    /**
     * Create an instance of {@link ReqEstatusServicio }
     * 
     */
    public ReqEstatusServicio createReqEstatusServicio() {
        return new ReqEstatusServicio();
    }

    /**
     * Create an instance of {@link ResLeyenda }
     * 
     */
    public ResLeyenda createResLeyenda() {
        return new ResLeyenda();
    }

    /**
     * Create an instance of {@link ReqPagoServicio }
     * 
     */
    public ReqPagoServicio createReqPagoServicio() {
        return new ReqPagoServicio();
    }

    /**
     * Create an instance of {@link ResReversoServicio }
     * 
     */
    public ResReversoServicio createResReversoServicio() {
        return new ResReversoServicio();
    }

    /**
     * Create an instance of {@link ReqConsultaServicio }
     * 
     */
    public ReqConsultaServicio createReqConsultaServicio() {
        return new ReqConsultaServicio();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPeticionPS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getPeticionPS")
    public JAXBElement<GetPeticionPS> createGetPeticionPS(GetPeticionPS value) {
        return new JAXBElement<GetPeticionPS>(_GetPeticionPS_QNAME, GetPeticionPS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSaldo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getSaldo")
    public JAXBElement<GetSaldo> createGetSaldo(GetSaldo value) {
        return new JAXBElement<GetSaldo>(_GetSaldo_QNAME, GetSaldo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReversoPS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getReversoPS")
    public JAXBElement<GetReversoPS> createGetReversoPS(GetReversoPS value) {
        return new JAXBElement<GetReversoPS>(_GetReversoPS_QNAME, GetReversoPS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCatalogoPSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getCatalogoPSResponse")
    public JAXBElement<GetCatalogoPSResponse> createGetCatalogoPSResponse(GetCatalogoPSResponse value) {
        return new JAXBElement<GetCatalogoPSResponse>(_GetCatalogoPSResponse_QNAME, GetCatalogoPSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEcoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getEcoResponse")
    public JAXBElement<GetEcoResponse> createGetEcoResponse(GetEcoResponse value) {
        return new JAXBElement<GetEcoResponse>(_GetEcoResponse_QNAME, GetEcoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsultaPSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getConsultaPSResponse")
    public JAXBElement<GetConsultaPSResponse> createGetConsultaPSResponse(GetConsultaPSResponse value) {
        return new JAXBElement<GetConsultaPSResponse>(_GetConsultaPSResponse_QNAME, GetConsultaPSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPeticionPSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getPeticionPSResponse")
    public JAXBElement<GetPeticionPSResponse> createGetPeticionPSResponse(GetPeticionPSResponse value) {
        return new JAXBElement<GetPeticionPSResponse>(_GetPeticionPSResponse_QNAME, GetPeticionPSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEstatusPS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getEstatusPS")
    public JAXBElement<GetEstatusPS> createGetEstatusPS(GetEstatusPS value) {
        return new JAXBElement<GetEstatusPS>(_GetEstatusPS_QNAME, GetEstatusPS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSaldoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getSaldoResponse")
    public JAXBElement<GetSaldoResponse> createGetSaldoResponse(GetSaldoResponse value) {
        return new JAXBElement<GetSaldoResponse>(_GetSaldoResponse_QNAME, GetSaldoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsultaPS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getConsultaPS")
    public JAXBElement<GetConsultaPS> createGetConsultaPS(GetConsultaPS value) {
        return new JAXBElement<GetConsultaPS>(_GetConsultaPS_QNAME, GetConsultaPS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCatalogoPS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getCatalogoPS")
    public JAXBElement<GetCatalogoPS> createGetCatalogoPS(GetCatalogoPS value) {
        return new JAXBElement<GetCatalogoPS>(_GetCatalogoPS_QNAME, GetCatalogoPS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLeyendaPSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getLeyendaPSResponse")
    public JAXBElement<GetLeyendaPSResponse> createGetLeyendaPSResponse(GetLeyendaPSResponse value) {
        return new JAXBElement<GetLeyendaPSResponse>(_GetLeyendaPSResponse_QNAME, GetLeyendaPSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLeyendaPS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getLeyendaPS")
    public JAXBElement<GetLeyendaPS> createGetLeyendaPS(GetLeyendaPS value) {
        return new JAXBElement<GetLeyendaPS>(_GetLeyendaPS_QNAME, GetLeyendaPS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReversoPSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getReversoPSResponse")
    public JAXBElement<GetReversoPSResponse> createGetReversoPSResponse(GetReversoPSResponse value) {
        return new JAXBElement<GetReversoPSResponse>(_GetReversoPSResponse_QNAME, GetReversoPSResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getEco")
    public JAXBElement<GetEco> createGetEco(GetEco value) {
        return new JAXBElement<GetEco>(_GetEco_QNAME, GetEco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEstatusPSResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wsServicios/", name = "getEstatusPSResponse")
    public JAXBElement<GetEstatusPSResponse> createGetEstatusPSResponse(GetEstatusPSResponse value) {
        return new JAXBElement<GetEstatusPSResponse>(_GetEstatusPSResponse_QNAME, GetEstatusPSResponse.class, null, value);
    }

}
