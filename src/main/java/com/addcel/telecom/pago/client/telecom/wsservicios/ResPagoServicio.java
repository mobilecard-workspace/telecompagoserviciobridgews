
package com.addcel.telecom.pago.client.telecom.wsservicios;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para resPagoServicio complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="resPagoServicio">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="disponible1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="disponible2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="disponible3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioProveedor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folioTelecomm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idProductoTerceros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mensaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="precio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="premio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="principalLocal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="principalPesos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="redondeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referencia1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referencia2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="referencia3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="terminal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tienda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCambio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoMoneda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="total" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resPagoServicio", propOrder = {
    "clave",
    "disponible1",
    "disponible2",
    "disponible3",
    "fechaOperacion",
    "folioCliente",
    "folioProveedor",
    "folioTelecomm",
    "idProductoTerceros",
    "iva",
    "mensaje",
    "operador",
    "precio",
    "premio",
    "principalLocal",
    "principalPesos",
    "redondeo",
    "referencia1",
    "referencia2",
    "referencia3",
    "subTotal",
    "terminal",
    "tienda",
    "tipoCambio",
    "tipoMoneda",
    "total"
})

@XmlRootElement(name="resPagoServicio")
public class ResPagoServicio {

    protected String clave;
    protected String disponible1;
    protected String disponible2;
    protected String disponible3;
    protected String fechaOperacion;
    protected String folioCliente;
    protected String folioProveedor;
    protected String folioTelecomm;
    protected String idProductoTerceros;
    protected String iva;
    protected String mensaje;
    protected String operador;
    protected String precio;
    protected String premio;
    protected String principalLocal;
    protected String principalPesos;
    protected String redondeo;
    protected String referencia1;
    protected String referencia2;
    protected String referencia3;
    protected String subTotal;
    protected String terminal;
    protected String tienda;
    protected String tipoCambio;
    protected String tipoMoneda;
    protected String total;

    /**
     * Obtiene el valor de la propiedad clave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClave() {
        return clave;
    }

    /**
     * Define el valor de la propiedad clave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClave(String value) {
        this.clave = value;
    }

    /**
     * Obtiene el valor de la propiedad disponible1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisponible1() {
        return disponible1;
    }

    /**
     * Define el valor de la propiedad disponible1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisponible1(String value) {
        this.disponible1 = value;
    }

    /**
     * Obtiene el valor de la propiedad disponible2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisponible2() {
        return disponible2;
    }

    /**
     * Define el valor de la propiedad disponible2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisponible2(String value) {
        this.disponible2 = value;
    }

    /**
     * Obtiene el valor de la propiedad disponible3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisponible3() {
        return disponible3;
    }

    /**
     * Define el valor de la propiedad disponible3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisponible3(String value) {
        this.disponible3 = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaOperacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Define el valor de la propiedad fechaOperacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaOperacion(String value) {
        this.fechaOperacion = value;
    }

    /**
     * Obtiene el valor de la propiedad folioCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioCliente() {
        return folioCliente;
    }

    /**
     * Define el valor de la propiedad folioCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioCliente(String value) {
        this.folioCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad folioProveedor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioProveedor() {
        return folioProveedor;
    }

    /**
     * Define el valor de la propiedad folioProveedor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioProveedor(String value) {
        this.folioProveedor = value;
    }

    /**
     * Obtiene el valor de la propiedad folioTelecomm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioTelecomm() {
        return folioTelecomm;
    }

    /**
     * Define el valor de la propiedad folioTelecomm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioTelecomm(String value) {
        this.folioTelecomm = value;
    }

    /**
     * Obtiene el valor de la propiedad idProductoTerceros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProductoTerceros() {
        return idProductoTerceros;
    }

    /**
     * Define el valor de la propiedad idProductoTerceros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProductoTerceros(String value) {
        this.idProductoTerceros = value;
    }

    /**
     * Obtiene el valor de la propiedad iva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIva() {
        return iva;
    }

    /**
     * Define el valor de la propiedad iva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIva(String value) {
        this.iva = value;
    }

    /**
     * Obtiene el valor de la propiedad mensaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Define el valor de la propiedad mensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensaje(String value) {
        this.mensaje = value;
    }

    /**
     * Obtiene el valor de la propiedad operador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperador() {
        return operador;
    }

    /**
     * Define el valor de la propiedad operador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperador(String value) {
        this.operador = value;
    }

    /**
     * Obtiene el valor de la propiedad precio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrecio() {
        return precio;
    }

    /**
     * Define el valor de la propiedad precio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrecio(String value) {
        this.precio = value;
    }

    /**
     * Obtiene el valor de la propiedad premio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremio() {
        return premio;
    }

    /**
     * Define el valor de la propiedad premio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremio(String value) {
        this.premio = value;
    }

    /**
     * Obtiene el valor de la propiedad principalLocal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrincipalLocal() {
        return principalLocal;
    }

    /**
     * Define el valor de la propiedad principalLocal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrincipalLocal(String value) {
        this.principalLocal = value;
    }

    /**
     * Obtiene el valor de la propiedad principalPesos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrincipalPesos() {
        return principalPesos;
    }

    /**
     * Define el valor de la propiedad principalPesos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrincipalPesos(String value) {
        this.principalPesos = value;
    }

    /**
     * Obtiene el valor de la propiedad redondeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedondeo() {
        return redondeo;
    }

    /**
     * Define el valor de la propiedad redondeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedondeo(String value) {
        this.redondeo = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia1() {
        return referencia1;
    }

    /**
     * Define el valor de la propiedad referencia1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia1(String value) {
        this.referencia1 = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia2() {
        return referencia2;
    }

    /**
     * Define el valor de la propiedad referencia2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia2(String value) {
        this.referencia2 = value;
    }

    /**
     * Obtiene el valor de la propiedad referencia3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia3() {
        return referencia3;
    }

    /**
     * Define el valor de la propiedad referencia3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia3(String value) {
        this.referencia3 = value;
    }

    /**
     * Obtiene el valor de la propiedad subTotal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubTotal() {
        return subTotal;
    }

    /**
     * Define el valor de la propiedad subTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubTotal(String value) {
        this.subTotal = value;
    }

    /**
     * Obtiene el valor de la propiedad terminal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminal() {
        return terminal;
    }

    /**
     * Define el valor de la propiedad terminal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminal(String value) {
        this.terminal = value;
    }

    /**
     * Obtiene el valor de la propiedad tienda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTienda() {
        return tienda;
    }

    /**
     * Define el valor de la propiedad tienda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTienda(String value) {
        this.tienda = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCambio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCambio() {
        return tipoCambio;
    }

    /**
     * Define el valor de la propiedad tipoCambio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCambio(String value) {
        this.tipoCambio = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoMoneda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMoneda() {
        return tipoMoneda;
    }

    /**
     * Define el valor de la propiedad tipoMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMoneda(String value) {
        this.tipoMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad total.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotal() {
        return total;
    }

    /**
     * Define el valor de la propiedad total.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotal(String value) {
        this.total = value;
    }

}
