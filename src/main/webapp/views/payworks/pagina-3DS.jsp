<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Purchase Verification</title>
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
%>
    	<style>
	    * {
		  /*with these codes padding and border does not increase it's width.Gives intuitive style.*/
		  -webkit-box-sizing: border-box;   
		  -moz-box-sizing: border-box;
		  box-sizing: border-box;
		  color:#33313;
		  font-family: "Arial", Helvetica, sans-serif;
		}

		body {
		   margin:0;
		   padding:0;		
		   color: white;  
		   background-color:#8C8C8C;
		}
		div#envelope{
			width:90%;
			margin: 0 auto;
			background-color:#D3441C;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
		} 
		form{
			width:80%;
			margin:0 10%;
		}  
		form header {
		  text-align:center;
		  font-family: 'Roboto Slab', serif;
		}
		/* Makes responsive fields.Sets size and field alignment.*/
		input[type=text],input[type=password]{
			margin-bottom: 20px;
			margin-top: 10px;
			width:100%;
			padding: 15px;
			border-radius:5px;
			border:1px solid #dbd9d6;
			font-size: 110%;
		}
		input[type=submit]
		{
			margin-bottom: 20px;
			width:100%;
			color:#000000;
			padding: 15px;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
			height: 55px;
		}
		input[type=button]
		{
			margin-bottom: 20px;
			width:100%;
			padding: 15px;
			color:#000000;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
		}
		textarea{
			width:100%;
			padding: 15px;
			margin-top: 10px;
			border:1px solid #7ac9b7;
			border-radius:5px; 
			margin-bottom: 20px;
			resize:none;
		}
		input[type=text]:focus, textarea:focus, select:focus {
		  border-color: #333132;
		}
		.styled-select {
			width:100%;
			overflow: hidden;
			background: #FFFFFF;
			border-radius:5px;
			border:1px solid #dbd9d6;
		}
		.styled-select select {
			font-size: 110%;
			width: 100%;
			border: 0 !important;
			padding: 15px 0px 15px 0px;
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;
			text-indent:  0.01px;
			text-overflow: '';
		}
		.btn-style {
		    font-family: arial;
		    font-weight: bold;
		    width: 100%;
		    opacity: 0.5;
		    border: solid 1px #e6e6e6;
		    border-radius: 3px;
		    moz-border-radius: 3px;
		    font-size: 16px;
		    padding: 1px 17px;
		    background-color: #ffffff;
		    height: 55px;
		}
	</style>
</head>
<body>

<div id="envelope">
	<%-- <p style="text-align: center;">
			<img src="https://www.mobilecard.mx:8443/ADOServicios/resources/logo-ado.jpg" width="100px" height="30px" align="center"/>
		</p>--%>
		<p style="text-align: center;">Portal 3D Secure para pago m&oacute;vil</p>
		<p style="text-align: center;">Proporciona la siguiente informaci&oacute;n:</p>
		<br/>
		<form:form id="main-form" name="main-form" method="post" 
    		autocomplete="off" modelAttribute="pagoForm"
    		action="${pageContext.request.contextPath}/telecom/pagoServicio/enviaPago3DS">
    		<form:hidden id="referencia" path="referencia"  />
    		<form:hidden id="idUsuario" path="idUsuario" />
    		<form:hidden id="idProveedor" path="idProveedor" />
    		<form:hidden id="emisor" path="emisor" />
    		<form:hidden id="operacion" path="operacion" />
    		<form:hidden id="concepto" path="concepto" />
    		<form:hidden id="comision" path="comision" />
    		<form:hidden id="monto" path="monto" />
    		<form:hidden id="eMail" path="email" />
    		<form:hidden id="idTransaccion" path="idTransaccion" />
    		<form:hidden id="banco" path="banco" />
    		<!-- coloque esta variable para que la el formulario pudiera funcionar -->
    		<form:hidden id="idPais" path="idPais" />
    		<form:hidden id="idTarjeta" path="idTarjeta" />
    		<form:hidden id="total" path="total" />
    		<form:hidden id="idProductoTerceros" path="idProductoTerceros" />
    		<form:hidden id="folioCliente" path="folioCliente" />
    		<form:hidden id="referencia1" path="referencia1" />
    		<form:hidden id="referencia2" path="referencia2" />
    		<form:hidden id="referencia3" path="referencia3" />
    		<form:hidden id="principalPesos" path="principalPesos" />
    		<form:hidden id="principalLocal" path="principalLocal" />
    		<form:hidden id="tipoCambio" path="tipoCambio" />
    		<form:hidden id="tipoMoneda" path="tipoMoneda" />
    		<form:hidden id="precio" path="precio" />
    		<form:hidden id="precio2" path="precio2" />
    		<form:hidden id="tienda" path="tienda" />
    		<form:hidden id="terminal" path="terminal" />
    		<form:hidden id="operador" path="operador" />
    		<form:hidden id="disponible1" path="disponible1" />
    		<form:hidden id="disponible2" path="disponible2" />
    		<form:hidden id="disponible3" path="disponible3" />
    		<form:hidden id="estado" path="estado" />
    		<form:hidden id="ciudad" path="ciudad" />
    		<form:hidden id="calle" path="calle" />
    		<form:hidden id="colonia" path="colonia" />
    		<form:hidden id="codigoPostal" path="codigoPostal" />
    		<form:hidden id="fechaNacimiento" path="fechaNacimiento" />
    		<form:hidden id="rfc" path="rfc" />
    		<form:hidden id="telefono" path="telefono" />
    		<!-- Fin Modificacion -->
				<p> Informaci&oacute;n de la tarjeta de cr&eacute;dito</p>

				<label for="nombre">Nombre: ${pagoForm.nombre}</label>
				<br></br>
				<!-- form:input type="text" path="nombre" size="40,1" maxlength="40" value="" required="true" /-->
				
				<label for="tarjeta">N&uacute;mero de Tarjeta: ${pagoForm.tarjeta}</label>
				<br></br>
				<!-- form:input type="text" path="tarjeta" size="40,1"	maxlength="16" value="" required="true" /-->

				<label for="tipoTarjeta">Tipo: ${pagoForm.tarjetaT}</label><br/>
				
				<!-- div class="styled-select">
					<form:select path="tipoTarjeta" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
						<option value="1">VISA</option>
						<option value="2">MasterCard</option>
						<option value="3">Carnet</option>
					</form:select  >
				</div -->
				<div style="width:100%;padding-top:10px">
					<label for ="mes">Fecha de Vencimiento (mes/a&ntilde;o) : ${pagoForm.vigencia}</label><br/>
					<!-- div class="styled-select" style="float:left;width:40%;">
						<form:select  path="mes" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</form:select> 
					</div>
					<div  class="styled-select" style="float:right;width:40%;">
						<form:select  path="anio" class="selmenu">
							<%
								java.util.Calendar C = java.util.Calendar.getInstance();
								int anio = C.get(java.util.Calendar.YEAR);
								for (int i = 0; i < 15; i++) {
									out.println("<option value=\""+anio+"\">" + anio+ "</option>");
									anio++;
								}
							%>
						</form:select>
					</div -->
				</div>
				<div style="width:100%;padding-top:10px">
					<label for ="cvv2">C&oacute;digo de seguridad (CVV2/CVC2)</label>
					<form:input type="password" path="cvv2" size="3,1" maxlength="3" value="" required="true" class="txtinput"/>
				</div>
				<p>Verifica los datos y selecciona el bot&oacute;n Pagar para efectuar el cargo a tu tarjeta.</p>
				<div style="width:100%;padding-top:10px">
					<input type="submit" value="Pagar" class="btn-style"/>
				</div>
				<!-- <div style="width:100%;padding-top:10px">
					<input type="button" value="Cancelar" />
				</div> -->
			</div>
		</form:form>
	</div>
</body>
</html>
